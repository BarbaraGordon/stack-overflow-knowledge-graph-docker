# import stanza
import nltk
from nltk.corpus import stopwords
from nltk import download
# import jieba
import mteb

import torch
from torch import utils, cosine_similarity
from accelerate import init_empty_weights, infer_auto_device_map, load_checkpoint_and_dispatch

from transformers import BertTokenizer, AutoModel, AutoTokenizer, AutoModelForCausalLM, AutoConfig

from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.feature_extraction.text import TfidfTransformer
# from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from sentence_transformers import SentenceTransformer,util

import heapq
import os
# import csv
# import json
# import itertools
from string import punctuation
import re
# import math
import numpy
from numpy import reshape, shape, empty, zeros, ndim, ndarray
import scipy
from scipy import spatial,sparse
from scipy.sparse import coo_matrix, csr_matrix, csc_matrix, spmatrix
from scipy.spatial.distance import pdist, cosine
from scipy.linalg import norm
from sklearn.metrics.pairwise import cosine_similarity

def torch_calculate_score(context:str,query:str):
    tokenizer = AutoTokenizer.from_pretrained(".\\transformers_models\\gpt-neo-125m")
    model = AutoModelForCausalLM.from_pretrained(".\\transformers_models\\gpt-neo-125m")
    context_enc = tokenizer.encode(context, add_special_tokens=False)
    continuation_enc = tokenizer.encode(query, add_special_tokens=False)
    # Slice off the last token, as we take its probability from the one before
    model_input = torch.tensor(context_enc+continuation_enc[:-1])
    continuation_len = len(continuation_enc)
    input_len, = model_input.shape
    # [seq_len] -> [seq_len, vocab]
    logprobs = torch.nn.functional.log_softmax(model(model_input)[0], dim=-1).cpu()
    # [seq_len, vocab] -> [continuation_len, vocab]
    logprobs = logprobs[input_len-continuation_len:]
    # Gather the log probabilities of the continuation tokens -> [continuation_len]
    logprobs = torch.gather(logprobs, 1, torch.tensor(continuation_enc).unsqueeze(-1)).squeeze(-1)
    score = torch.sum(logprobs)
    absolute_score = abs(score)

def gpt_neo_get_answer(input:str, descriptions_filepath:str, logs_filepath:str):

    descriptions_file = open(descriptions_filepath, 'r', encoding='utf-8', newline='')
    descriptions_origin_file = open(descriptions_filepath.replace('.txt','_origin.txt'), 'r', encoding='utf-8', newline='')
    descriptions_index_file = open(descriptions_filepath.replace('.txt','_index.txt'), 'r', encoding='utf-8', newline='')
    logs_file = open(logs_filepath, 'r', encoding='utf-8', newline='')
    logs_index_file = open(logs_filepath.replace('.txt','_index.txt'), 'r', encoding='utf-8', newline='')

    descriptions = [d.strip() for d in descriptions_file.readlines()]
    descriptions_origin = [d.strip() for d in descriptions_origin_file.readlines()]
    logs = [l.strip() for l in logs_file.readlines()]
    descriptions_index = [eval(d.strip())[0] for d in descriptions_index_file.readlines()]
    logs_index = [l.split()[0] for l in logs_index_file.readlines()]
    # Get models - The package will take care of downloading the models automatically
    # For best performance: EleutherAI/gpt-j-6B
    # Original: EleutherAI/gpt-neo-125m
    model_path = ".\\transformers_models\\gpt-neo-125m"
    model_path = ".\\gpt_models\\gpt-j-6B"
    config = AutoConfig.from_pretrained(model_path)
    # with init_empty_weights():
    tokenizer = AutoTokenizer.from_pretrained(".\\transformers_models\\gpt-neo-125m")
    model = AutoModelForCausalLM.from_pretrained(".\\transformers_models\\gpt-neo-125m")
    # Deactivate Dropout (There is no dropout in the above models so it makes no difference here but other SGPT models may have dropout)
    model.eval()
    prompt = 'Documents are searched to find matches with the same content.\nThe document "{}" is a good search result for "'
    query = input
    docs = descriptions
#     for query in queries:
#         print(f"Query: {query}")
    print(f"Query: {query}")
    corpus_number = len(docs)
    
    corpus_score_list = []
    log_score_list = []
    score_list = []
    for i in range(0,corpus_number):
        doc = docs[i]
        index = descriptions_index[i]
        context = prompt.format(doc)
        context_enc = tokenizer.encode(context, add_special_tokens=False)
        continuation_enc = tokenizer.encode(query, add_special_tokens=False)
        # Slice off the last token, as we take its probability from the one before
        model_input = torch.tensor(context_enc+continuation_enc[:-1])
        continuation_len = len(continuation_enc)
        input_len, = model_input.shape
        # [seq_len] -> [seq_len, vocab]
        logprobs = torch.nn.functional.log_softmax(model(model_input)[0], dim=-1).cpu()
        # [seq_len, vocab] -> [continuation_len, vocab]
        logprobs = logprobs[input_len-continuation_len:]
        # Gather the log probabilities of the continuation tokens -> [continuation_len]
        logprobs = torch.gather(logprobs, 1, torch.tensor(continuation_enc).unsqueeze(-1)).squeeze(-1)
        score = torch.sum(logprobs)
        absolute_score = abs(score)
        # The higher (closer to 0), the more similar
        index_corpus = [index, absolute_score]
        corpus_score_list.append(index_corpus)
        score_list.append(index_corpus)
        print(f"Index: {index} Description: {doc[:20] + '...'} Score: {absolute_score}")
    
    docs = logs
    log_number = len(docs)
    for i in range(0,log_number):
        doc = docs[i]
        index = logs_index[i]
        context = prompt.format(doc)
        context_enc = tokenizer.encode(context, add_special_tokens=False)
        continuation_enc = tokenizer.encode(query, add_special_tokens=False)
        # Slice off the last token, as we take its probability from the one before
        model_input = torch.tensor(context_enc + continuation_enc[:-1])
        continuation_len = len(continuation_enc)
        input_len, = model_input.shape
        # [seq_len] -> [seq_len, vocab]
        logprobs = torch.nn.functional.log_softmax(model(model_input)[0], dim=-1).cpu()
        # [seq_len, vocab] -> [continuation_len, vocab]
        logprobs = logprobs[input_len-continuation_len:]
        # Gather the log probabilities of the continuation tokens -> [continuation_len]
        logprobs = torch.gather(logprobs, 1, torch.tensor(continuation_enc).unsqueeze(-1)).squeeze(-1)
        score = torch.sum(logprobs)
        absolute_score = abs(score)
        # The higher (closer to 0), the more similar
        index_log = [index, absolute_score]
        log_score_list.append(index_log)
        score_list.append(index_log)
        print(f"Index: {index} Log: {doc[:20] + '...'} Score: {score}")

    # score_list_length = len(score_list)
    score_number_col = [s[1] for s in score_list]
    index_col = [s[0] for s in score_list]
    descriptions_line_count_col = [d[1] for d in descriptions_index]
    descriptions_index_col = [d[0] for d in descriptions_index]
    largest5 = heapq.nlargest(5, score_number_col)
    target_index_list = []
    reference_list = []

    print('您可以参考以下索引对应的问题：')
    for i in range(0, len(largest5)):
        target_index = index_col[score_number_col.index(largest5[i])]
        target_index_list.append(target_index)
        target_description_line = int(descriptions_line_count_col[descriptions_index_col.index(target_index)])
        target_reference_description = descriptions_origin[target_description_line]
        reference_list.append(target_reference_description)
        print(target_index + ': ' + target_reference_description)
    
    
gpt_neo_get_answer('how can I fix my docker container','.\\similarity_template\\description.txt','.\\log_template_extract\\log_train.txt')


# @article{muennighoff2022sgpt,
#   title={SGPT: GPT Sentence Embeddings for Semantic Search},
#   author={Muennighoff, Niklas},
#   journal={arXiv preprint arXiv:2202.08904},
#   year={2022}
# }
# <https://github.com/Muennighoff/sgpt>
    
        
