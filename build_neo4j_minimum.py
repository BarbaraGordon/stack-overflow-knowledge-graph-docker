import neo4j
from neo4j import GraphDatabase, RoutingControl, Driver, Session
from neo4j.exceptions import DriverError, Neo4jError
import py2neo
from py2neo import Graph,Node,Relationship,NodeMatcher
import os
import subprocess
import csv
import typing_extensions
import typing
from typing import LiteralString
import re

# import logging
# import sys
# handler = logging. FileHandler('logfile.log')
# handler.setLevel(logging.DEBUG)
# logging.getLogger("neo4j").addHandler(handler)
# logging.getLogger("neo4j").setLevel(logging.DEBUG)

# URI = "neo4j://127.0.0.1:7687"
URI = "bolt://127.0.0.1:7687"
AUTH = ("neo4j", "Hirain!neo4j")
database_name = "neo4j"

def clear_graph(driver:Driver):
    driver.execute_query(
        "MATCH p=()--() DELETE p",
        database_ = database_name
    )
    driver.execute_query(
        "MATCH (n) DELETE n",
        database_ = database_name
    )

def is_index_in_range(current_index:str, end_index:str) -> bool:
    end_page = int(end_index[0:3])
    end_order = int(end_index[4:6])
    page = int(current_index[0:3])
    order = int(current_index[4:6])
    if page < end_page:
        return True
    if page == end_page and order <= end_order:
        return True
    else:
        return False

def add_node_complex(driver:Driver, node_id:str, node_label:str, node_content:str):
    # labels = ['Index', 'Description', 'Log', 'Tag', 'Reason', 'Solution']
    if node_label == labels[0]:
        driver.execute_query(
        "CREATE (n:Index {id: $node_id, content: $node_content})",
        node_id = node_id, node_content = node_content, 
        database_ = database_name,
    )
        
    elif node_label == labels[1]:
        driver.execute_query(
        "CREATE (n:Description {id: $node_id, content: $node_content})",
        node_id = node_id, node_content = node_content, 
        database_ = database_name,
    )
        
    elif node_label == labels[2]:
        driver.execute_query(
        "CREATE (n:Log {id: $node_id, content: $node_content})",
        node_id = node_id, node_content = node_content, 
        database_ = database_name,
    )
        
    elif node_label == labels[3]:
        driver.execute_query(
        "CREATE (n:Tag {id: $node_id, content: $node_content})",
        node_id = node_id, node_content = node_content, 
        database_ = database_name,
    )
        
    elif node_label == labels[4]:
        driver.execute_query(
        "CREATE (n:Reason {id: $node_id, content: $node_content})",
        node_id = node_id, node_content = node_content, 
        database_ = database_name,
    )
    
    elif node_label == labels[5]:
        driver.execute_query(
        "CREATE (n:Solution {id: $node_id, content: $node_content})",
        node_id = node_id, node_content = node_content, 
        database_ = database_name,
    )
    
    else:
        print('LABEL ERROR!')
        exit('LABEL ERROR!')

def add_node(driver:Driver, node_id:str, node_label:str, node_content:str):
    driver.execute_query(
        query_ = "CALL apoc.create.node([$label], {id: $node_id, content: $node_content}) YIELD node RETURN node",
        parameters_ = {'label': node_label, 'node_id': node_id, 'node_content': node_content},
        database_ = database_name,
    )

def add_log_node_info(driver:Driver, node_id:str, template_list: list[str]):
    template_number = len(template_list)
    driver.execute_query(
        query_="MATCH (n {id: $id}) "
        "CALL apoc.create.setProperties(n, ['template_list', 'template_number'], [$template_list, $template_number]) YIELD node RETURN node",
        parameters_={'id': node_id,'template_list': template_list, 'template_number': template_number}, 
        database_ = database_name,
    )

def remove_node(driver:Driver, node_id:str):
    driver.execute_query(
        "MATCH (n {id: $node_id}) RETURN n DELETE n",
        node_id = node_id, 
        database_ = database_name,
    )

def is_tag_node_exist(driver:Driver, tag:str):
    records, _, _ = driver.execute_query(
        "MATCH (n {id: $node_id}) RETURN n",
        node_id = 'tag_' + tag, 
        database_ = database_name,
    )
    if records:
        return True
    else:
        return False

def is_node_exist(driver:Driver, node_id:str):
    records, _, _ = driver.execute_query(
        "MATCH (n {id: $node_id}) RETURN n",
        node_id = node_id, 
        database_ = database_name,
    )
    if records:
        return True
    else:
        return False

def is_relationship_exist(driver:Driver, start_node_id:str, end_node_id:str):
    records, _, _ = driver.execute_query(
        "MATCH (a {id: $start_node_id})-[rel]->(b {id: $end_node_id}) RETURN rel",
        start_node_id = start_node_id, end_node_id=end_node_id,
        database_ = database_name,
    )
    if records:
        return True
    else:
        return False

# start_node必须是index、description、log或者reason中的一种
def add_relationship(driver:Driver, start_node_id:str, end_node_id:str):
    if start_node_id[0:3] == 'ind':
        if end_node_id[0:3] == 'des':
            relationship_order = 0
            reverse_relationship_order = 5
            driver.execute_query(
                "MATCH (a:Index {id: $start_node_id}), (b:Description {id: $end_node_id})"
                "MERGE (a)-[:is_described_as {key: $keyword}]->(b)"
                "MERGE (b)-[:refer_to_index_as {key: $reverse_keyword}]->(a)",
                start_node_id = start_node_id, end_node_id=end_node_id, 
                keyword = keywords[relationship_order],
                reverse_keyword = keywords[reverse_relationship_order],
                database_ = database_name,
            )
        elif end_node_id[0:3] == 'log':
            relationship_order = 1
            driver.execute_query(
                "MATCH (a:Index {id: $start_node_id}), (b:Log {id: $end_node_id})"
                "MERGE (a)-[:has_log_as {key: $keyword}]->(b)",
                start_node_id = start_node_id, end_node_id=end_node_id, 
                keyword = keywords[relationship_order],
                database_ = database_name,
            )
        elif end_node_id[0:3] == 'tag':
            relationship_order = 2
            reverse_relationship_order = 6
            driver.execute_query(
                "MATCH (a:Index {id: $start_node_id}), (b:Tag {id: $end_node_id})"
                "MERGE (a)-[:is_tagged_with {key: $keyword}]->(b)"
                "MERGE (b)-[:include_index_as {key: $reverse_keyword}]->(a)",
                start_node_id = start_node_id, end_node_id=end_node_id, 
                keyword = keywords[relationship_order], 
                reverse_keyword = keywords[reverse_relationship_order],
                database_ = database_name,
            )
        elif end_node_id[0:3] == 'rea':
            relationship_order = 3
            driver.execute_query(
                "MATCH (a:Index {id: $start_node_id}), (b:Reason {id: $end_node_id})"
                "MERGE (a)-[:is_possibly_caused_by {key: $keyword}]->(b)",
                start_node_id = start_node_id, end_node_id=end_node_id, 
                keyword = keywords[relationship_order],
                database_ = database_name,
            )
        elif end_node_id[0:3] == 'sol':
            # 如果这个答案里没有找到原因，就把解决方案直接连接到故障索引上
            relationship_order = 4
            driver.execute_query(
                "MATCH (a:Index {id: $start_node_id}), (b:Solution {id: $end_node_id})"
                "MERGE (a)-[:can_be_possibly_solved_by {key: $keyword}]->(b)",
                start_node_id = start_node_id, end_node_id=end_node_id, 
                keyword = keywords[relationship_order],
                database_ = database_name,
            )
    elif start_node_id[0:3] == 'des':
        if end_node_id[0:3] == 'des':
            relationship_order = 7
            driver.execute_query(
                "MATCH (a:Description {id: $start_node_id}), (b:Description {id: $end_node_id})"
                "MERGE (a)-[:has_textual_similarity_with {key: $keyword}]-(b)"
                "MERGE (b)-[:has_textual_similarity_with {key: $keyword}]-(a)",
                start_node_id = start_node_id, end_node_id=end_node_id, 
                keyword = keywords[relationship_order],
                database_ = database_name,
            )
    elif start_node_id[0:3] == 'log':
        if end_node_id[0:3] == 'log':
            relationship_order = 8
            driver.execute_query(
                "MATCH (a:Log {id: $start_node_id}), (b:Log {id: $end_node_id})"
                "MERGE (a)-[:shares_same_log_template_with {key: $keyword}]-(b)"
                "MERGE (b)-[:shares_same_log_template_with {key: $keyword}]-(a)",
                start_node_id = start_node_id, end_node_id=end_node_id, 
                keyword = keywords[relationship_order],
                database_ = database_name,
            )
    elif start_node_id[0:3] == 'rea':
        if end_node_id[0:3] == 'rea':
            relationship_order = 7
            driver.execute_query(
                "MATCH (a:Reason {id: $start_node_id}), (b:Reason {id: $end_node_id})"
                "MERGE (a)-[:has_textual_similarity_with {key: $keyword}]-(b)"
                "MERGE (b)-[:has_textual_similarity_with {key: $keyword}]-(a)",
                start_node_id = start_node_id, end_node_id=end_node_id, 
                keyword = keywords[relationship_order],
                database_ = database_name,
            )
        elif end_node_id[0:3] == 'sol':
            # 如果这个答案里找到了原因，就把解决方案连接到这个答案里的所有原因上
            relationship_order = 4
            driver.execute_query(
                "MATCH (a:Reason {id: $start_node_id}), (b:Solution {id: $end_node_id})"
                "MERGE (a)-[:can_be_possibly_solved_by {key: $keyword}]->(b)",
                start_node_id = start_node_id, end_node_id=end_node_id, 
                keyword = keywords[relationship_order],
                database_ = database_name,
            )

def add_relationship_complex(driver:Driver, start_node_id:str, end_node_id:str):
    merge_request = " CALL apoc.merge.relationship(a, $relationship_, {key: $keyword}, {}, b, {}) YIELD rel RETURN rel "
    reverse_merge_request = " CALL apoc.merge.relationship(b, $reverse_relationship, {key: $reverse_keyword}, {}, a, {}) YIELD rel RETURN rel "
    dict_1_str = "{'start_node_id': start_node_id, 'end_node_id': end_node_id,'relationship_': relationships[relationship_order], 'keyword': keywords[relationship_order]}"
    dict_2_str = """{'start_node_id': start_node_id, 'end_node_id': end_node_id,'relationship_': relationships[relationship_order], 
    'reverse_relationship': relationships[reverse_relationship_order], 'keyword': keywords[relationship_order], 'reverse_keyword': keywords[reverse_relationship_order]}"""
    if start_node_id[0:3] == 'ind':
        if end_node_id[0:3] == 'des':
            relationship_order = 0
            reverse_relationship_order = 5
            driver.execute_query(
                query_ = "MATCH (a:Index {id: $start_node_id}), (b:Description {id: $end_node_id})" + merge_request,
                parameters_ = eval(dict_2_str), 
                database_ = database_name,
            )
            driver.execute_query(
                query_ = "MATCH (a:Index {id: $start_node_id}), (b:Description {id: $end_node_id})" + reverse_merge_request,
                parameters_ = eval(dict_2_str), 
                database_ = database_name,
            )
        elif end_node_id[0:3] == 'log':
            relationship_order = 1
            driver.execute_query(
                query_ = "MATCH (a:Index {id: $start_node_id}), (b:Log {id: $end_node_id})" + merge_request,
                parameters_ = eval(dict_1_str), 
                database_ = database_name,
            )
        elif end_node_id[0:3] == 'tag':
            relationship_order = 2
            reverse_relationship_order = 6
            driver.execute_query(
                query_ = "MATCH (a:Index {id: $start_node_id}), (b:Tag {id: $end_node_id})" + merge_request,
                parameters_ = eval(dict_2_str),
                database_ = database_name,
            )
            driver.execute_query(
                query_ = "MATCH (a:Index {id: $start_node_id}), (b:Tag {id: $end_node_id})" + reverse_merge_request,
                parameters_ = eval(dict_2_str),
                database_ = database_name,
            )
        elif end_node_id[0:3] == 'rea':
            relationship_order = 3
            driver.execute_query(
                "MATCH (a:Index {id: $start_node_id}), (b:Reason {id: $end_node_id})" + merge_request,
                parameters_ = eval(dict_1_str), 
                database_ = database_name,
            )
        elif end_node_id[0:3] == 'sol':
            # 如果这个答案里没有找到原因，就把解决方案直接连接到故障索引上
            relationship_order = 4
            driver.execute_query(
                "MATCH (a:Index {id: $start_node_id}), (b:Solution {id: $end_node_id})" + merge_request,
                parameters_ = eval(dict_1_str),
                database_ = database_name,
            )
    elif start_node_id[0:3] == 'des':
        if end_node_id[0:3] == 'des':
            relationship_order = reverse_relationship_order = 7
            driver.execute_query(
                "MATCH (a:Description {id: $start_node_id}), (b:Description {id: $end_node_id})" + merge_request,
                parameters_ = eval(dict_2_str),
                database_ = database_name,
            )
            driver.execute_query(
                "MATCH (a:Description {id: $start_node_id}), (b:Description {id: $end_node_id})" + reverse_merge_request,
                parameters_ = eval(dict_2_str),
                database_ = database_name,
            )
    elif start_node_id[0:3] == 'log':
        if end_node_id[0:3] == 'log':
            relationship_order = reverse_relationship_order = 8
            driver.execute_query(
                "MATCH (a:Log {id: $start_node_id}), (b:Log {id: $end_node_id})" + merge_request,
                parameters_ = eval(dict_2_str),
                database_ = database_name,
            )
            driver.execute_query(
                "MATCH (a:Log {id: $start_node_id}), (b:Log {id: $end_node_id})" + reverse_merge_request,
                parameters_ = eval(dict_2_str),
                database_ = database_name,
            )
    elif start_node_id[0:3] == 'rea':
        if end_node_id[0:3] == 'rea':
            relationship_order = 7
            driver.execute_query(
                "MATCH (a:Reason {id: $start_node_id}), (b:Reason {id: $end_node_id})" + merge_request,
                parameters_ = eval(dict_2_str),
                database_ = database_name,
            )
            driver.execute_query(
                "MATCH (a:Reason {id: $start_node_id}), (b:Reason {id: $end_node_id})" + reverse_merge_request,
                parameters_ = eval(dict_2_str),
                database_ = database_name,
            )
        elif end_node_id[0:3] == 'sol':
            # 如果这个答案里找到了原因，就把解决方案连接到这个答案里的所有原因上
            relationship_order = 4
            driver.execute_query(
                "MATCH (a:Reason {id: $start_node_id}), (b:Solution {id: $end_node_id})" + merge_request,
                parameters_ = eval(dict_1_str),
                database_ = database_name,
            )

def add_relationship_between_log_nodes(driver:Driver, intersection_info_dict:dict):
    index1 = intersection_info_dict['index1']
    log_order1 = intersection_info_dict['log_order1']
    index2 = intersection_info_dict['index2']
    log_order2 = intersection_info_dict['log_order2']
    intersection = intersection_info_dict['intersection']
    intersection_size = intersection_info_dict['intersection_size']
    start_node_id = 'log_' + index1 + f' ({log_order1})'
    print(start_node_id)
    end_node_id = 'log_' + index2 + f' ({log_order2})'
    print(end_node_id)
    relationship_order = 8
    driver.execute_query(
        "MATCH (a:Log {id: $start_node_id}), (b:Log {id: $end_node_id})"
        "MERGE (a)-[:shares_same_log_template_with {key: $keyword, intersection: $intersection, intersection_size: $intersection_size}]->(b)"
        "MERGE (b)-[:shares_same_log_template_with {key: $keyword, intersection: $intersection, intersection_size: $intersection_size}]->(a)",
        start_node_id = start_node_id, end_node_id = end_node_id, 
        intersection = intersection, intersection_size = intersection_size,
        keyword = keywords[relationship_order],
        database_ = database_name,
    )

def print_nodes_with_relation_to(driver:Driver, node_id:str):
    records, _, _ = driver.execute_query(
        "MATCH ({id: $node_id})-[rel]->(node)"
        "RETURN type(rel) AS relationship, node AS node",
        node_id = node_id, 
        database_=database_name, routing_ = RoutingControl.READ,
    )
    for record in records:
        print(record["relationship"] + '\t' + record["node"])

def match_specific_type_nodes(tx, name_filter):
    result = tx.run("""
        MATCH (p:Person) WHERE p.name STARTS WITH $filter
        RETURN p.name AS name ORDER BY name
        """, filter=name_filter)
    return list(result)  # return a list of Record objects

def get_database_info(knowledge_extract_result_file_csv_path:str) -> str:
    knowledge_extract_result_file = open(knowledge_extract_result_file_csv_path, 'r',encoding='utf-8',newline='')
    reader = csv.reader(knowledge_extract_result_file)
    row_list = list(reader)
    row_list = row_list[0:csv_row_limit]
    end_index = ''

    row_number = len(row_list)
    for row_i in range(1,row_number):
        row = row_list[row_i][0]
        question_info = eval(row)       # eval将string转为dict
        index = question_info['index']
        description = question_info['title']
        logs = question_info['logs']
        tags = question_info['tags']
        id_base = index
        index_id = 'index_' + id_base
        description_id = 'description_' + id_base
        logs_id_base = 'log_' + id_base
        tags_id_base = 'tag_'
        solution_id_base = 'solution_' + id_base
        reason_id_base = 'reason_' + id_base

        if not is_node_exist(driver,index_id):
            add_node(driver, index_id, labels[0], index)
        if not is_node_exist(driver, description_id):
            add_node(driver, description_id, labels[1], description)
        add_relationship(driver, index_id, description_id)
        # add_one_way_relationship(driver, description_id, 5, index_id)
        end_index = index
        
        log_number = len(logs)
        for log_i in range(0, log_number):
            log = logs[log_i]
            log_id = logs_id_base + f' ({log_i + 1})'
            
            if not is_node_exist(driver, log_id):
                add_node(driver, log_id, labels[2], log)
                # print(log_id)
            add_relationship(driver, index_id, log_id)
            # add_one_way_relationship(driver, description_id, 5, index_id)
            
        tag_number = len(tags)
        for tag_i in range(0,tag_number):
            tag = tags[tag_i]
            tag_id = tags_id_base + tag
            if not is_tag_node_exist(driver, tag):
                add_node(driver, tag_id, labels[3], tag)
                print(tag_id)
            add_relationship(driver, index_id, tag_id)

        answer_number = len(row_list[row_i]) - 1
        for ans_i in range(1, answer_number + 1):
            answer = row_list[row_i][ans_i]
            answer_info = eval(answer)
            solution = answer_info['solution_md']
            reason = answer_info['combined_reason']

            # reason_number = len(reasons)
            is_reason_exist = bool(len(reason))
            is_solution_exist = bool(solution != '')
            solution_id = solution_id_base + f' ({ans_i})'
            reason_id = reason_id_base + f' ({ans_i})'
            if is_solution_exist:
                if not is_node_exist(driver, solution_id):
                    add_node(driver, solution_id, labels[5], solution)
            else:
                if is_node_exist(driver, solution_id):
                    remove_node(driver, solution_id)
            pass
        
            if is_reason_exist:
                if not is_node_exist(driver, reason_id):
                    add_node(driver, reason_id, labels[4], reason)
            if is_reason_exist and is_solution_exist:
                add_relationship(driver, index_id, reason_id)
                add_relationship(driver, reason_id, solution_id)
            elif (not is_reason_exist) and is_solution_exist:
                add_relationship(driver, index_id, solution_id)
            elif is_reason_exist and (not is_solution_exist):
                add_relationship(driver, index_id, reason_id)
            else:
                pass
    return end_index

            
            # for reason_i in range(0,reason_number):
            #     reason = reasons[reason_i]
            #     reason_id = reason_id_base + f' ({ans_i}, {reason_i + 1})'
            #     # if not is_node_exist(driver, reason_id):
            #     #     add_node(driver, reason_id, labels[4], reason)
            #     if is_node_exist(driver, reason_id):
            #         remove_node(driver, reason_id)

                # add_relationship(driver, index_id, reason_id)
            
                # solution_id = solution_id_base + f' ({ans_i})'
                # if not is_node_exist(driver, solution_id):
                #     add_node(driver, solution_id, labels[5], solution)
                # add_relationship(driver, reason_id, solution_id)

            # if reason_number == 0:
            #     add_relationship(driver, index_id, solution_id)

def get_log_info(template_for_neo4j_csv_path:str, end_index:str):

    dict_txt_file_path = template_for_neo4j_csv_path.replace('.csv','.txt')
    dict_file = open(dict_txt_file_path, 'r', encoding='utf-8', newline='')
    template_miner_file = open(template_for_neo4j_csv_path, 'r', encoding='utf-8', newline='')
    reader = csv.reader(template_miner_file)
    row_list = list(reader)
    row_number = len(row_list)
    for row_i in range(1,row_number):
        row1 = row_list[row_i]
        log_number = len(row1) - 1
        index = row_list[row_i][0].strip()

        if is_index_in_range(index, end_index):
            for log_i in range(1,log_number + 1):
                template = row_list[row_i][log_i].strip()
                template_list = template.strip().split('\n')
                template_list = [t.strip() for t in template_list if t != '']
                node_id = 'log_' + index + f' ({log_i})'
                add_log_node_info(driver, node_id, template_list)
        else:
            break

    dict_lines = dict_file.readlines()
    for dict_line in dict_lines:
        dict_line = dict_line.strip()
        info_dict = eval(dict_line)
        current_index1 = info_dict['index1']
        current_index2 = info_dict['index2']
        if is_index_in_range(current_index1, end_index) and is_index_in_range(current_index2, end_index):
            add_relationship_between_log_nodes(driver, info_dict)
        else:
            break
    
def link_similar_text(similarity_template_folder:str,train_filename:str, end_index:str):
    mean_similarity_filepath = similarity_template_folder + train_filename.replace('.txt','') + '_mean_similarity.txt'
    if not os.path.exists(mean_similarity_filepath):
        return
    mean_similarity_file = open(mean_similarity_filepath, 'r', encoding='utf-8', newline='')
    index_list = mean_similarity_file.readlines()
    index_list = [eval(i.strip()) for i in index_list]
    index_number = len(index_list)
    merge_request = " CALL apoc.merge.relationship(a, $relationship_, {key: $keyword, mean_similarity: $mean_similarity}, {}, b, {}) YIELD rel RETURN rel "
    for i in range(0,index_number):
        index_similarity = index_list[i]

        mean_similarity = index_similarity[-1]
        threshold = 0.7
        if train_filename == 'reason.txt':
            threshold = 0.7
        elif train_filename == 'description.txt':
            threshold = 0.7
        if mean_similarity >= threshold:
            index1 = index_similarity[0]
            index2 = index_similarity[1]
            if is_index_in_range(index1, end_index) and is_index_in_range(index2, end_index):
                graph_index1 = train_filename.replace('.txt','') + '_' + index1
                graph_index2 = train_filename.replace('.txt','') + '_' + index2
                
                driver.execute_query(
                    query_='MATCH (a {id: $index1}),(b {id: $index2})' + merge_request,
                    parameters_={'index1': graph_index1, 'index2': graph_index2, 'relationship_': relationships[7], 'keyword': keywords[7], 'mean_similarity': mean_similarity},
                    database_ = database_name,
                )

                driver.execute_query(
                    query_='MATCH (a {id: $index2}),(b {id: $index1})' + merge_request,
                    parameters_={'index1': graph_index1, 'index2': graph_index2, 'relationship_': relationships[7], 'keyword': keywords[7], 'mean_similarity': mean_similarity},
                    database_ = database_name,
                )
            
centre_number = 5
csv_row_limit = centre_number + 1

labels = ['Index', 'Description', 'Log', 'Tag', 'Reason', 'Solution']

relationships = ['is_described_as', 'has_log_as', 'is_tagged_with', 'is_possibly_caused_by', 'can_be_possibly_solved_by', 
                 'refer_to_index_as', 'include_index_as', 'has_textual_similarity_with', 'shares_same_log_template_with']
keywords = ['has_description', 'has_log', 'has_tag', 'has_reason', 'has_solution', 
            'to_index', 'include_index', 'is_similar', 'share_log_template']
# 0: has_description
# 1: has_log
# 2: has_tag
# 3: has_reason
# 4: has_solution
# 5: to_index(to only one)
# 6: include_index(to possibly multiple)
# 7: is_similar(both ways)
# 8: share_log_template(both_ways)

def main():
    # Delete
    clear_graph(driver)
    # Build
    end_index = get_database_info('data_test_log.csv')
    get_log_info('.\\log_template_extract\\log_train_neo4j.csv',end_index)
    folder = '.\\similarity_template\\'
    link_similar_text(folder, 'description.txt', end_index)
    link_similar_text(folder, 'reason.txt', end_index)

# command = 'Powershell neo4j stop'
# subprocess.run(command)

# # Backup Current database
# word_dir = os.getcwd().replace('\\','/')
# dump_command = 'Powershell neo4j-admin database dump neo4j --to-path='+ word_dir +'/neo4j_library_5.10/ --overwrite-destination=True'
# subprocess.run(dump_command, timeout = 2000)
# name = '.\\neo4j_library_5.10\\neo4j.dump'
# os.rename(name, name.replace('.dump','-backup.dump'))

# command = 'Powershell neo4j console'
# subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

with GraphDatabase.driver(URI, auth=AUTH, trusted_certificates = neo4j.TrustAll()) as driver:
    driver.verify_connectivity()
    session = driver.session()
    # graph = Graph(URI, AUTH)

    if __name__ == '__main__':
        main()



# command_set = 'Powershell $wshell = New-Object -ComObject wscript.shell'
# command_send = 'Powershell $wshell.SendKeys("{Control}({C})")'
# os.system(command = command)

