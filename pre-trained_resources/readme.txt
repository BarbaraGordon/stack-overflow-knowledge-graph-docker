pre-trained_resources
file specification:
stop_word_archive.zip 	-- stopwords downloaded from kaggle
default.zip 			-- pre-trained model of stanza version 1.5.0 English
en_ewt_models.zip 		-- pre-trained model of latest stanfordnlp(the older version of stanza stopped maintaining) English
resources_1.5.0.json 	-- pre-trained model index for stanza version 1.5.0
SimLex-999.zip			-- please check for detailed information on the project's homepage <https://fh295.github.io/simlex.html>