# %% import
import os
import math
# spider libraries
import requests
import scrapy
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as BS
import bs4
import lxml
from lxml import etree
from lxml import html
from lxml.html import fromstring, tostring
from html.parser import HTMLParser

def get_answer_page_total(etree_parser_result:etree._Element) -> int:
    answer_number_str = etree_parser_result.xpath('//*[@class="answers-subheader d-flex ai-center mb8"]//span/text()')
    answer_number_str = ''.join(answer_number_str).strip()
    answer_number = int(answer_number_str)
    print('%d' %(answer_number))
    answer_page_total = math.ceil(answer_number/30)
    return answer_page_total        # 得到这个问题的总页数

# %% 类定义
# class StackoverflowItem(scrapy.Item):
#     index = scrapy.Field()
#     content = scrapy.Field()

folderpath = os.getcwd() + '\\data_docker\\'
path_exist = os.path.exists(folderpath)
if not path_exist:
    os.makedirs(folderpath)

# %% 遍历爬取页面
# NOTE - 设置请求url

tag = 'docker'     # 要查询的Tag
start_page = 69
end_page = 100
page_range = range(start_page,end_page + 1)
page_number = end_page - start_page + 1
question_rank = 1

_url = 'https://stackoverflow.com/questions/tagged/'+tag+'?tab=votes&page={page}&pagesize=50'
# _url = 'https://stackoverflow.com/questions/tagged/'+tag+'?tab=Active&page={page}&pagesize=50'

urls = [_url.format(page = page) for page in page_range]

# NOTE - 爬取
for page in page_range:
    _url = urls[page-start_page]
    headers = {'User-Agent':UserAgent().random}

    request_summary_page = requests.get(_url,headers = headers)
    html_summary = request_summary_page.text    # 纯文本
    # print(html)

    # %% selector xpath
    selector_summary = etree.HTML(html_summary,parser = None)
    questions = selector_summary.xpath('//*[@class="s-post-summary    js-post-summary"]')  # 如何找到questions所在的块
    question_per_page = len(questions)

    for question_number in range(1,question_per_page+1):
        filename = str(page).zfill(3) + '-' + str(question_number).zfill(2) + '.html'   # 固定位数：xxx-xx.html
        filepath = folderpath + filename
        if os.path.exists(filepath):
            continue
        print('Start requesting page %d - %d' %(page,question_number))
        question = questions[question_number-1]
        
        question_relative_path = question.xpath('.//h3/a/@href')[0]
        question_url = 'https://stackoverflow.com' + question_relative_path
        # print(question_url)

        request_detail_page = requests.get(question_url,headers = headers)
        html_detail = request_detail_page.text  # 纯文本
        selector_detail = etree.HTML(html_detail,parser = None) # selector
        # print(html_detail)


        html_detail_byte = html_detail.encode() # 把str格式转化成byte格式，否则无法写入文件
        filename = str(page).zfill(3) + '-' + str(question_number).zfill(2) + '.html'   # 固定位数：xxx-xx.html
        filepath = folderpath + filename
        # 存储格式：页数-问题序号-问题详情所在url

        # f = open(filepath,'w')

        with open(filepath,'wb') as f:
            f.write(html_detail_byte)
            f.close()

        # %% 如果答案不止一页（一页可以容纳30个答案），用总答案数判断页数
        answer_page_number = get_answer_page_total(selector_detail)
        # answer_number_str = selector_detail.xpath('//*[@class="answers-subheader d-flex ai-center mb8"]//span/text()')
        # answer_number_str = ''.join(answer_number_str).strip()
        # answer_number = int(answer_number_str)
        # print('%d' %(answer_number))

        if answer_page_number > 1:      # 答案大于一页的情况
            for answer_page_order in range(2, answer_page_number + 1):
                answer_url_after = question_url + '?page=' + str(answer_page_order)    # 默认排序方式是获得分数最高的答案
                request_answer_page_after = requests.get(answer_url_after,headers = headers)
                html_detail_after = request_answer_page_after.text
                html_detail_after_byte = html_detail_after.encode()
                filename_after = filename.strip('.html') + '-' + str(answer_page_order).zfill(1) + '.html'
                    # xxx-xx-x.html
                filepath_after = folderpath + filename_after
                with open(filepath_after,'wb') as f:
                    f.write(html_detail_after_byte)
                    f.close()

        # item = StackoverflowItem()
        # item['index'] = page
        # item['content'] = selector_detail.xpath('/html')[0]     # <Element html at 0x2c781c4b600>
        # print(item['content'])

        question_rank += 1

'''
# %% 遍历爬取页面
# NOTE - 设置请求url

tag = 'java'     # 要查询的Tag
start_page = 1
end_page = 5
page_range = range(start_page,end_page + 1)
page_number = end_page - start_page + 1
question_rank = 1

_url = 'https://stackoverflow.com/questions/tagged/'+tag+'?tab=votes&page={page}&pagesize=50'
# _url = 'https://stackoverflow.com/questions/tagged/'+tag+'?tab=Active&page={page}&pagesize=50'

urls = [_url.format(page = page) for page in page_range]

# NOTE - 爬取
for page in page_range:
    _url = urls[page-start_page]
    headers = {'User-Agent':UserAgent().random}

    request_summary_page = requests.get(_url,headers = headers)
    html_summary = request_summary_page.text    # 纯文本
    # print(html)

    # %% selector xpath
    selector_summary = etree.HTML(html_summary,parser = None)
    questions = selector_summary.xpath('//*[@class="s-post-summary    js-post-summary"]')  # 如何找到questions所在的块
    question_per_page = len(questions)

    for question_number in range(1,question_per_page+1):
        question = questions[question_number-1]
        
        question_relative_path = question.xpath('.//h3/a/@href')[0]
        question_url = 'https://stackoverflow.com' + question_relative_path
        # print(question_url)

        request_detail_page = requests.get(question_url,headers = headers)
        html_detail = request_detail_page.text  # 纯文本
        selector_detail = etree.HTML(html_detail,parser = None) # selector
        # print(html_detail)


        html_detail_byte = html_detail.encode() # 把str格式转化成byte格式，否则无法写入文件
        filename = str(page).zfill(3) + '-' + str(question_number).zfill(2) + '.html'   # 固定位数：xxx-xx.html
        filepath = folderpath + filename
        # 存储格式：页数-问题序号-问题详情所在url

        # f = open(filepath,'w')

        with open(filepath,'wb') as f:
            f.write(html_detail_byte)
            f.close()

        # %% 如果答案不止一页（一页可以容纳30个答案），用总答案数判断页数
        answer_page_number = get_answer_page_total(selector_detail)
        # answer_number_str = selector_detail.xpath('//*[@class="answers-subheader d-flex ai-center mb8"]//span/text()')
        # answer_number_str = ''.join(answer_number_str).strip()
        # answer_number = int(answer_number_str)
        # print('%d' %(answer_number))

        if answer_page_number > 1:      # 答案大于一页的情况
            for answer_page_order in range(2, answer_page_number + 1):
                answer_url_after = question_url + '?page=' + str(answer_page_order)    # 默认排序方式是获得分数最高的答案
                request_answer_page_after = requests.get(answer_url_after,headers = headers)
                html_detail_after = request_answer_page_after.text
                html_detail_after_byte = html_detail_after.encode()
                filename_after = filename.strip('.html') + '-' + str(answer_page_order).zfill(1) + '.html'
                    # xxx-xx-x.html
                filepath_after = folderpath + filename_after
                with open(filepath_after,'wb') as f:
                    f.write(html_detail_after_byte)
                    f.close()

        # item = StackoverflowItem()
        # item['index'] = page
        # item['content'] = selector_detail.xpath('/html')[0]     # <Element html at 0x2c781c4b600>
        # print(item['content'])

        question_rank += 1
'''