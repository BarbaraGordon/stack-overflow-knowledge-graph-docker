import stanza
import nltk
from nltk.corpus import stopwords
from nltk import download
import jieba

import mteb

import gensim
from gensim import utils, corpora, models, similarities
from gensim.test.utils import datapath, common_corpus, common_dictionary
from gensim.models import word2vec, KeyedVectors
from gensim.models.ldamodel import LdaModel
from gensim.models.coherencemodel import CoherenceModel
from gensim.similarities import WmdSimilarity
# import word2vec

from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.feature_extraction.text import TfidfTransformer
# from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from sentence_transformers import SentenceTransformer

import csv
import json
import itertools
from string import punctuation
import re
import math
import numpy as np
from numpy import reshape, shape, empty, zeros
from scipy.sparse import coo_matrix
from scipy import spatial
from scipy.spatial.distance import pdist
from scipy.linalg import norm
import scipy

import warnings
warnings.filterwarnings("ignore")

# download('stopwords')  # Download stopwords list.
stop_words = stopwords.words('english')

LOGS = False  # Set to True if you want to see progress in logs.
if LOGS:
    import logging
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# 使用nltk对句子分词，并去掉特殊符号全部转化为小写后转化成一个字符串输出
def tokenize_doc(document:str) -> str:
    # 输入的一定是没有换行符的文本
    document = re.sub('\n',' ',document)
    number_pattern = re.compile('[0-9]+')
    document = re.sub(number_pattern, '', document)
    # print(document)

    output = ''
    words = []
    # use stanza
    # words = stanza.Pipeline(processors='tokenize', lang='en')

    # use nltk
    sentences = nltk.sent_tokenize(document)
    for sentence in sentences:
        # sentence_letter = re.sub('[\W 0-9]+',' ',sentence)   # 去掉所有非字母字符（包括数字）
        sentence_letter = re.sub('[\W]+',' ',sentence)   # 去掉所有非字母和数字的字符
        words_origin = nltk.word_tokenize(sentence_letter, language='english')
        words.extend([word.lower() for word in words_origin if word not in punctuation])  # remove the punctuation in words
        # 得到的都是小写字母
        # for word in words_origin:
        #     if word not in punctuation:
    result = ' '.join(words)
    return result.strip()  # join连接的单词由前面的空格连接。

def clean_sentence(sentence:str) -> list:
    stopwords = stop_words
    list = [word for word in sentence.lower().split() if word not in stopwords]
    return list

def get_stopword_list(train_file_folder:str) -> list:
    # stoplist = set('for a an of or the and to in'.split())      # 停用词表，不计入统计
    f = open(train_file_folder + 'stopwords.txt','r',encoding='utf-8')
    stopwords = [word.strip() for word in f.readlines()]
    f.close()
    return stopwords

def get_sentences_list(train_file_folder:str,train_filename:str) -> list:
    f = open(train_file_folder + train_filename,'r',encoding='utf-8')
    list = [sentence.strip() for sentence in f.readlines()]
    f.close()
    return list

# 从页面抽取知识得到的csv结果文件中读取所有的故障描述（只有标题），然后分词生成训练文件
def get_title_train_file(knowledge_extract_result_file_csv_path:str, train_file_output_txt_path:str) -> list:
    knowledge_extract_result_file = open(knowledge_extract_result_file_csv_path, 'r',encoding='utf-8',newline='')
    reader = csv.reader(knowledge_extract_result_file)
    row_list = list(reader)
    description_train_file = open(train_file_output_txt_path,'w',encoding='utf-8',newline='')

    origin_file_output_txt_path = train_file_output_txt_path.replace('.txt', '_origin.txt')
    description_origin_file = open(origin_file_output_txt_path,'w',encoding='utf-8',newline='')

    clean_file_output_txt_path = train_file_output_txt_path.replace('.txt', '_clean.txt')
    description_clean_file = open(clean_file_output_txt_path,'w',encoding='utf-8',newline='')

    description_list = []

    row_number = len(row_list)
    for row_i in range(1,row_number):
        row = row_list[row_i][0]
        question_info = eval(row)       # eval将string转为dict
        title = question_info['title']
        if len(title) > 0:
            description_origin_file.write(title)

            token_description = tokenize_doc(title)
            description_train_file.write(token_description)

            clean = clean_sentence(token_description)
            clean_sent = ' '.join(clean)
            description_clean_file.write(clean_sent)
            if row_i < row_number - 1:         # 最后一个标题不用换行。
                description_train_file.write('\n')
                description_origin_file.write('\n')
                description_clean_file.write('\n')
            description_list.append(token_description)
    knowledge_extract_result_file.close()
    description_train_file.close()
    return description_list

# 从页面抽取知识得到的csv结果文件中读取所有的故障原因，然后分词生成训练文件
def get_reason_train_file(knowledge_extract_result_file_csv_path:str, train_file_output_txt_path:str) -> list:
    knowledge_extract_result_file = open(knowledge_extract_result_file_csv_path, 'r',encoding='utf-8',newline='')
    reader = csv.reader(knowledge_extract_result_file)
    row_list = list(reader)
    reason_train_file = open(train_file_output_txt_path,'w',encoding='utf-8',newline='')

    origin_file_output_txt_path = train_file_output_txt_path.replace('.txt','') + '_origin.txt'
    reason_origin_file = open(origin_file_output_txt_path,'w',encoding='utf-8',newline='')

    clean_file_output_txt_path = train_file_output_txt_path.replace('.txt','') + '_clean.txt'
    reason_clean_file = open(clean_file_output_txt_path,'w',encoding='utf-8',newline='')

    reason_list = []

    row_number = len(row_list)
    for row_i in range(1,row_number):
        answer_number = len(row_list[row_i]) - 1
        for ans_i in range(1,answer_number + 1):
            ans = row_list[row_i][ans_i]
            answer_info = eval(ans)       # eval将string转为dict
            reasons = answer_info['possible_reasons']

        # %% 方案1：将一个答案中的所有可能原因合并为一个句子
            # reason = ' '.join(reasons)
            # if len(reason) > 0:
            #     reason_train_file.write(tokenize_doc(reason))
            #     if row_i < row_number - 1 and ans_i < answer_number - 1:         # 最后一个标题不用换行。
            #         reason_train_file.write('\n')
            
        # %% 方案2：一个答案中的原因按照列表，一行一个，不合并
            reason_number = len(reasons)
            for res_i in range(0,reason_number):
                reason_i = reasons[res_i]

                reason_origin_file.write(reason_i)

                token_reason_i = tokenize_doc(reason_i)
                reason_train_file.write(token_reason_i)

                clean = clean_sentence(token_reason_i)
                clean_sent = ' '.join(clean)
                reason_clean_file.write(clean_sent)

                if row_i < row_number - 1 and ans_i < answer_number - 1 and res_i < reason_number - 1:
                    reason_train_file.write('\n')
                    reason_origin_file.write('\n')
                    reason_clean_file.write('\n')

                reason_list.append(token_reason_i)

    knowledge_extract_result_file.close()
    reason_train_file.close()
    reason_origin_file.close()
    return reason_list

# NOTE- %% sklearn_tf_idf

# 使用TF-IDF计算两个句子的相似度，语料库仅为两句话
def sklearn_tf_idf_similarity(sentence1:str,sentence2:str) -> float:
    # 分词、去掉特殊符号、转化为小写处理
    sentence1 = tokenize_doc(sentence1)
    sentence2 = tokenize_doc(sentence2)
    # 转化为TF矩阵
    cv = CountVectorizer(tokenizer=lambda s: s.split())
    corpus = [sentence1, sentence2]
    vectors = cv.fit_transform(corpus)
    vectors_array = vectors.A
    # 计算TF系数
    return np.dot(vectors_array[0], vectors_array[1]) / (norm(vectors_array[0]) * norm(vectors_array[1]))

# NOTE- %% gensim_ti_idf

def get_valid_words(train_file_folder:str,train_filename:str) -> list:
    train_file_path = train_file_folder + train_filename
    # stopwords = get_stopword_list(train_file_folder)
    stopwords = stop_words
    train_list = []
    
    f = open(train_file_path,'r',encoding='utf-8')
    for line in f.readlines():
        train_words = []
        sentence = tokenize_doc(line)
        words = sentence.split()
        for train_word in words:
            if train_word.strip() not in stopwords:
                train_words.append(train_word.strip())
        # train_words = [train_word.strip() for train_word in sentence.split() if train_word not in stopwords]
        train_list.append(train_words)
    dictionary = corpora.Dictionary(train_list)
    dictionary.save(train_file_folder + '%s.dict' %(train_filename.replace('.txt',''))) # 把字典保存起来，方便以后使用

    return train_list

def gensim_ti_idf_similarity(sentence1:str,sentence2:str,train_file_folder:str,train_filename:str) -> float:
    dictionary = corpora.Dictionary.load(train_file_folder + '%s.dict' %(train_filename.replace('.txt','')))
    sentence1 = tokenize_doc(sentence1)
    sentence2 = tokenize_doc(sentence2)
    bow1 = dictionary.doc2bow(sentence1.split())
    bow2 = dictionary.doc2bow(sentence2.split())
    compared_vectors = [bow1, bow2]

    train_list = get_valid_words(train_file_folder,train_filename)
    corpus = [dictionary.doc2bow(text) for text in train_list]
    tfidf = gensim.models.TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]
    new_vector_tfidf_ls = [tfidf[vector] for vector in compared_vectors]  # 将要比较文档转换为tfidf表示方法

    index = similarities.MatrixSimilarity(corpus_tfidf)
    sims = [index[new_vector_tfidf] for new_vector_tfidf in new_vector_tfidf_ls]
    similarity_float = np.dot(sims[0], sims[1]) / (norm(sims[0]) * norm(sims[1]))
    similarity = float(1 - spatial.distance.cosine(sims[0], sims[1]))

    return similarity_float

# NOTE- gensim word2vec

def gensim_word2vec_similarity(sentence1:str,sentence2:str,train_file_folder:str,train_filename:str) -> float:
    # stopwords = stop_words
    # sentence_cleaned_list = []
    sentence_cleaned_list_separate = []
    s1 = clean_sentence(sentence1)
    s2 = clean_sentence(sentence2)
    # s_1 = ' '.join(s1)
    # s_2 = ' '.join(s2)
    # word_list = get_valid_words(train_file_folder,train_filename)
    sentence_list = get_sentences_list(train_file_folder,train_filename)
    for sentence in sentence_list:
        cs_word_list = clean_sentence(sentence)
        # cs = ' '.join(cs_word_list)
        # sentence_cleaned_list.append(cs)
        sentence_cleaned_list_separate.append(cs_word_list)


    # min_count = round(len(list(itertools.chain(*word_list)))/3000000)
    min_count = 0
    # model = models.Word2Vec(sentences = sentence_cleaned_list, min_count = min_count)
    model = models.Word2Vec(sentences = sentence_cleaned_list_separate, min_count = min_count)
    # model.wv.evaluate_word_pairs(datapath('wordsim353.tsv'))
    # model.save(train_file_folder + 'gensim_%s_word2vec_train_model.model'  %(train_filename.replace('.txt','')))

    distance_gensim = model.wv.n_similarity(s1,s2)

    # return distance
    
    shape1 = shape(model.wv[s1[0]])
    v1 = zeros(shape = shape1)
    n = 0
    for w in s1:
        v1 += model.wv[w]
        n += 1
    sv1 = v1 / n
    
    shape2 = shape(model.wv[s2[0]])
    v2 = zeros(shape = shape2)
    n = 0
    for w in s2:
        v2 += model.wv[w]
        n += 1
    sv2 = v2 / n

    distance_scipy = float(1 - spatial.distance.cosine(sv1, sv2))
    distance_sklearn = cosine_similarity(sv1.reshape((-1, 1)),sv2.reshape((-1, 1)))[0][0]

    return float(distance_gensim)

    # Similarity = model.wmdSimilarity(s_1, s_2)
    # index[s1]
    # index[s2]
    
    # index = WmdSimilarity(word_list,model)
    # sims_int = np.dot(index[s1], index[s2]) / (norm(index[s1]) * norm(index[s2]))
    # pass

# %% NOTE- %% word2vec word2vec

# word2vec模型训练
def train_word2vec_model(train_file_folder:str, train_filename:str) -> None:
    # 获取训练数据集
    train_file_path = train_file_folder + train_filename
    model_output_path = train_file_folder + train_filename.replace('.txt','') + '_word2vec_train_model.model'
    train_file = open(train_file_path)
    sentences = train_file.readlines()

    x_train = []

    for sentence in sentences:
        word_list = sentence.strip().split(' ')
        for i in range(0, len(word_list)):
            word_list[i] = word_list[i].strip()
        x_train.append(word_list)

    model = word2vec.Word2Vec(x_train, sg=1, window=3, min_count=1, workers=4)
    model.save(model_output_path)
    model.wv.save_word2vec_format(model_output_path+'_format')  # 顺便保存一份可视化模型以便人工查看

    train_file.close()

# 根据word2Vec模型获取句子向量
def get_vec(train_file_folder:str, train_filename:str, sentence):
    size = 200
    model_path = train_file_folder + train_filename.replace('.txt','') + '_word2vec_train_model.model'
    model = word2vec.Word2Vec.load(model_path)
    words = tokenize_doc(sentence).strip().split(' ')
    sentence_vec = np.zeros(size)
    sentence_word_length = 0

    for word in words:
        try:
            # sentence_vec += model.wv[word]
            vec = model.wv[word]
            sentence_vec += vec.reshape(vec.shape[0],1)
        except KeyError:
            sentence_vec += np.zeros(size)
        sentence_word_length += 1

    if sentence_word_length > 0:
        sentence_vec /= sentence_word_length

    return sentence_vec

# 计算两个句子向量的相似度 sentence_type取0代表故障描述,取1代故障原因
def word2vec_sim_cal(train_file_folder:str, train_filename:str, sentence1:str, sentence2:str):
    sentence1_vec = get_vec(train_file_folder, train_filename, sentence1)
    sentence2_vec = get_vec(train_file_folder, train_filename, sentence2)

    cosine = pdist([sentence1_vec, sentence2_vec], 'euclidean')
    euclidean = pdist([sentence1_vec, sentence2_vec], 'cosine')
    jaccard = pdist([sentence1_vec, sentence2_vec], 'jaccard')

    dis = float((cosine + euclidean + jaccard) / 3)
    return dis

# %% TODO - BERT sentence_transformers 下不下来预训练模型

def sentence_transformers_bert_similarity(sentence1:str,sentence2:str):
    sentences = [sentence1, sentence2]
    # model = SentenceTransformer('bert-base-nli-mean-tokens')
    model = SentenceTransformer('all-MiniLM-L6-v2')
    sentence_embeddings = model.encode(sentences)
    sim = cosine_similarity(sentence_embeddings[0],sentence_embeddings[1])
    return sim

# NOTE- %% BERT transformer

# TODO - 下不下来预训练模型

# NOTE- %% gensim lda

# lda主题模型，不太适用
def train_lda_model(train_file_folder:str,train_filename:str) -> None:
    train_list = get_valid_words(train_file_folder,train_filename)
    # 构建词典，构建词频矩阵，训练LDA模型
    dictionary = corpora.Dictionary.load(train_file_folder + '%s.dict' %(train_filename.replace('.txt','')))
    # train_list = list(dictionary.token2id.keys())
    corpus = [dictionary.doc2bow(text) for text in train_list]
    lda = models.ldamodel.LdaModel(corpus=corpus,id2word=dictionary,num_topics=5)
    lda.save(train_file_folder + '%s_lda_train_model.model'  %(train_filename.replace('.txt','')))

def lda_similarity(sentence1:str,sentence2:str,train_file_folder:str,train_filename:str):
    # stopwords = get_stopword_list(train_file_folder)
    stopwords = stop_words
    lda = models.ldamodel.LdaModel.load(train_file_folder + '%s_lda_train_model.model'  %(train_filename.replace('.txt','')))
    s1 = tokenize_doc(sentence1)
    s2 = tokenize_doc(sentence2)
    
    w1 = [word.strip() for word in s1.split() if word not in stopwords]
    w2 = [word.strip() for word in s2.split() if word not in stopwords]

    d1 = corpora.Dictionary([w1])
    d2 = corpora.Dictionary([w2])

    bow1 = d1.doc2bow(w1)
    bow2 = d2.doc2bow(w2)

    lda1 = lda[bow1]
    lda2 = lda[bow2]

    list1 = [i[1] for i in lda1]
    list2 = [i[1] for i in lda2]

    try:
        sim = np.dot(list1, list2) / (np.linalg.norm(list1) * np.linalg.norm(list2))
    except ValueError:
        sim = 0

    return sim

# model = LdaModel(common_corpus, 5, common_dictionary)
# cm = CoherenceModel(model=model, corpus=common_corpus, coherence='u_mass')
# coherence = cm.get_coherence()  # get coherence value

# NOTE- %% gensim 语料库处理

# 分割语料库
def split_corpus(corpus_path:str) -> list:
    corpus_list = []
    for line in open(corpus_path):
        corpus_list.append(utils.simple_preprocess(line))
    return corpus_list

class MyCorpus:
    """An iterator that yields sentences (lists of str)."""

    def __iter__(self):
        corpus_path = datapath('lee_background.cor')
        for line in open(corpus_path):
            # assume there's one document per line, tokens separated by whitespace
            yield utils.simple_preprocess(line)





description_corpus = get_title_train_file('data.csv','.\\similarity_template\\description.txt')

reason_corpus = get_reason_train_file('data.csv','.\\similarity_template\\reason.txt')

# train_lda_model('.\\similarity_template\\','description.txt')

# train_lda_model('.\\similarity_template\\','reason.txt')

# train_word2vec_model('.\\similarity_template\\','reason.txt')
# w2vec = word2vec_sim_cal('.\\similarity_template\\','reason.txt',sentence1,sentence2)
# lda = lda_similarity(sentence1,sentence2,'.\\similarity_template\\','reason.txt')

# bert = sentence_transformers_bert_similarity(sentence1,sentence2)
# print(bert)

description_file = open('.\\similarity_template\\description.txt', 'r', encoding='utf-8', newline='')

descriptions = description_file.readlines()
line_number = len(descriptions)
for i in range(0,line_number):
    sentence1 = descriptions[i].strip()
    for j in range(i+1, line_number):
        sentence2 = descriptions[j].strip()
        ti_idf_gensim = gensim_ti_idf_similarity(sentence1,sentence2,'.\\similarity_template\\','description.txt')
        ti_idf_sklearn = sklearn_tf_idf_similarity(sentence1,sentence2)
        word2vec_gensim = gensim_word2vec_similarity(sentence1,sentence2,'.\\similarity_template\\','description.txt')
        # word2vec_sim = word2vec_sim_cal('.\\similarity_template\\','reason.txt',sentence1,sentence2)
        # result = (ti_idf_gensim + ti_idf_sklearn + word2vec_gensim + word2vec_sim)/4
        result = (ti_idf_gensim + ti_idf_sklearn + word2vec_gensim)/3
        # result_file.write("%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n" %(ti_idf_gensim,ti_idf_sklearn,word2vec_gensim,word2vec_sim,result))
        result_file = open('.\\similarity_template\\description_compare.txt', 'w', encoding='utf-8', newline='')
        result_file.write("%.4f\t%.4f\t%.4f\t%.4f\n" %(ti_idf_gensim,ti_idf_sklearn,word2vec_gensim,result))
        result_file.close()
        print("%.4f\t%.4f\t%.4f\t%.4f\n" %(ti_idf_gensim,ti_idf_sklearn,word2vec_gensim,result))
