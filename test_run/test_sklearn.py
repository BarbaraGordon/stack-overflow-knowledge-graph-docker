import stanza
import nltk
import jieba

from gensim.models import word2vec
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer


import csv
import json
from string import punctuation
import re
import numpy as np
from scipy.sparse import coo_matrix
from scipy.spatial.distance import pdist
from scipy.linalg import norm
import scipy


def tokenize_doc(document:str) -> str:
    # 输入的一定是没有换行符的文本
    document = re.sub('\s',' ',document)
    output = ''
    words = []
    # use stanza
    # words = stanza.Pipeline(processors='tokenize', lang='en')

    # use nltk
    sentences = nltk.sent_tokenize(document)
    for sentence in sentences:
        # sentence_letter = re.sub('[\W 0-9]+',' ',sentence)   # 去掉所有非字母字符（包括数字）
        sentence_letter = re.sub('[\W]+',' ',sentence)   # 去掉所有非字母和数字的字符
        words_origin = nltk.word_tokenize(sentence_letter.strip(), language='english')
        words.extend([word for word in words_origin if word not in punctuation])  # remove the punctuation in words
        # for word in words_origin:
        #     if word not in punctuation:
    result = ' '.join(words)
    return result.strip()  # join连接的单词由前面的空格连接。

sentence1 = "I Love Beijing, London, America, NY."
sentence2 = "I hate Beijing, London, Australia, Sydney."


sentence1 = tokenize_doc(sentence1)
sentence2 = tokenize_doc(sentence2)
# 转化为TF矩阵

cv = CountVectorizer()
tv = TfidfVectorizer()
corpos = [sentence1, sentence2]
vectorsc = cv.fit_transform(corpos)
vectorst = tv.fit_transform(corpos)
vc = vectorsc.A
    
vt = vectorst.A
t = np.dot(vt[0], vt[1])

v = np.dot(vc[0], vc[1]) / (norm(vc[0]) * norm(vc[1]))

# if isinstance(vectors,scipy.):
# vectors_array = vectors.toarray()
# # 计算TF系数
# return np.dot(vectors[0], vectors[1]) / (norm(vectors[0]) * norm(vectors[1]))

# tf_idf_matrix = TfidfVectorizer()

pass