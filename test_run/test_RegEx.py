# import re
 
# content = "abcabcabc"
# rex = re.search("ca", content).span()[0]
# print(rex)
import re

content = "I'm old enough to die."
rex = re.compile(".*(old|I|enough).*")

a = re.findall(rex, content)
print(a)

content = "I'm [old enoughszxcpj'koks;zmdvc.path] to die."

rex = re.compile(r"[[](.*?)[]]")

a = re.findall(rex, content)

pass