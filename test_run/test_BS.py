from bs4 import BeautifulSoup
import bs4
from lxml import etree


markup = '''
<Html>
<Head><title>Example of Paragraph tag</title></Head>  
<Body>
<p> this is paragraph</p>
<!-- It is a Paragraph tag for creating the paragraph -->   
<p><b>HTML</b>stands for<i><u> Hyper Text Markup Language. </u> </i> It is used to create a web pages and applications. This language   
is easily understandable by the user and also be modifiable.<p>It is actually a Markup language, hence it provides a flexible way for designing the  
web pages along with the text.  </p><p>  
HTML file is made up of different elements. <b> An element </b> is a collection of <i> start tag, end tag, attributes and the text between them</i>.   
</p>  
</p>
<p> second paragraph</p>
<input name="email"/>
<div>
<div data-foo="value">foo!</div>
<b> 'b' tag use for bold text</b><i>important note formatted with italic tag</i><strong>strong also use to important note</strong>
<p>
set of link
<a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>
<a href="http://example.com/lacie" class="sister" id="link2">Lacie</a>
<a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>
<a href="https://cppsecrets.com"  class"python" id='link4'>InternShip</a>
</div>
</p> 
</Body>  
</Html>
'''

soup = BeautifulSoup(markup,'html.parser')


tag = soup.find('a')

if tag:

    print(tag.find_next(string=True))

    print(tag.find_next())




my_html = """
    <div>
        <p id="alex">Alex</p>
    </div>
    <div>
        <p id="share">share
            <code>.NET</code>
        </p>
    </div>
    <code></code>
    <p>Bob</p>
"""
soup = BeautifulSoup(my_html, "html.parser")

print(soup.contents)

# p = soup.find(id="alex")

# if p:

#     print(p.find_all_next(string=True))

#     print(p.find_all_next())


html = '<p>I tried both connection types: simple and with user defined URL <code>jdbc:oracle:thin:@172.31.10.3:1521:LZH</code>. This URL works in other applications like SQL Workbench/J using oracle driver <code>ojdbc14_g.jar</code>.</p>'
soup = BeautifulSoup(html, "html.parser")
for node in soup.contents:
    if isinstance(node,bs4.element.Tag):
        print(node.name)
        

html = '''<div><p>I love you
Dear Sonia
<code>
python
</code>
I will always love you.
</p></div>'''
soup = BeautifulSoup(html, "html.parser")
e = etree.HTML(html,parser = None)
pass