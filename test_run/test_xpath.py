# .用来匹配出换行符\n以外的任意字符
# *用来匹配前面的子表达式任意次
# +用来匹配前面的子表达式一次或多次(大于等于1次）
# ?用来匹配前面的子表达式零次或一次
# 正则表达式中的括号有三种类型:
# 中括号 [ ]: 匹配单个字符是否属于中括号中的一个字符。
# 中括号中的特殊符号也会认为是字符，比如<>,(),{}都会被看作字符的括号而非特殊含义,^ - \ 这个三个特殊符号保留特殊含义，想要匹配^ - \ 则使用\^, \-, \\
# 大括号 { }: 用于重复次数，大括号左边表达式的匹配次数。
# 小括号 ( ): 表示一个子表达式，这部分所匹配的字符将会被记住以备后续使用。
# 括号在正则表达式中常用作记忆设备，例如使用括号的子字符串匹配

# %% import
import os
# spider libraries
import requests
import scrapy
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as BS
from lxml import etree
from lxml import html
from lxml.html import fromstring, tostring
from html.parser import HTMLParser

import urllib3
import re
# NLP Libraries
import scapy
import nltk
import stanza
from stanfordcorenlp import StanfordCoreNLP
# stanza.download('en')      # This downloads the English models for the neural pipeline
# nlp = stanza.Pipeline()    # This sets up a default neural pipeline in English
nltk.download('punkt')

from string import punctuation

# %% 函数定义
def get_dictionary(dictionary_path):
    dictionary_file = open(dictionary_path)
    dictionary_patterns = dictionary_file.readlines()
    dictionary_patterns = [word.strip().replace('_',' ') for word in dictionary_patterns]
    # 把txt里的横线换成空格，把每行的判断词存贮进数组，strip用来删除字符串开头和结尾的空格
    # error和exception可能和别的词没有空格，比如‘RuntimeException’。
    dictionary_file.close()
    return dictionary_patterns

def remove_punctuation(sentence):   # 去掉句子的标点符号
    # use stanfordnlp
    # words = stanfordnlp.Pipeline(processors='tokenize', lang='en')
    
    # use nltk
    words_origin = nltk.word_tokenize(sentence.strip(), language='english')
    words = [word for word in words_origin if word not in punctuation]  # remove the punctuation in words
    return ' ' + ' '.join(words) + ' '  # join连接的单词由前面的空格连接。

def cut_minimum_sentences(text):
    mini_sentences = []
    sentences = nltk.sent_tokenize(text)
    for sentence in sentences:
        mini_sentences += sentence.split(',')
    return mini_sentences

# 将list中所有词语写成一条正则表达式，只要匹配上list中的任意一个元素（单词）就可以
def generate_RegEx_to_match_any_word(words):  # words是一个list
    RegEx_text = ".*("    # .*贪婪匹配。其实只要匹配上括号里“|”分割的任意一个词就可以了。
    for word in words[0:-1]:    # Python取数组不包含最后一个索引对应的元素
        RegEx_text = RegEx_text + word + "|"    # 最后一个单词应该直接接括号
    RegEx_text = RegEx_text + words[-1]
    RegEx_text = RegEx_text + ").*"   # reg_text[0:-1] 从第一个元素取到倒数第一个元素
    # for word in words:            # 另一种写法。
    #     reg_text = reg_text + word + "|"
    # reg_text = reg_text[0:-1] + ").*"
    return RegEx_text

def conform_word1_pattern(sentence, words1):
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1),re.I)
    # re.compile：正则表达式匹配；re.IGNORECASE=re.I：匹配时忽略大小写
    if re.search(word1_pattern,sentence) is None:
        return False
    else:
        return True

def conform_word1_word2_pattern(sentence, words1, words2):
    # word1必须在word2前面，否定词必须在动词前面
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1), re.I)
    word2_pattern = re.compile(generate_RegEx_to_match_any_word(words2), re.I)

    match_words = re.findall(word2_pattern,sentence)
    # findall返回数组，if后如果是空数组就相当于false，如果不是空数组就相当于true。一个空list本身等同于False。
    if match_words:
        for match_word in match_words:
            search_former_sentence = re.search(word1_pattern,remove_punctuation(sentence[0:sentence.find(match_word)]))
            if search_former_sentence is not None:
                # find返回的是找到的匹配项在list的坐标，这一步是要从找到word2的地方的前面去找有没有word1，字符串加索引默认步长为1
                # Python find()方法检测字符串中是否包含子字符串str，如果指定beg（开始）和end（结束）范围，则检查是否包含在指定范围内，如果包含子字符串返回开始的索引值，否则返回-1。
                # https://www.runoob.com/python/att-string-find.html
                print(search_former_sentence.group())
                return True
    return False

def is_question_valid(title_text,question_text,dictionary_path):
    # 获得文件
    words1 = get_dictionary(dictionary_path + 'words1.txt')
    words2 = get_dictionary(dictionary_path + 'words2.txt')
    words3 = get_dictionary(dictionary_path + 'words3.txt')
    words4 = get_dictionary(dictionary_path + 'words4.txt')
    words5 = get_dictionary(dictionary_path + 'words5.txt')
    negative_words = get_dictionary(dictionary_path + 'negative_words.txt')
    # 开始检查标题
    title = remove_punctuation(title_text)
    # 优先级1：title符合M1(出现[触发词1])
    if conform_word1_pattern(title,words1):
        print('Title M1')
        return True
    # 优先级2：title符合M2(出现[否定词*触发词2])
    if conform_word1_word2_pattern(title, negative_words, words2):
        print('Title M2')
        return True
    # 优先级3：title符合M3(出现[触发词3])
    if conform_word1_pattern(title,words3):
        print('Title M3')
        return True
    
    # 如果标题不符合要求，继续检查问题描述部分
    sentences = cut_minimum_sentences(question_text)    # 分成最小的句子的集合
    # 优先级4：question的最短句集合符合M4(出现[触发词4*触发词5]或[触发词5*触发词4])
    for sentence in sentences:
        sentence = remove_punctuation(sentence)
        if conform_word1_word2_pattern(sentence,words4,words5) or conform_word1_word2_pattern(sentence,words5,words4):
            # or说明words4、5不分前后顺序
            print('Question M4')
            return True
    # 优先级5：question的最短句集合符合M2(出现[否定词*触发词2])
        elif conform_word1_word2_pattern(sentence,negative_words,words2):
            print('Question M4')
            return True
    return False

def get_description_xpath(codeblocks_in_question_xpath):    # 用codeblock的xpath字符串表达式得到前面紧邻描述的xpath字符串表达式，简单的字符串连接。
    description_before_codeblock_xpath = codeblocks_in_question_xpath + '/preceding-sibling::node()[2]'     # node()[1]输出的是两个“/N”，想要得到描述需要取node()[2]。
    return description_before_codeblock_xpath

def find_codeblocks_xpath(codeblocks_in_question_xpath):        # 用来看需不需要往上去一个父节点。
    description_before_codeblocks_xpath = get_description_xpath(codeblocks_in_question_xpath)
    description_before_codeblock = e_content.xpath(description_before_codeblocks_xpath + '//text()')
    if len(description_before_codeblock) != 0:      # 如果前面没有描述，则需要再往上取一个父节点。
        # 原判断条件：看父节点是不是<pre>或<p>: if log_element.parent.name == 'pre' or log_element.parent.name == 'p':
        return codeblocks_in_question_xpath
    else:
        codeblocks_in_question_xpath = codeblocks_in_question_xpath + '/..'  # ..：选取当前节点的父节点。
        # find_codeblocks_xpath(codeblocks_in_question_xpath)     # 迭代判断有没有前面的节点的过程。报错。
        # return codeblocks_in_question_xpath
        return find_codeblocks_xpath(codeblocks_in_question_xpath)
    
def is_log(codeblock, description_before, dictionary_path):
    words1 = get_dictionary(dictionary_path + 'words1.txt')
    words2 = get_dictionary(dictionary_path + 'words2.txt')
    words4 = get_dictionary(dictionary_path + 'words4.txt')
    words5 = get_dictionary(dictionary_path + 'words5.txt')

    codeblock = codeblock.replace('\n',' ') # 输入纯文本，把换行符改为空格。

    # %% codeblock是否符合M1(出现[触发词1]或[触发词2])
    codeblock_m1 = False
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1), re.I)  # 不分大小写
    word2_pattern = re.compile(generate_RegEx_to_match_any_word(words2))        # 是带大小写的名词，不能忽略大小写。
    if conform_word1_pattern(codeblock,words1) or re.search(word2_pattern,codeblock) != None:  # 不分大小写，就可以用前面定义的函数。
        codeblock_m1 = True
        print('Codeblock M1 √')

    # %% codeblock是否符合M2(不出现[触发词3])
    codeblock_m2 = False
    #  这个r代表了原字符串的意思，比如我们匹配信息item\n时，如果没有r，我们要将正则表达式写成re.compile('item\\n')；但是，当我们加上了r时，我们的正则表达式写成re.compile(r'item\n')就可以了。
    # 字符串前面加上r：不考虑转义字符，不进行转义，按照原字符串处理。

    # m2_pattern1 = re.compile(r'[{](.*?)[}]')    # 出现[{*}]
    m2_pattern1 = re.compile(r'[{](.*)[}]')    # 出现[{*}]
    # TODO 为啥用非贪婪？应该去掉问号也可以。
    # 表达式 .*? 是满足条件的情况只匹配一次，即最小匹配（非贪婪模式）
    m2_pattern2 = re.compile(r'[=]')            # 出现[=]
    if re.search(m2_pattern1, codeblock)==None and re.search(m2_pattern2, codeblock)==None:
        # 出现[<*1>*</*2>]
        m2_pattern3 = re.compile(r'[<](.*?)[>]')
        # 这里必须用非贪婪匹配，才能取出所有成对的<>。
        contents = [content.replace('/', '') for content in re.findall(m2_pattern3, codeblock)]  # 提取<>中的内容，并覆盖掉其中的'/'
        if len(contents) == len(set(contents)):
            # 看<>标签是否有成对出现的（比如<a>...</a>），如果成对出现则认为是xml配置文件或者html文件，而不是log。
            codeblock_m2 = True
            print('Codeblock M2 √')

    description_before = remove_punctuation(description_before) # 去掉描述中的标点符号。

    # %% description_before是否符合M3(出现[触发词4])
    description_before_m3 = False
    if conform_word1_pattern(description_before,words4):        # 可以忽略大小写，re.I，使用函数
        description_before_m3 = True
        print('Description_before M3 √')

    # %% description_before是否符合M4(不出现[触发词5])
    description_before_m4 = False
    if not conform_word1_pattern(description_before,words5):    # 可以忽略大小写，re.I，使用函数
        description_before_m4 = True
        print('Description_before M4 √')

    if codeblock_m1 and codeblock_m2 and (description_before_m3 or description_before_m4):
        return True
    else:
        return False

# %% 类定义
class StackoverflowItem(scrapy.Item):
    index = scrapy.Field()
    content = scrapy.Field()

# %% 爬取网页

tag = 'java'     # 要查询的Tag
page_range = range(1,2)
question_rank = 1

folderpath = os.getcwd() + '\\data\\'
path_exist = os.path.exists(folderpath)
if not path_exist:
    os.makedirs(folderpath)

# _url = 'https://stackoverflow.com/questions/tagged/'+tag+'?tab=votes&page={page}&pagesize=50'
_url = 'https://stackoverflow.com/questions/tagged/'+tag+'?tab=Active&page={page}&pagesize=50'

urls = [_url.format(page = page) for page in page_range]

# %% xpath查找条件
question_xpath_base = '//*[@class="postcell post-layout--right"]/div[@class="s-prose js-post-body"][1]'
question_base_child_xpath = question_xpath_base + '/'
possible_log_child_nodes_xpath = question_xpath_base + '/*[name()="blockquote" or name()="pre" or name()="p"]'
format_fit_log_xpath = possible_log_child_nodes_xpath + '//*[name()="blockquote" or name()="code"]'
code_child_node_xpath = question_xpath_base + '/*[name()="code"]'       # 只要是code，里面的内容都直接取出来。

codeblocks_in_question_xpath = question_xpath_base + '//*[name()="code" or name()="blockquote"]'
# description_before_codeblock_xpath = codeblocks_in_question_xpath + '/preceding-sibling::node()[1]'   # 参见前面的函数。
# siblings_after_codeblock_xpath = codeblocks_in_question_xpath + '/following-sibling::node()'          # 好像用不到。

# %% 遍历爬取页面

# for page in page_range:
#     _url = urls[page-1]
#     headers = {'User-Agent':UserAgent().random}

#     request_summary_page = requests.get(_url,headers = headers)
#     html_summary = request_summary_page.text    # 纯文本
#     # print(html)

#     # %% BS4
#     soup = BS(html_summary,'html.parser')

#     # %% selector xpath
#     selector_summary = etree.HTML(html_summary,parser = None)
#     questions = selector_summary.xpath('//*[@class="s-post-summary    js-post-summary"]')  # 如何找到questions所在的块
#     question_per_page = len(questions)

#     for question_number in range(1,question_per_page+1):
#         question = questions[question_number-1]
        
#         question_relative_path = question.xpath('.//h3/a/@href')[0]
#         question_url = 'https://stackoverflow.com' + question_relative_path
#         # print(question_url)

#         request_detail_page = requests.get(question_url,headers = headers)
#         html_detail = request_detail_page.text  # 纯文本
#         selector_detail = etree.HTML(html_detail,parser = None) # selector
#         # print(html_detail)

#         html_detail_byte = html_detail.encode() # 把str格式转化成byte格式，否则无法写入文件
#         filename = str(page) + '-' + str(question_number) + '.html'
#         filepath = folderpath + filename
#         # 存储格式：页数-问题序号-问题详情所在url

#         # f = open(filepath,'w')

#         with open(filepath,'wb') as f:
#             f.write(html_detail_byte)
#             f.close()

#         item = StackoverflowItem()
#         item['index'] = page
#         item['content'] = selector_detail.xpath('/html')[0]     # <Element html at 0x2c781c4b600>
#         # print(item['content'])

#         question_rank += 1

# %% 筛选需要列入提取的问题
dir_list = os.listdir(folderpath)
for a in dir_list:
    html_page = open(os.path.join(folderpath,a),'r',encoding = 'utf-8')
    html_content = html_page.read()
    e_content = etree.HTML(html_content,parser = None)

    # 找到标题
    title = e_content.xpath('//*[@class="fs-headline1 ow-break-word mb8 flex--item fl1"]/a/text()')[0]
    # print(''.join(title).strip())
    print(title)

    # 找到全部问题描述
    question = e_content.xpath(question_xpath_base + '//text()')
    question = ''.join(question).strip()
    # print(question)
    e_question = etree.HTML(question,parser = None)

    # 判断可能的codeblock，找question_xpath_base的子节点中的<p><pre><code><blockquote>标签。
    # NOTE - 从这些子节点里遍历，看子节点中间是否包含<code>或<blockquote>标签，如果有一对，就直接取子节点的text，如果多于一对，则继续往下分。
    child_nodes = e_question.xpath('./node()')
    scope_num = len(child_nodes)

    # possible_log_child_nodes = e_content.xpath(possible_log_child_nodes_xpath)
    # scope_num = len(possible_log_child_nodes)

    

        # fit_format_division = e_content.xpath(format_fit_log_xpath)
        # if len(fit_format_division) == 0:
        #     pass
        # elif len(fit_format_division) == 1:
        #     codeblock = e_content.xpath(possible_log_child_nodes_xpath + '//text()')[num]
        #     if num == 0:
        #         description_before = ''
        #     else:
        #         description_before = e_content.xpath(possible_log_child_nodes_xpath + '[]/preceding-sibling::node()[1]')
        # elif len(fit_format_division) > 1:
        #     codeblocks = e_content.xpath(format_fit_log_xpath)
        #     number = len(fit_format_division)
        #     for i in range(0,number):
        #         codeblock = e_content.xpath(format_fit_log_xpath + '//text()')[i]
        #         if i == 0:
        #             description_before = ''
        #         else:
        #             description_before = e_content.xpath(format_fit_log_xpath + '[%d]/preceding-sibling::node()[1]' %(i+1))
            
    







    codeblocks = e_content.xpath(codeblocks_in_question_xpath + '//text()')
    # description_before_codeblock = e_content.xpath(description_before_codeblock_xpath + '//text()')       # 写到前面的函数了。
    # siblings_after_codeblock = e_content.xpath(siblings_after_codeblock_xpath + 'node()')   # 后面的同级节点，好像用不到了。


    # 判断问题描述里有没有code或者backquote标签，如果没有就不再处理这个页面。
    codeblock_total_number = len(codeblocks)
    if codeblock_total_number != 0:
        # 提取html元素内pre、code和blockquote标签以外的所有文本
        pure_user_description = e_question.xpath('.//*[name()!="code" and name()!="blockquote"]//text()')
        # pure_user_description = e_content.xpath('//*not[code or blockquote]//text()')  # 另一种写法
        pure_user_description = ''.join(pure_user_description).strip()
        print(pure_user_description)

        if is_question_valid(title,pure_user_description,'.\\template\\fault_page_select\\'):
            print('%s is valid.' %(a))  # 问题可用

            # 找到该问题的所有标签
            tag_list = e_content.xpath('//*[@class="ml0 list-ls-none js-post-tag-list-wrapper d-inline"][last()]//text()')
            tag_list = list(set(tag_list))      # 去掉重复的项。set()函数创建一个无序不重复元素集，可进行关系测试，删除重复数据，还可以计算交集、差集、并集等。
            print(' '.join(tag_list).strip())
        else:
            continue
        
        for num in range(0,scope_num):

            # 如果是code，直接取。
            is_tag_code = e_content.xpath('./code')
            if len(is_tag_code) != 0:
                codeblock = e_content.xpath(question_base_child_xpath + '[%d]//text()' %(num+1))
                if num == 0:
                    description_before = ''
                else:
                    description_before = e_content.xpath(question_base_child_xpath + '[%d]//text()' %(num))
                continue

            # 如果是其他标签，看里面的标签对数。
            is_tag_valid = e_content.xpath(question_base_child_xpath + 'node()[name()="blockquote" or name()="pre" or name()="p"]')
            if len(is_tag_valid) == 0:
                continue
            if len(is_tag_valid) != 0:
                fit_format_division = e_content.xpath(format_fit_log_xpath)
                if len(fit_format_division) == 1:
                    codeblock = e_content.xpath(question_base_child_xpath + '[%d]//text()' %(num+1))
                    if num == 0:
                        description_before = ''
                    else:
                        description_before = e_content.xpath(question_base_child_xpath + '[%d]//text()' %(num))
                    continue
                elif len(fit_format_division) >= 2:
                    number = len(fit_format_division)
                    for i in range(0,number):
                        codeblock = e_content.xpath(possible_log_child_nodes_xpath + '//node()[name()="blockquote" or name()="code"][%d]' %(i+1))
                        description_before = e_content.xpath(possible_log_child_nodes_xpath + '//[name()="blockquote" or name()="code"][%d]/preceding-sibling::node()[1]' %(i+1))
                        if len(description_before) == 0:
                            description_before = ''
                        else:
                            description_before = e_content.xpath(possible_log_child_nodes_xpath + '//[name()="blockquote" or name()="code"][%d]/preceding-sibling::node()[1]' %(i+1))

                









            # %% 废弃 Not Sure
            # codeblocks_in_question_xpath = find_codeblocks_xpath(codeblocks_in_question_xpath)
            # codeblocks = e_content.xpath(codeblocks_in_question_xpath + '//text()').strip()


            # description_before_codeblocks_xpath = get_description_xpath(codeblocks_in_question_xpath)
            # descriptions = e_content.xpath(description_before_codeblocks_xpath + '//text()')
            
            # description_total_number = len(descriptions)
            # if description_total_number == codeblock_total_number:
            #     for num in range(0,codeblock_total_number):
            #         codeblock = codeblocks[num]
            #         description_before_codeblock = descriptions[num]
            #         # description_before_codeblock_xpath = codeblocks_in_question_xpath + '/preceding-sibling::node()[1]'
            #         print(codeblock)
            #         print(description_before_codeblock)

            #         if is_log(codeblock,description_before_codeblock,'.\\template\\log_extract\\'):
            #             print('log %d found in %s' %(num+1,a))
