# %% import
import os
import math
# spider libraries
import requests
import scrapy
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as BS
import bs4
from lxml import etree
from lxml import html
from lxml.html import fromstring, tostring
from html.parser import HTMLParser

import pandas
import csv
import openpyxl

import urllib3
import re
import regex
# NLP Libraries
import scapy
import nltk
import stanza
from stanfordcorenlp import StanfordCoreNLP
# stanza.download('en')      # This downloads the English models for the neural pipeline
# nlp = stanza.Pipeline()    # This sets up a default neural pipeline in English
# nltk.download('punkt')

from string import punctuation

def get_pure_description(BS_question_subject:bs4.element.Tag) -> str:
    # 输入是bs4.BeautifulSoup经过过滤器之后的对象
    description = ''
    for node in BS_question_subject.contents:
        if isinstance(node,bs4.element.Tag):
            if node.name == 'pre' or node.name == 'code' or node.name == 'blockquote':
                continue
            elif node.contents:      # 没有子标签的话可以直接加里面的文本
                # description = description + ' ' + node.get_text(' ')
                # description = description.replace('\n',' ')
                # print(description)
                description = description + ' ' + get_pure_description(node)
            # else:
            #     description = description + ' ' + get_pure_description(node)
        else:
            description = description + node.get_text(' ')
            description = description.replace('\n',' ')
            # print(description)
    return description

    # for node in BS_question_subject.contents:
    #     if node and isinstance(node,bs4.element.Tag):
    #         if node.name:
    #             if node.name == 'pre' or node.name == 'code' or node.name == 'blockquote':
    #                 continue
    #             elif node.contents:      # 没有子标签的话可以直接加里面的文本
    #                 # description = description + ' ' + node.get_text(' ')
    #                 # description = description.replace('\n',' ')
    #                 # print(description)
    #                 description = description + get_pure_description(node)
    #             # else:
    #             #     description = description + ' ' + get_pure_description(node)
    #         else:
    #             description = description + node.get_text(' ')
    #             description = description.replace('\n',' ')
    #             print(description)
    # return description

def find_code_and_description(BS_question_subject:bs4.element.Tag, result:list):
    query =len(BS_question_subject.contents)

    for i in range(0,query):
        node = BS_question_subject.contents[i]
        if isinstance(node,bs4.element.Tag):
            if node.name == 'pre' or node.name == 'code':
                code_block = node.get_text()
                if i == 0:
                    description_before = ''
                else:
                    node_p = BS_question_subject.contents[i-1]
                    if isinstance(node_p,bs4.element.NavigableString):
                        description_before = node_p.get_text()
                    else:
                        ps = node.previousSibling
                        if ps:
                            description_before = ps.get_text()
                        else:
                            description_before = ''
                if is_log(code_block,description_before,'.\\template\\log_extract\\'):
                    # print('The log is available!\nThe code is:%s \nThe description is:%s' %(code_block,description_before))

                    result.append(code_block)
                    # return result
                # csv_write.writerow([code_block,description_before])
                # f.close()
            elif node.name == 'blockquote':             # 'blockquote'里一定有'p'标签。
                p_layer = node.find()
                if isinstance(p_layer,bs4.element.Tag):
                    code_block = p_layer.get_text()
                    ps = node.previousSibling   # blockquote一定在问题部分下的第一层
                    if ps:
                        description_before = ps.get_text()
                    else:
                        description_before = ''
                    if is_log(code_block,description_before,'.\\template\\log_extract\\'):
                        # print('The log is available!\ncode is:%s \ndescription is:%s' %(code_block,description_before))
                        result.append(code_block)
                        # return result
                    in_line = len(p_layer.contents)
                    for in_n in range(0,in_line):
                        in_node = p_layer.contents[in_n]
                        if isinstance(in_node,bs4.element.NavigableString):
                            pass
                        elif isinstance(in_node,bs4.element.Tag):
                            # result.append(find_code_and_description(in_node))
                            find_code_and_description(in_node,result)
            else:
                find_code_and_description(node,result)
        else:
            pass

def get_dictionary(dictionary_path:str) -> list:
    dictionary_file = open(dictionary_path)
    dictionary_patterns = dictionary_file.readlines()
    dictionary_patterns = [word.strip().replace('_',' ') for word in dictionary_patterns]
    # 把txt里的横线换成空格，把每行的判断词存贮进数组，strip用来删除字符串开头和结尾的空格
    # error和exception可能和别的词没有空格，比如‘RuntimeException’。
    dictionary_file.close()
    return dictionary_patterns

def remove_punctuation(sentence:str) -> str:   # 去掉句子的标点符号
    # use stanfordnlp
    # words = stanfordnlp.Pipeline(processors='tokenize', lang='en')
    
    # use nltk
    words_origin = nltk.word_tokenize(sentence.strip(), language='english')
    words = [word for word in words_origin if word not in punctuation]  # remove the punctuation in words
    return ' ' + ' '.join(words) + ' '  # join连接的单词由前面的空格连接。

def cut_minimum_sentences(text:str) -> list:
    mini_sentences = []
    sentences = nltk.sent_tokenize(text)
    for sentence in sentences:
        mini_sentences += sentence.split(',')
    return mini_sentences

# 将list中所有词语写成一条正则表达式，只要匹配上list中的任意一个元素（单词）就可以
def generate_RegEx_to_match_any_word(words:list) -> str:  # words是一个list
    RegEx_text = ".*("    # .*贪婪匹配。其实只要匹配上括号里“|”分割的任意一个词就可以了。
    for word in words[0:-1]:    # Python取数组不包含最后一个索引对应的元素
        RegEx_text = RegEx_text + word + "|"    # 最后一个单词应该直接接括号
    RegEx_text = RegEx_text + words[-1]
    RegEx_text = RegEx_text + ").*"   # reg_text[0:-1] 从第一个元素取到倒数第一个元素
    # for word in words:            # 另一种写法。
    #     reg_text = reg_text + word + "|"
    # reg_text = reg_text[0:-1] + ").*"
    return RegEx_text

def conform_word1_pattern(sentence:str, words1:list) -> bool:
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1),re.I)
    # re.compile：正则表达式匹配；re.IGNORECASE=re.I：匹配时忽略大小写
    if re.search(word1_pattern,sentence) is None:
        return False
    else:
        return True

def conform_word1_word2_pattern(sentence:str, words1:list, words2:list) -> bool:
    # word1必须在word2前面，否定词必须在动词前面
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1), re.I)
    word2_pattern = re.compile(generate_RegEx_to_match_any_word(words2), re.I)

    match_words = re.findall(word2_pattern,sentence)
    # findall返回数组，if后如果是空数组就相当于false，如果不是空数组就相当于true。一个空list本身等同于False。
    if match_words:
        for match_word in match_words:
            search_former_sentence = re.search(word1_pattern,remove_punctuation(sentence[0:sentence.find(match_word)]))
            if search_former_sentence is not None:
                # find返回的是找到的匹配项在list的坐标，这一步是要从找到word2的地方的前面去找有没有word1，字符串加索引默认步长为1
                # Python find()方法检测字符串中是否包含子字符串str，如果指定beg（开始）和end（结束）范围，则检查是否包含在指定范围内，如果包含子字符串返回开始的索引值，否则返回-1。
                # https://www.runoob.com/python/att-string-find.html

                # print(search_former_sentence.group())
                return True
    return False

def is_question_valid(title_text:str,question_text:str,dictionary_path:str) -> bool:
    # 获得文件
    words1 = get_dictionary(dictionary_path + 'words1.txt')
    words2 = get_dictionary(dictionary_path + 'words2.txt')
    words3 = get_dictionary(dictionary_path + 'words3.txt')
    words4 = get_dictionary(dictionary_path + 'words4.txt')
    words5 = get_dictionary(dictionary_path + 'words5.txt')
    negative_words = get_dictionary(dictionary_path + 'negative_words.txt')
    # 开始检查标题
    title = remove_punctuation(title_text)
    # 优先级1：title符合M1(出现[触发词1])
    if conform_word1_pattern(title,words1):
        print('Title M1')
        return True
    # 优先级2：title符合M2(出现[否定词*触发词2])
    if conform_word1_word2_pattern(title, negative_words, words2):
        print('Title M2')
        return True
    # 优先级3：title符合M3(出现[触发词3])
    if conform_word1_pattern(title,words3):
        print('Title M3')
        return True
    
    # 如果标题不符合要求，继续检查问题描述部分
    sentences = cut_minimum_sentences(question_text)    # 分成最小的句子的集合
    # 优先级4：question的最短句集合符合M4(出现[触发词4*触发词5]或[触发词5*触发词4])
    for sentence in sentences:
        sentence = remove_punctuation(sentence)
        if conform_word1_word2_pattern(sentence,words4,words5) or conform_word1_word2_pattern(sentence,words5,words4):
            # or说明words4、5不分前后顺序
            print('Question M4')
            return True
    # 优先级5：question的最短句集合符合M2(出现[否定词*触发词2])
        elif conform_word1_word2_pattern(sentence,negative_words,words2):
            print('Question M5')
            print(sentence)
            return True
    return False

def is_log(codeblock:str, description_before:str, dictionary_path:str) -> bool:
    words1 = get_dictionary(dictionary_path + 'words1.txt')
    words2 = get_dictionary(dictionary_path + 'words2.txt')
    words4 = get_dictionary(dictionary_path + 'words4.txt')
    words5 = get_dictionary(dictionary_path + 'words5.txt')

    codeblock = codeblock.replace('\n',' ') # 输入纯文本，把换行符改为空格。

    # %% codeblock是否符合M1(出现[触发词1]或[触发词2])
    codeblock_m1 = False
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1), re.I)  # 不分大小写
    word2_pattern = re.compile(generate_RegEx_to_match_any_word(words2))        # 是带大小写的名词，不能忽略大小写。
    if conform_word1_pattern(codeblock,words1) or re.search(word2_pattern,codeblock) != None:  # 不分大小写，就可以用前面定义的函数。
        codeblock_m1 = True
        # print('Codeblock M1 √')

    # %% codeblock是否符合M2(不出现[触发词3])
    codeblock_m2 = False
    #  这个r代表了原字符串的意思，比如我们匹配信息item\n时，如果没有r，我们要将正则表达式写成re.compile('item\\n')；但是，当我们加上了r时，我们的正则表达式写成re.compile(r'item\n')就可以了。
    # 字符串前面加上r：不考虑转义字符，不进行转义，按照原字符串处理。

    # m2_pattern1 = re.compile(r'[{](.*?)[}]')    # 出现[{*}]
    m2_pattern1 = re.compile(r'[{](.*)[}]')    # 出现[{*}]
    # TODO 为啥用非贪婪？应该去掉问号也可以。
    # 表达式 .*? 是满足条件的情况只匹配一次，即最小匹配（非贪婪模式）
    m2_pattern2 = re.compile(r'[=]')            # 出现[=]
    if re.search(m2_pattern1, codeblock)==None and re.search(m2_pattern2, codeblock)==None:
        # 出现[<*1>*</*2>]
        m2_pattern3 = re.compile(r'[<](.*?)[>]')
        # 这里必须用非贪婪匹配，才能取出所有成对的<>。
        contents = [content.replace('/', '') for content in re.findall(m2_pattern3, codeblock)]  # 提取<>中的内容，并覆盖掉其中的'/'
        if len(contents) == len(set(contents)):
            # 看<>标签是否有成对出现的（比如<a>...</a>），如果成对出现则认为是xml配置文件或者html文件，而不是log。
            codeblock_m2 = True
            # print('Codeblock M2 √')

    description_before = remove_punctuation(description_before) # 去掉描述中的标点符号。

    # %% description_before是否符合M3(出现[触发词4])
    description_before_m3 = False
    if conform_word1_pattern(description_before,words4):        # 可以忽略大小写，re.I，使用函数
        description_before_m3 = True
        # print('Description_before M3 √')

    # %% description_before是否符合M4(不出现[触发词5])
    description_before_m4 = False
    if not conform_word1_pattern(description_before,words5):    # 可以忽略大小写，re.I，使用函数
        description_before_m4 = True
        # print('Description_before M4 √')

    if codeblock_m1 and codeblock_m2 and (description_before_m3 or description_before_m4):
        return True
    else:
        return False

folderpath = os.getcwd() + '\\data_answer_find\\'
path_exist = os.path.exists(folderpath)
if not path_exist:
    os.makedirs(folderpath)

# %% xpath查找条件
question_xpath_base = '//*[@class="postcell post-layout--right"]/div[@class="s-prose js-post-body"][1]'
codeblocks_in_question_xpath = question_xpath_base + '//*[name()="code" or name()="blockquote"]'

a = '001-12.html'
html_page = open(os.path.join(folderpath,a),'r',encoding = 'utf-8')
html_content_origin = html_page.read()                              # HTML的内容
html_content_origin = html_content_origin.replace('\n','').strip()  # 去掉所有换行符
html_content_origin = html_content_origin.replace('<!DOCTYPE html>','')
# 去掉空格
# RegEx_remove_space = re.compile(r'>\s+<')
html_content = re.sub(r'>\s+?<',r'><',html_content_origin)
e_content = etree.HTML(html_content,parser = None)      # xpath解析：用来找title,question,tags,codeblocks
b_content = BS((html_content.strip()), 'html.parser')   # BeautifulSoup解析
html_page.close()                                       # 关闭文档

# xpath找到标题
title = e_content.xpath('//*[@class="fs-headline1 ow-break-word mb8 flex--item fl1"]/a/text()')[0]
# print(''.join(title).strip())
# print(title)

# xpath找到全部问题描述
e_question = e_content.xpath(question_xpath_base + '//text()')
question = ''.join(e_question).strip()
# print(question)

b_question = b_content.find(class_="postcell post-layout--right")
if isinstance(b_question,bs4.element.Tag):
    b_question = b_question.find(class_="s-prose js-post-body")    # 找第二层标签的时候需要用find_next
    # if b_question != None:
    #     print(b_question.text)

# 判断可能的codeblock，找question_xpath_base的子节点中的<p><pre><code><blockquote>标签。
# NOTE - 从这些子节点里遍历，看子节点中间是否包含<code>或<blockquote>标签，如果有一对，就直接取子节点的text，如果多于一对，则继续往下分。

codeblocks = e_content.xpath(codeblocks_in_question_xpath + '//text()')

# 判断问题描述里有没有code或者backquote标签，如果没有就不再处理这个页面。
codeblock_total_number = len(codeblocks)
if codeblock_total_number != 0:
    # 提取html元素内pre、code和blockquote标签以外的所有文本
    if isinstance(b_question,bs4.element.Tag):
        pure_user_description = get_pure_description(b_question)
        pure_user_description = pure_user_description.strip().replace('  ',' ')
        # pure_user_description = regex.sub('\\s+','\\s',pure_user_description)
        # print(pure_user_description)

        if is_question_valid(title,pure_user_description,'.\\template\\fault_page_select\\'):
            print('%s is valid.' %(a))  # 问题可用