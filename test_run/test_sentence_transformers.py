from sentence_transformers import SentenceTransformer,util

def sentence_transformers_embedding(sentence:str, model_name:str):
    model = SentenceTransformer(model_name)
    sentence_embedding = model.encode(sentence, convert_to_numpy = True)
    print(type(sentence_embedding))
    print(str(sentence_embedding))
    return sentence_embedding

sentence_transformers_embedding('I am so happy',r'..\\transformers_models\\uncased-bert-triplet-40\\')
# https://huggingface.co/kmariunas/uncased-bert-triplet-40
