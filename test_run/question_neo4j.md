How can I add string variables into SET request using official Neo4j Python Drive?

neo4j
python
python-3.x
neo4j-apoc
driver


Since py2neo isn't quite compatible with the latest Neo4j, I chose to use the official Neo4j Python Driver.

# My purpose

I've been trying to add a string variable into the `Driver.execute_query()` method's cypher `SET` request.  

My purpose is using Neo4j-python-drive to give some specific nodes another key "content" and set its value equals to an input string variable(`node. Content = input_string_variable`).

Which is turn `(n:Log {id: 'log_001-02'})` into `(n:Log {id: 'log_001-02', content: my_input_log_string})`.

Now every node in my database has given a unique value of key "id", so I search any `(node)` by `node.id`.

# information
## python & Neo4j version
```shell
> python -V 
Python 3.11.3
> pip show neo4j
Name: neo4j
Version: 5.10.0
Summary: Neo4j Bolt driver for Python
Home-page:
Author:
Author-email: "Neo4j, Inc." <drivers@neo4j.com>
License: Apache License, Version 2.0
Location: D:\miniconda3\envs\coderunnew\Lib\site-packages
Requires: pytz
Required-by:
```

## My code
```python
import neo4j
from neo4j import GraphDatabase, RoutingControl, Driver
# from neo4j.exceptions import DriverError, Neo4jError
import logging
import sys
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
logging.getLogger("neo4j").addHandler(handler)
logging.getLogger("neo4j").setLevel(logging.DEBUG)
URI = "neo4j://127.0.0.1:7687"
AUTH = ("neo4j", "!neo4j")
database_name = "neo4j"

def add_node(driver: Driver, target_node_id: str):
    driver.execute_query(
        "CREATE (n {id: $target_node_id})",
        target_node_id = target_node_id, 
        database_ = 'neo4j',
    )

def add_node_content(driver: Driver, target_node_isd: str, content_to_be_added: str):
    driver.execute_query(
        "MATCH (n {id: $target_node_id})"
        "SET n.content= $content_to_be_added",
        target_node_id = target_node_id, content_to_be_added = content_to_be_added,
        database_ = 'neo4j',
    )

with GraphDatabase.driver(URI, auth=AUTH, trusted_certificates = neo4j.TrustAll()) as driver:
    driver.verify_connectivity()
    add_node(driver, 'test_id')
    add_node_content(driver, 'test_id', 'content_text')
```

## Error Massage
```
  File "D:\test_neo4j.py", line 22, in add_node_content
    driver.execute_query(
  File "D:\test_neo4j.py", line 32, in <module>
    add_node_content(driver, 'test_id', 'content_text')
neo4j.exceptions.CypherSyntaxError: {code: Neo.ClientError.Statement.SyntaxError} {message: Invalid input '{': expected "+" or "-" (line 1, column 47 (offset: 46))
"MATCH (n {id: $target_node_id})SET n.content= {content_to_be_added}"
                                               ^}
```
## Log Massage
I selected some of the log about the error below.
```shell
>> [#DAC1]  _: <CONNECTION> state: READY > TX_READY_OR_TX_STREAMING
>> [#DAC1]  C: RUN 'CREATE (n {id: $target_node_id})' {'target_node_id': 'test_id'} {}
>> [#DAC1]  C: PULL {'n': 1000}
>> [#DAC1]  S: SUCCESS {'t_first': 3, 'fields': [], 'qid': 0}
>> [#DAC1]  S: SUCCESS {'stats': {'contains-updates': True, 'nodes-created': 1, 'properties-set': 1}, 'type': 'w', 't_last': 0, 'db': 'neo4j'}
>> [#DAC1]  C: COMMIT
>> [#DAC1]  S: SUCCESS {'bookmark': 'FB:kcwQX/44Cf2sTYCaOP0gmu8R8MoAAhrckA=='}
>> [#DAC1]  _: <CONNECTION> state: TX_READY_OR_TX_STREAMING > READY
```
```shell
>> [#DAC1]  _: <CONNECTION> state: READY > TX_READY_OR_TX_STREAMING
>> [#DAC1]  C: RUN 'MATCH (n {id: $target_node_id})SET n.content= {content_to_be_added}' {'target_node_id': 'test_id', 'content_to_be_added': 'content_text'} {}
>> [#DAC1]  C: PULL {'n': 1000}
>> [#DAC1]  S: FAILURE {'code': 'Neo.ClientError.Statement.SyntaxError', 'message': 'Invalid input \'{\': expected "+" or "-" (line 1, column 47 (offset: 46))\r\n"MATCH (n {id: $target_node_id})SET n.content= {content_to_be_added}"\r\n                                               ^'}
>> [#DAC1]  C: RESET
>> [#DAC1]  S: IGNORED
>> [#DAC1]  S: SUCCESS {}
>> [#DAC1]  _: <CONNECTION> state: FAILED > READY
```
```cypher
"MATCH (n {id: $id})
WITH n
CALL apoc.create.setProperties(n, ['content'], [$content])
YIELD n
RETURN n" {'parameters_': {'content': 'content_text', 'id': 'test_id'}, 'database_': 'neo4j'} {'db': 'neo4j'}
```