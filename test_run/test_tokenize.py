import nltk
from string import punctuation

def remove_punctuation(sentence:str) -> str:   # 去掉句子的标点符号
    # use stanfordnlp
    # words = stanfordnlp.Pipeline(processors='tokenize', lang='en')
    
    # use nltk
    words_origin = nltk.word_tokenize(sentence.strip(), language='english')
    words = [word for word in words_origin if word not in punctuation]  # remove the punctuation in words
    return ' ' + ' '.join(words) + ' '  # join连接的单词由前面的空格连接。


# text = "I have a file --VIM named /root/.docker/cert.pem set.json. I'm working on it -f, I mean 'C:\\ProgramData\\Docker\\config' `set.xml`."
# text_sentences = nltk.sent_tokenize(text)
# s1 = text_sentences[0]

# s2 = text_sentences[1]

# text = "Make sure you have space left on the device, Docker can't start up if there isn't any space left."

# text_r = remove_punctuation(text)


text = "Hi! I'm Jon. Don't treat me like child, I'm a set.json. age 29."
text_sentences = nltk.sent_tokenize(text)
pass



# %%
# def get_dictionary(dictionary_path:str) -> list:
#     dictionary_file = open(dictionary_path)
#     dictionary_patterns = dictionary_file.readlines()
#     dictionary_patterns = [word.strip().replace('_',' ') for word in dictionary_patterns]
#     # 把txt里的横线换成空格，把每行的判断词存贮进数组，strip用来删除字符串开头和结尾的空格
#     # error和exception可能和别的词没有空格，比如‘RuntimeException’。
#     dictionary_file.close()
#     return dictionary_patterns
# list_i = get_dictionary('.\\knowledge_verify\\instructions_docker_compose.txt')
# print(list_i)