import neo4j
from neo4j import GraphDatabase, RoutingControl, Driver, Session
from neo4j.exceptions import DriverError, Neo4jError
import logging
import sys
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
logging.getLogger("neo4j").addHandler(handler)
logging.getLogger("neo4j").setLevel(logging.DEBUG)
URI = "neo4j://127.0.0.1:7687"
AUTH = ("neo4j", "Hirain!neo4j")
database_name = "neo4j"

def add_node(driver: Driver, target_node_id: str):
    driver.execute_query(
        "CREATE (n {id: $target_node_id})",
        target_node_id = target_node_id, 
        database_ = 'neo4j',
    )

def add_node_content(driver: Driver, target_node_id: str, content_to_be_added: str):
    driver.execute_query(
        query_="MATCH (n {id: $id}) CALL apoc.create.setProperties(n, ['content'], [$content]) YIELD node RETURN node AS a"
        # ":params {}"
        "MATCH (n {id: $id_}) CALL apoc.create.setProperties(n, ['content'], [$content_]) YIELD node RETURN node AS b",
        parameters_={'content': content_to_be_added, 'id': target_node_id,'content_': content_to_be_added + '_', 'id': target_node_id + '_'},
        database_ = 'neo4j',
    )
def add_node_content_session(session: Session, target_node_id: str, content_to_be_added: str):
    session.run(
        query= "MATCH (n {id: $id}) CALL apoc.create.setProperties(n, ['content'], [$content]) YIELD node AS a RETURN a"
        "MATCH (n {id: $id}) CALL apoc.create.setProperties(n, ['content'], [$content]) YIELD node AS b RETURN b",
        parameters = {'content': content_to_be_added, 'id': target_node_id},
    )

with GraphDatabase.driver(URI, auth=AUTH, trusted_certificates = neo4j.TrustAll()) as driver:
    driver.verify_connectivity()
    add_node(driver, 'test_id')
    add_node(driver, 'test_id_')
    add_node_content(driver, 'test_id', 'content_text')
    # session = driver.session()
    # add_node_content_session(session,'test_id','content_text')
    
