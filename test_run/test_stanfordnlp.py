import stanfordnlp
# words = stanfordnlp.Pipeline(processors='tokenize', lang='en')

nlp = stanfordnlp.Pipeline(processors='tokenize,pos', lang='en',models_dir='D:\\xingyu.shao\\code\\test\\stanfordnlp_resources', treebank='en_ewt', use_gpu=True, pos_batch_size=3000) # Build the pipeline, specify part-of-speech processor's batch size
doc = nlp("Barack Obama was born in Hawaii.") # Run the pipeline on input text
pass