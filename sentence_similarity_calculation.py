# import stanza
import nltk
from nltk.corpus import stopwords
from nltk import download
# import jieba
import mteb

import torch
from torch import utils, cosine_similarity

from transformers import BertTokenizer, AutoModel, AutoTokenizer, AutoModelForCausalLM


from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.feature_extraction.text import TfidfTransformer
# from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from sentence_transformers import SentenceTransformer,util

import os
# import csv
# import json
# import itertools
from string import punctuation
import re
# import math
import numpy
from numpy import reshape, shape, empty, zeros, ndim, ndarray
import scipy
from scipy import spatial,sparse
from scipy.sparse import coo_matrix, csr_matrix, csc_matrix, spmatrix
from scipy.spatial.distance import pdist, cosine
from scipy.linalg import norm
from sklearn.metrics.pairwise import cosine_similarity


import warnings
warnings.filterwarnings("ignore")

from sentence_embedding_model_training import tokenize_doc
from sentence_embedding_model_training import clean_sentence

# download('stopwords')  # Download stopwords list.
stop_words = stopwords.words('english')

ignore_list = ['gpt-neo-125m']

LOGS = False  # Set to True if you want to see progress in logs.
if LOGS:
    import logging
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def check_dir(folderpath:str):
    # folderpath = os.getcwd() + '\\' + relative_path + '\\'
    path_exist = os.path.exists(folderpath)
    if not path_exist:
        os.makedirs(folderpath)

# 使用TF-IDF计算两个句子的相似度，语料库仅为两句话
def sklearn_tf_idf_similarity(sentence1:str,sentence2:str) -> float:
    # 分词、去掉特殊符号、转化为小写处理
    sentence1 = tokenize_doc(sentence1)
    sentence2 = tokenize_doc(sentence2)
    # 转化为TF矩阵
    cv = CountVectorizer(tokenizer=lambda s: s.split())
    corpus = [sentence1, sentence2]
    vectors = cv.fit_transform(corpus)

    # vectors_array = vectors.A
    # if isinstance(vectors, spmatrix):
    #     vectors_array = spmatrix.
    if isinstance(vectors, csc_matrix):
        vectors_array = csc_matrix.toarray(vectors)
    elif isinstance(vectors, coo_matrix):
        vectors_array = coo_matrix.toarray(vectors)
    elif isinstance(vectors,csr_matrix):
        vectors_array = csr_matrix.toarray(vectors)
    elif isinstance(vectors,ndarray):
        vectors_array = vectors
    else:
        print('spmaritx error')
        exit(1)
    # 计算TF系数
    result =  numpy.dot(vectors_array[0], vectors_array[1]) / (norm(vectors_array[0]) * norm(vectors_array[1]))
    return result

def gpt_neo_get_answer(input:str, corpus:list[str]):
    # Get models - The package will take care of downloading the models automatically
    # For best performance: EleutherAI/gpt-j-6B
    tokenizer = AutoTokenizer.from_pretrained("EleutherAI/gpt-neo-125M")
    model = AutoModelForCausalLM.from_pretrained("EleutherAI/gpt-neo-125M")
    # Deactivate Dropout (There is no dropout in the above models so it makes no difference here but other SGPT models may have dropout)
    model.eval()
    prompt = 'Documents are searched to find matches with the same content.\nThe document "{}" is a good search result for "'
    queries = [
        input,
    ]
    docs = corpus
    for query in queries:
        print(f"Query: {query}")
        for doc in docs:
            context = prompt.format(doc)
            context_enc = tokenizer.encode(context, add_special_tokens=False)
            continuation_enc = tokenizer.encode(query, add_special_tokens=False)
            # Slice off the last token, as we take its probability from the one before
            model_input = torch.tensor(context_enc+continuation_enc[:-1])
            continuation_len = len(continuation_enc)
            input_len, = model_input.shape
            # [seq_len] -> [seq_len, vocab]
            logprobs = torch.nn.functional.log_softmax(model(model_input)[0], dim=-1).cpu()
            # [seq_len, vocab] -> [continuation_len, vocab]
            logprobs = logprobs[input_len-continuation_len:]
            # Gather the log probabilities of the continuation tokens -> [continuation_len]
            logprobs = torch.gather(logprobs, 1, torch.tensor(continuation_enc).unsqueeze(-1)).squeeze(-1)
            score = torch.sum(logprobs)
            # The higher (closer to 0), the more similar
            print(f"Document: {doc[:20] + '...'} Score: {score}")

def embedding_cosine_similarity(npy_file_path1:str, npy_file_path2:str):
    embedding1 = numpy.load(npy_file_path1)
    embedding2 = numpy.load(npy_file_path2)
    dim1 = ndim(embedding1)
    dim2 = ndim(embedding2)
    shape1 = shape(embedding1)
    shape2 = shape(embedding2)
    if dim1 != dim2:
        print('ERROR DIM!')
        exit(1)
    if dim1 != 1:
        if shape1 != shape2:
            exit(1)
        #     for dim in range(0, dim1):
        #         ml = max(shape1[dim], shape2[dim])
        #         ll1 = []
        #         ll2 = []
        #         l1 = ml - shape1[dim]
        #         l2 = ml - shape2[dim]
        #         ll1.append(l1)
        #         ll2.append(l2)
        #         lt1 = tuple(ll1)
        #         lt2 = tuple(ll2)
        #     embedding1 = numpy.pad()
        flatten1 = ndarray.flatten(embedding1)
        flatten2 = ndarray.flatten(embedding2)
        cos_similarity_matrix = cosine_similarity(embedding1, embedding2)
        cos_similarity = 1 - cosine(flatten1, flatten2)
    else:
        if shape1 != shape2:
            print('ERROR SHAPE!')
            max_length = max(len(embedding1),len(embedding2))
            embedding1 = numpy.concatenate((embedding1,zeros(max_length - len(embedding1))))
            embedding2 = numpy.concatenate((embedding2,zeros(max_length - len(embedding2))))
        cos_similarity = 1 - cosine(embedding1, embedding2)
    return cos_similarity


def list_subdirectory(path:str) -> list[str]:
    subdir_list = []
    item_list = os.scandir(path)
    for item in item_list:
        if os.path.isdir(item.path):
            subdir_list.append(item.path)
    return subdir_list

# gensim [tf_idf,word2vec,fasttext]
# transformers dirlist
def write_corpus_similarity(train_file_folder:str,train_filename:str):
    subdir_list = list_subdirectory(train_file_folder)
    # npy_folder_path = train_file_folder + train_filename.replace('.txt','_transformers_' + dir_i) + '\\'
    for subdir in subdir_list:
        # keywords = subdir.split('_')
        keywords = re.split(r'\\|_|\s',subdir)
        if train_filename.replace('.txt','') in keywords and 'sklearn' not in keywords:
            index_file_path = subdir + '\\' + 'index.txt'
            index_file = open(index_file_path, 'w', encoding='utf-8', newline='')
            index_file.close()
            npy_file_list = os.listdir(subdir)
            npy_file_list.remove('index.txt')
            file_number = len(npy_file_list)
            for i in range(0,file_number):
                file_i = npy_file_list[i]
                index_i = file_i.replace('.npy','')
                filepath_i = subdir + '\\' + file_i
                for j in range(i + 1, file_number):
                    file_j = npy_file_list[j]
                    index_j = file_j.replace('.npy','')
                    filepath_j = subdir + '\\' + file_j
                    similarity = embedding_cosine_similarity(filepath_i, filepath_j)
                    line = [index_i, index_j, similarity]
                    index_file = open(index_file_path, 'a', encoding='utf-8', newline='')
                    index_file.write(str(line) + '\n')
                    index_file.close()

def write_sklearn_similarity(train_file_folder:str,train_filename:str):
    train_file = open(train_file_folder + train_filename, 'r', encoding='utf-8', newline='')
    sklearn_tfidf_folder = train_file_folder + train_filename.replace('.txt','_') + 'sklearn_tfidf\\'
    check_dir(sklearn_tfidf_folder)
    similarity_index_file = open(sklearn_tfidf_folder + 'index.txt', 'w', encoding='utf-8', newline='')
    similarity_index_file.close()
    corpus_index_filepath = train_file_folder + train_filename.replace('.txt','_index.txt')
    corpus_index_file = open(corpus_index_filepath, 'r', encoding='utf-8', newline='')
    corpus = train_file.readlines()
    corpus_index = corpus_index_file.readlines()
    corpus_number = len(corpus)
    for k in range(0, corpus_number):
        s1 = corpus[k].strip()
        index_k = eval(corpus_index[k].strip())
        index1 = index_k[0]
        for l in range(k + 1, corpus_number):
            s2 = corpus[l].strip()
            index_l = eval(corpus_index[l].strip())
            index2 = index_l[0]

            similarity = sklearn_tf_idf_similarity(s1,s2)
            line = [index1, index2, similarity]
            similarity_index_file = open(sklearn_tfidf_folder + 'index.txt', 'a', encoding='utf-8', newline='')
            similarity_index_file.write(str(line) + '\n')
            similarity_index_file.close()
    corpus_index_file.close()

def main():
    train_file_folder = '.\\similarity_template\\'
    write_corpus_similarity(train_file_folder,'description.txt')
    # write_sklearn_similarity(train_file_folder,'description.txt')
    write_corpus_similarity(train_file_folder, 'reason.txt')
    # write_sklearn_similarity(train_file_folder,'reason.txt')

if __name__ == '__main__':
    main()