import stanza
import nltk
from nltk.corpus import stopwords
from nltk import download
import jieba

import mteb
# import word2vec
import gensim
from gensim import utils, corpora, models, similarities
from gensim.test.utils import datapath, common_corpus, common_dictionary
from gensim.models import word2vec, keyedvectors
from gensim.models.fasttext import FastText
from gensim.models.word2vec import Word2Vec, LineSentence
from gensim.models.keyedvectors import KeyedVectors
from gensim.models.ldamodel import LdaModel
from gensim.models.coherencemodel import CoherenceModel
from gensim.similarities import WmdSimilarity

import torch
from transformers import BertTokenizer, AutoModel, AutoTokenizer


from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.feature_extraction.text import TfidfTransformer
# from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from sentence_transformers import SentenceTransformer,util

import os
import csv
import json
import itertools
from string import punctuation
import re
import math
import numpy as np
from numpy import reshape, shape, empty, zeros, ndarray
from scipy.sparse import coo_matrix
from scipy import spatial
from scipy.spatial.distance import pdist, cosine
from scipy.linalg import norm
import scipy

import warnings
warnings.filterwarnings("ignore")

# download('stopwords')  # Download stopwords list.
stop_words = stopwords.words('english')

LOGS = False  # Set to True if you want to see progress in logs.
if LOGS:
    import logging
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# 使用nltk对句子分词，并去掉特殊符号全部转化为小写后转化成一个字符串输出
def tokenize_doc(document:str) -> str:
    # 输入的一定是没有换行符的文本
    document = re.sub('\n',' ',document)
    number_pattern = re.compile('[0-9]+')
    document = re.sub(number_pattern, '', document)
    # print(document)

    output = ''
    words = []
    # use stanza
    # words = stanza.Pipeline(processors='tokenize', lang='en')

    # use nltk
    sentences = nltk.sent_tokenize(document)
    for sentence in sentences:
        # sentence_letter = re.sub('[\W 0-9]+',' ',sentence)   # 去掉所有非字母字符（包括数字）
        sentence_letter = re.sub(r'[\W]+',' ',sentence)   # 去掉所有非字母和数字的字符
        words_origin = nltk.word_tokenize(sentence_letter, language='english')
        words.extend([word.lower() for word in words_origin if word not in punctuation])  # remove the punctuation in words
        # 得到的都是小写字母
        # for word in words_origin:
        #     if word not in punctuation:
    result = ' '.join(words)
    return result.strip()  # join连接的单词由前面的空格连接。

def clean_sentence(sentence:str) -> list[str]:
    stopwords = stop_words
    list = [word for word in sentence.lower().split() if word not in stopwords]
    return list

def get_stopword_list(train_file_folder:str) -> list:
    # 停用词表，不计入统计
    f = open(train_file_folder + 'stopwords.txt','r',encoding='utf-8')
    stopwords = [word.strip() for word in f.readlines()]
    f.close()
    return stopwords

def get_sentences_list(train_file_folder:str,train_filename:str) -> list[str]:
    f = open(train_file_folder + train_filename,'r',encoding='utf-8')
    list = [sentence.strip() for sentence in f.readlines()]
    f.close()
    return list

# 从页面抽取知识得到的csv结果文件中读取所有的故障描述（只有标题），然后分词生成训练文件
def get_title_train_file(knowledge_extract_result_file_csv_path:str, train_file_output_txt_path:str) -> list:
    knowledge_extract_result_file = open(knowledge_extract_result_file_csv_path, 'r',encoding='utf-8',newline='')
    reader = csv.reader(knowledge_extract_result_file)
    row_list = list(reader)
    description_train_file = open(train_file_output_txt_path,'w',encoding='utf-8',newline='')

    origin_file_output_txt_path = train_file_output_txt_path.replace('.txt', '_origin.txt')
    description_origin_file = open(origin_file_output_txt_path,'w',encoding='utf-8',newline='')

    clean_file_output_txt_path = train_file_output_txt_path.replace('.txt', '_clean.txt')
    description_clean_file = open(clean_file_output_txt_path,'w',encoding='utf-8',newline='')

    index_file_output_txt_path = train_file_output_txt_path.replace('.txt','_index.txt')
    description_index_file = open(index_file_output_txt_path,'w',encoding='utf-8',newline='')

    description_list = []
    train_list = []
    line_count = 0

    row_number = len(row_list)
    for row_i in range(1,row_number):
        q_col = row_list[row_i][0]
        question_info = eval(q_col)       # eval将string转为dict
        index = question_info['index']
        title = question_info['title']
        if len(title) > 0:
            line_count += 1
            description_origin_file.write(title)

            token_description = tokenize_doc(title)
            description_train_file.write(token_description)

            clean = clean_sentence(token_description)
            train_list.append(clean)
            clean_sent = ' '.join(clean)
            description_clean_file.write(clean_sent)

            index_line = [index, line_count]
            description_index_file.write(str(index_line) + '\n')
            if row_i < row_number - 1:         # 最后一个标题不用换行。
                description_train_file.write('\n')
                description_origin_file.write('\n')
                description_clean_file.write('\n')
            description_list.append(token_description)
    knowledge_extract_result_file.close()
    description_train_file.close()
    dictionary = corpora.Dictionary(train_list)
    dictionary.save(train_file_output_txt_path.replace('.txt','.dict')) # 把字典保存起来，方便以后使用
    return description_list

# 从页面抽取知识得到的csv结果文件中读取所有的故障原因，然后分词生成训练文件
def get_reason_train_file(knowledge_extract_result_file_csv_path:str, train_file_output_txt_path:str) -> list:

    knowledge_extract_result_file = open(knowledge_extract_result_file_csv_path, 'r',encoding='utf-8',newline='')
    reader = csv.reader(knowledge_extract_result_file)
    row_list = list(reader)
    reason_train_file = open(train_file_output_txt_path,'w',encoding='utf-8',newline='')

    origin_file_output_txt_path = train_file_output_txt_path.replace('.txt','_origin.txt')
    reason_origin_file = open(origin_file_output_txt_path,'w',encoding='utf-8',newline='')

    clean_file_output_txt_path = train_file_output_txt_path.replace('.txt','_clean.txt')
    reason_clean_file = open(clean_file_output_txt_path,'w',encoding='utf-8',newline='')

    index_file_output_txt_path = train_file_output_txt_path.replace('.txt','_index.txt')
    reason_index_file = open(index_file_output_txt_path,'w',encoding='utf-8',newline='')

    reason_list = []
    train_list = []
    line_count = 0

    row_number = len(row_list)
    for row_i in range(1,row_number):
        question = row_list[row_i][0]
        question_info = eval(question)
        index = question_info['index']
        answer_number = len(row_list[row_i]) - 1
        for ans_i in range(1,answer_number + 1):
            ans = row_list[row_i][ans_i]
            answer_info = eval(ans)       # eval将string转为dict
            # reasons = answer_info['possible_reasons']

        # %% 方案1：将一个答案中的所有可能原因合并为一个句子
            # reason = ' '.join(reasons)
            # if len(reason) > 0:
            #     reason_train_file.write(tokenize_doc(reason))
            #     if row_i < row_number - 1 and ans_i < answer_number - 1:         # 最后一个标题不用换行。
            #         reason_train_file.write('\n')
            
        # %% 方案1.1：直接加载csv文件中已经按照回答合并好的原因
            reason = answer_info['combined_reason']
            if reason != '':
                line_count += 1

                reason_origin_file.write(reason + '\n')

                token_reason = tokenize_doc(reason)
                reason_train_file.write(token_reason + '\n')

                clean = clean_sentence(token_reason)
                train_list.append(clean)
                clean_sent = ' '.join(clean)
                reason_clean_file.write(clean_sent + '\n')

                index_i = index + f' ({ans_i})'
                index_line = [index_i, line_count]
                reason_index_file.write(str(index_line) + '\n')

                reason_list.append(token_reason)

        # %% 方案2：一个答案中的原因按照列表，一行一个，不合并
            # reason_number = len(reasons)
            # for res_i in range(0,reason_number):
            #     reason_i = reasons[res_i]

            #     reason_origin_file.write(reason_i)

            #     token_reason_i = tokenize_doc(reason_i)
            #     reason_train_file.write(token_reason_i)

            #     clean = clean_sentence(token_reason_i)
            #     train_list.append(clean)
            #     clean_sent = ' '.join(clean)
            #     reason_clean_file.write(clean_sent)

            #     if row_i < row_number - 1 and ans_i < answer_number - 1 and res_i < reason_number - 1:
            #         reason_train_file.write('\n')
            #         reason_origin_file.write('\n')
            #         reason_clean_file.write('\n')

            #     reason_list.append(token_reason_i)


    knowledge_extract_result_file.close()
    reason_train_file.close()
    reason_origin_file.close()
    dictionary = corpora.Dictionary(train_list)
    dictionary.save(train_file_output_txt_path.replace('.txt','.dict')) # 把字典保存起来，方便以后使用
    return reason_list

def get_valid_words(train_file_folder:str,train_filename:str) -> list[str]:
    # train_file_path = train_file_folder + train_filename
    clean_train_file_path = train_file_folder + train_filename.replace('.txt','_clean.txt')
    # stopwords = get_stopword_list(train_file_folder)
    stopwords = stop_words
    train_list = []
    f = open(clean_train_file_path,'r',encoding='utf-8',newline='')
    for line in f.readlines():
        sentence = line.strip()
        words = sentence.split()
        train_words = words
        train_list.append(train_words)

    # f = open(train_file_path,'r',encoding='utf-8',newline='')
    # for line in f.readlines():
    #     train_words = []
    #     # sentence = tokenize_doc(line)
    #     sentence = line.strip()
    #     words = sentence.split()
    #     for train_word in words:
    #         if train_word.strip() not in stopwords:
    #             train_words.append(train_word.strip())
    #     train_words = clean_sentence(sentence)
    #     train_list.append(train_words)
    if not os.path.exists(train_file_folder + train_filename.replace('.txt','.dict')):
        dictionary = corpora.Dictionary(train_list)
        dictionary.save(train_file_folder + train_filename.replace('.txt','.dict')) # 把字典保存起来，方便以后使用

    return train_list

# %% NOTE sklearn tf_idf

# 使用TF-IDF计算两个句子的相似度，语料库仅为两句话
def sklearn_tf_idf_similarity(sentence1:str,sentence2:str) -> float:
    # 分词、去掉特殊符号、转化为小写处理
    sentence1 = tokenize_doc(sentence1)
    sentence2 = tokenize_doc(sentence2)
    # 转化为TF矩阵
    cv = CountVectorizer(tokenizer=lambda s: s.split())
    corpus = [sentence1, sentence2]
    vectors = cv.fit_transform(corpus)
    vectors_array = vectors.A
    # 计算TF系数
    return np.dot(vectors_array[0], vectors_array[1]) / (norm(vectors_array[0]) * norm(vectors_array[1]))

# %% NOTE gensim ti_idf

def train_gensim_ti_idf_model(train_file_folder:str,train_filename:str):
    corpus_file = open(train_file_folder + train_filename.replace('.txt','_corpus_list.txt'),'w',encoding='utf-8',newline='')
    dictionary = corpora.Dictionary.load(train_file_folder + train_filename.replace('.txt','.dict'))
    train_list = get_valid_words(train_file_folder,train_filename)
    corpus = [dictionary.doc2bow(text) for text in train_list]
    corpus_str = str(corpus)
    corpus_file.write(corpus_str)
    corpus_file.close()

    tfidf = models.TfidfModel(corpus)
    tfidf.save(train_file_folder + 'gensim_%s_tf_idf_train_model.model'  %(train_filename.replace('.txt','')))

    corpus_tfidf = tfidf[corpus]
    
    return corpus_tfidf

def gensim_ti_idf_similarity(sentence1:str,sentence2:str,train_file_folder:str,train_filename:str) -> float:
    dictionary = corpora.Dictionary.load(train_file_folder + train_filename.replace('.txt','.dict'))
    corpus_file = open(train_file_folder + train_filename.replace('.txt','_corpus_list.txt'),'r',encoding='utf-8',newline='')
    sentence1 = tokenize_doc(sentence1)
    sentence2 = tokenize_doc(sentence2)
    bow1 = dictionary.doc2bow(sentence1.split())
    bow2 = dictionary.doc2bow(sentence2.split())
    compared_vectors = [bow1, bow2]
    corpus = eval(corpus_file.read())

    # train_list = get_valid_words(train_file_folder,train_filename)
    # corpus = [dictionary.doc2bow(text) for text in train_list]
    # tfidf = models.TfidfModel(corpus)
    # corpus_tfidf = tfidf[corpus]

    tfidf = models.TfidfModel.load(train_file_folder + 'gensim_%s_tf_idf_train_model.model'  %(train_filename.replace('.txt','')))
    corpus_tfidf = tfidf[corpus]

    new_vector_tfidf_ls = [tfidf[vector] for vector in compared_vectors]  # 将要比较文档转换为tfidf表示方法

    index = similarities.MatrixSimilarity(corpus_tfidf)
    target_vectors = [(index[new_vector_tfidf]) for new_vector_tfidf in new_vector_tfidf_ls]
    if isinstance(target_vectors[0],ndarray) and isinstance(target_vectors[1],ndarray):
        # similarity_float = np.dot(target_vectors[0], target_vectors[1]) / (norm(target_vectors[0]) * norm(target_vectors[1]))
        similarity = float(1 - cosine(target_vectors[0], target_vectors[1]))
        pass
    else:
        exit('TYPE ERROR!')

    return similarity

# %% NOTE gensim word2vec
def train_gensim_word2vec_model(train_file_folder:str,train_filename:str):
    clean_train_filename = train_filename.replace('.txt', '_clean.txt')

    # NOTE 自己从语料库读句子，一句一句汇总成list
    clean_sentence_list = get_sentences_list(train_file_folder,clean_train_filename)
    cleaned_corpus_list = []
    for cs in clean_sentence_list:
        cs_word_list = cs.split()
        cleaned_corpus_list.append(cs_word_list)

    # min_count = round(len(list(itertools.chain(*word_list)))/3000000)
    min_count = 0
    LineSentence_path = train_file_folder + clean_train_filename
    # NOTE 自己从语料库读句子
    model_list = Word2Vec(sentences = cleaned_corpus_list, min_count = min_count, window = 3, epochs = 10, vector_size = 150, negative = 7, sg = 1, hs = 0)
    # NOTE 用Gensim自带的读语料库的方法读句子
    model_file = Word2Vec(sentences = None, corpus_file = LineSentence_path, min_count = min_count, window = 4, epochs = 15, vector_size = 200, negative = 7, sg = 1, hs = 0)
    # NOTE Gensim方法见<https://radimrehurek.com/gensim/models/keyedvectors.html#gensim.models.keyedvectors.KeyedVectors>
    # model.wv.evaluate_word_pairs(datapath('wordsim353.tsv'))
    model = model_file
    model.save(train_file_folder + 'gensim_%s_word2vec_train_model.model'  %(train_filename.replace('.txt','')))

def gensim_word2vec_similarity(sentence1:str,sentence2:str,train_file_folder:str,train_filename:str) -> float:
    # stopwords = stop_words
    # sentence_cleaned_list = []
    s1 = clean_sentence(sentence1)
    s2 = clean_sentence(sentence2)
    # s_1 = ' '.join(s1)
    # s_2 = ' '.join(s2)

    model = Word2Vec.load(train_file_folder + 'gensim_%s_word2vec_train_model.model'  %(train_filename.replace('.txt','')))

    distance_gensim = model.wv.n_similarity(s1,s2)

    # NOTE 计算句子向量
    # shape1 = shape(model.wv[s1[0]])
    # v1 = zeros(shape = shape1)
    # n = 0
    # for w in s1:
    #     v1 += model.wv[w]
    #     n += 1
    # sv1 = v1 / n
    
    # shape2 = shape(model.wv[s2[0]])
    # v2 = zeros(shape = shape2)
    # n = 0
    # for w in s2:
    #     v2 += model.wv[w]
    #     n += 1
    # sv2 = v2 / n

    # distance_scipy = float(1 - spatial.distance.cosine(sv1, sv2))
    # distance_sklearn = cosine_similarity(sv1.reshape((-1, 1)),sv2.reshape((-1, 1)))[0][0]

    return float(distance_gensim)

def train_gensim_fasttext_model(train_file_folder:str,train_filename:str):
    min_count = 0
    train_path = train_file_folder + train_filename
    model_file = FastText(corpus_file = train_path, min_count = min_count, window = 4, epochs = 15, vector_size = 200, negative = 7, sg = 1, hs = 0)
    model = model_file
    model.save(train_file_folder + 'gensim_%s_fasttext_train_model.model'  %(train_filename.replace('.txt','')))

# %% TODO - sentence_transformers 下不下来预训练模型

def sentence_transformers_similarity_e5_large_v2(sentence1:str, sentence2:str):
    sentences = [sentence1, sentence2]
    # model = SentenceTransformer('bert-base-nli-mean-tokens')
    model = SentenceTransformer('.\\pre-trained_semantic_similarity_models\\sentence-transformers-e5-large-v2')
    sentence_embeddings = model.encode(sentences, convert_to_tensor = True)
    #Compute cosine-similarities
    similarity_torch = util.cos_sim(sentence_embeddings[0], sentence_embeddings[1])
    similarity = similarity_torch.numpy()[0][0]
    return similarity

# %% NOTE BERT transformer

# TODO - 下不下来预训练模型

# %% NOTE sgpt SGPT-125M-weightedmean-nli-bitfit

def Symmetric_Semantic_Search_Bi_Encoder(sentence1:str,sentence2:str):
    s1 = ' '.join(clean_sentence(sentence1))
    s2 = ' '.join(clean_sentence(sentence1))
    # Get our models - The package will take care of downloading the models automatically
    # For best performance: Muennighoff/SGPT-5.8B-weightedmean-nli-bitfit
    tokenizer = AutoTokenizer.from_pretrained("Muennighoff/SGPT-125M-weightedmean-nli-bitfit")
    model = AutoModel.from_pretrained("Muennighoff/SGPT-125M-weightedmean-nli-bitfit")
    # Deactivate Dropout (There is no dropout in the above models so it makes no difference here but other SGPT models may have dropout)
    model.eval()
    texts = [sentence1,sentence2]
    # Tokenize input texts
    batch_tokens = tokenizer(texts, padding=True, truncation=True, return_tensors="pt")
    # Get the embeddings
    with torch.no_grad():
        # Get hidden state of shape [bs, seq_len, hid_dim]
        last_hidden_state = model(**batch_tokens, output_hidden_states=True, return_dict=True).last_hidden_state

    # Get weights of shape [bs, seq_len, hid_dim]
    weights = (
        torch.arange(start=1, end=last_hidden_state.shape[1] + 1)
        .unsqueeze(0)
        .unsqueeze(-1)
        .expand(last_hidden_state.size())
        .float().to(last_hidden_state.device)
    )

    # Get attn mask of shape [bs, seq_len, hid_dim]
    input_mask_expanded = (
        batch_tokens["attention_mask"]
        .unsqueeze(-1)
        .expand(last_hidden_state.size())
        .float()
    )
    # Perform weighted mean pooling across seq_len: bs, seq_len, hidden_dim -> bs, hidden_dim
    sum_embeddings = torch.sum(last_hidden_state * input_mask_expanded * weights, dim=1)
    sum_mask = torch.sum(input_mask_expanded * weights, dim=1)

    embeddings = sum_embeddings / sum_mask

    # Calculate cosine similarities
    # Cosine similarities are in [-1, 1]. Higher means more similar
    cosine_sim = 1 - cosine(embeddings[0], embeddings[1])
    # print("Cosine similarity between \"%s\" and \"%s\" is: %.3f" % (texts[0], texts[1], cosine_sim))
    return cosine_sim

# NOTE- %% gensim lda

# lda主题模型，不太适用
def train_lda_model(train_file_folder:str,train_filename:str) -> None:
    train_list = get_valid_words(train_file_folder,train_filename)
    # 构建词典，构建词频矩阵，训练LDA模型
    dictionary = corpora.Dictionary.load(train_file_folder + '%s.dict' %(train_filename.replace('.txt','')))
    # train_list = list(dictionary.token2id.keys())
    corpus = [dictionary.doc2bow(text) for text in train_list]
    lda = models.ldamodel.LdaModel(corpus=corpus,id2word=dictionary,num_topics=5)
    lda.save(train_file_folder + '%s_lda_train_model.model'  %(train_filename.replace('.txt','')))

def lda_similarity(sentence1:str,sentence2:str,train_file_folder:str,train_filename:str):
    # stopwords = get_stopword_list(train_file_folder)
    stopwords = stop_words
    lda = models.ldamodel.LdaModel.load(train_file_folder + '%s_lda_train_model.model'  %(train_filename.replace('.txt','')))
    s1 = tokenize_doc(sentence1)
    s2 = tokenize_doc(sentence2)
    
    w1 = [word.strip() for word in s1.split() if word not in stopwords]
    w2 = [word.strip() for word in s2.split() if word not in stopwords]

    d1 = corpora.Dictionary([w1])
    d2 = corpora.Dictionary([w2])

    bow1 = d1.doc2bow(w1)
    bow2 = d2.doc2bow(w2)

    lda1 = lda[bow1]
    lda2 = lda[bow2]

    list1 = [i[1] for i in lda1]
    list2 = [i[1] for i in lda2]

    try:
        sim = np.dot(list1, list2) / (np.linalg.norm(list1) * np.linalg.norm(list2))
    except ValueError:
        sim = 0

    return sim

# model = LdaModel(common_corpus, 5, common_dictionary)
# cm = CoherenceModel(model=model, corpus=common_corpus, coherence='u_mass')
# coherence = cm.get_coherence()  # get coherence value

# NOTE- %% gensim 语料库处理

# 分割语料库
def split_corpus(corpus_path:str) -> list:
    corpus_list = []
    for line in open(corpus_path):
        corpus_list.append(utils.simple_preprocess(line))
    return corpus_list

class MyCorpus:
    """An iterator that yields sentences (lists of str)."""

    def __iter__(self):
        corpus_path = datapath('lee_background.cor')
        for line in open(corpus_path):
            # assume there's one document per line, tokens separated by whitespace
            yield utils.simple_preprocess(line)





description_corpus = get_title_train_file('data_test_log.csv','.\\similarity_template\\description.txt')

reason_corpus = get_reason_train_file('data_test_log.csv','.\\similarity_template\\reason.txt')

# train_lda_model('.\\similarity_template\\','description.txt')

# train_lda_model('.\\similarity_template\\','reason.txt')

# train_word2vec_model('.\\similarity_template\\','reason.txt')
# w2vec = word2vec_sim_cal('.\\similarity_template\\','reason.txt',sentence1,sentence2)
# lda = lda_similarity(sentence1,sentence2,'.\\similarity_template\\','reason.txt')

# bert = sentence_transformers_bert_similarity(sentence1,sentence2)
# print(bert)

train_gensim_fasttext_model('.\\similarity_template\\','description.txt')
train_gensim_word2vec_model('.\\similarity_template\\','description.txt')
train_gensim_ti_idf_model('.\\similarity_template\\','description.txt')

train_gensim_word2vec_model('.\\similarity_template\\','reason.txt')
train_gensim_ti_idf_model('.\\similarity_template\\','reason.txt')

description_file = open('.\\similarity_template\\description.txt', 'r', encoding='utf-8', newline='')
if os.path.exists('.\\similarity_template\\description_compare.txt'):
    os.remove('.\\similarity_template\\description_compare.txt')

descriptions = description_file.readlines()
line_number = len(descriptions)
for i in range(0,line_number):
    sentence1 = descriptions[i].strip()
    for j in range(i+1, line_number):
        result = 0
        n = 0
        print('ti_idf_gensim\tti_idf_sklearn\tword2vec_gensim\tSymmetric_Semantic_Search_BE\te5_large_v2\t')
        sentence2 = descriptions[j].strip()
        ti_idf_gensim = gensim_ti_idf_similarity(sentence1,sentence2,'.\\similarity_template\\','description.txt')
        result += ti_idf_gensim
        n += 1
        print("%.4f\t" %(ti_idf_gensim), end='')
        ti_idf_sklearn = sklearn_tf_idf_similarity(sentence1,sentence2)
        result += ti_idf_sklearn
        n += 1
        print("%.4f\t" %(ti_idf_sklearn), end='')
        word2vec_gensim = gensim_word2vec_similarity(sentence1,sentence2,'.\\similarity_template\\','description.txt')
        result += word2vec_gensim
        n += 1
        print("%.4f\t" %(word2vec_gensim), end='')
        Symmetric_Semantic_Search_BE = Symmetric_Semantic_Search_Bi_Encoder(sentence1,sentence2)
        result += Symmetric_Semantic_Search_BE
        n += 1
        print("%.4f\t" %(Symmetric_Semantic_Search_BE), end='')
        e5_large_v2 = sentence_transformers_similarity_e5_large_v2(sentence1,sentence2)
        result += e5_large_v2
        n += 1
        print("%.4f\t" %(e5_large_v2), end='')

        result_n = result/n
        print("%.4f\t" %(result_n))
        # result = (ti_idf_gensim + ti_idf_sklearn + word2vec_gensim)/3
        # result_file.write("%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n" %(ti_idf_gensim,ti_idf_sklearn,word2vec_gensim,word2vec_sim,result))
        # result_file = open('.\\similarity_template\\description_compare.txt', 'a', encoding='utf-8', newline='')
        # result_file.write("%.4f\t%.4f\t%.4f\t%.4f\n" %(ti_idf_gensim,ti_idf_sklearn,word2vec_gensim,result))
        # result_file.write("%.4f\t%.4f\t%.4f\t%.4f\t%.4f\n" %(ti_idf_gensim,ti_idf_sklearn,word2vec_gensim,Symmetric_Semantic_Search_BE,result))
        # result_file.close()

