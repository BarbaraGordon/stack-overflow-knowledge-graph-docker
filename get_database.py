# .用来匹配出换行符\n以外的任意字符
# *用来匹配前面的子表达式任意次
# +用来匹配前面的子表达式一次或多次(大于等于1次）
# ?用来匹配前面的子表达式零次或一次
# 正则表达式中的括号有三种类型:
# 中括号 [ ]: 匹配单个字符是否属于中括号中的一个字符。
# 中括号中的特殊符号也会认为是字符，比如<>,(),{}都会被看作字符的括号而非特殊含义,^ - \ 这个三个特殊符号保留特殊含义，想要匹配^ - \ 则使用\^, \-, \\
# 大括号 { }: 用于重复次数，大括号左边表达式的匹配次数。
# 小括号 ( ): 表示一个子表达式，这部分所匹配的字符将会被记住以备后续使用。
# 括号在正则表达式中常用作记忆设备，例如使用括号的子字符串匹配

# %% import
import os
import math
# spider libraries
import requests
import scrapy
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as BS
import bs4
import lxml
from lxml import etree
from lxml import html
from lxml.html import fromstring, tostring
from html.parser import HTMLParser

import pandas
import csv
import openpyxl
from markdownify import markdownify as md

import urllib3
import re
import regex
# NLP Libraries
import scapy
import nltk
import stanza
import stanfordnlp
from stanfordcorenlp import StanfordCoreNLP
# stanza.download('en')      # This downloads the English models for the neural pipeline
# nlp = stanza.Pipeline()    # This sets up a default neural pipeline in English
# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')

from string import punctuation

# %% 函数定义
def get_html_content(folderpath:str,filename:str) -> str:
    html_page = open(os.path.join(folderpath,filename),'r',encoding = 'utf-8')
    html_content_origin = html_page.read()                              # HTML的内容
    # html_content_origin = html_content_origin.replace('\n',' ').strip()  # 去掉所有换行符
    # html_content_origin = html_content_origin.strip()
    html_content_origin = html_content_origin.replace('<!DOCTYPE html>',' ').strip()
    # 去掉空格
    # RegEx_remove_space = re.compile(r'>\s+<')
    html_content_origin = re.sub('[\n]+', '\n',html_content_origin)
    html_content = re.sub(r'>\s+?<',r'><',html_content_origin)
    html_page.close()   # 关闭文档
    return html_content.strip()

def get_pure_description(BS_question_subject:bs4.element.Tag) -> str:
    # 输入是bs4.BeautifulSoup经过过滤器之后的对象
    description = ''
    for node in BS_question_subject.contents:
        if isinstance(node,bs4.element.Tag):
            if node.name == 'pre' or node.name == 'code' or node.name == 'blockquote':
                continue
            elif node.contents:     # 如果是其他标签，就再往里递归一层
                # description = description + ' ' + node.get_text(' ')
                # description = description.replace('\n',' ')
                # print(description)
                description = description + ' ' + get_pure_description(node)
            # else:
            #     description = description + ' ' + get_pure_description(node)
        else:           # 没有子标签的话可以直接加里面的文本
            description = description +  ' ' + node.get_text(' ')
            description = description.replace('\n',' ').strip()
    description.replace('\n',' ')
    description.replace('  ',' ')
    # print(description)
    return description

    # for node in BS_question_subject.contents:
    #     if node and isinstance(node,bs4.element.Tag):
    #         if node.name:
    #             if node.name == 'pre' or node.name == 'code' or node.name == 'blockquote':
    #                 continue
    #             elif node.contents:      # 没有子标签的话可以直接加里面的文本
    #                 # description = description + ' ' + node.get_text(' ')
    #                 # description = description.replace('\n',' ')
    #                 # print(description)
    #                 description = description + get_pure_description(node)
    #             # else:
    #             #     description = description + ' ' + get_pure_description(node)
    #         else:
    #             description = description + node.get_text(' ')
    #             description = description.replace('\n',' ')
    #             print(description)
    # return description

def get_description_with_inline_code(BS_question_subject:bs4.element.Tag) -> str:   # 提出类似md文件中行内`code`的形式  
    # 输入是bs4.BeautifulSoup经过过滤器之后的对象
    # inline code 在<code>代码外不会有<pre>预格式文本标签，代码块则将<pre>标签放在紧邻<code>标签的外侧
    description = ''
    for node in BS_question_subject.contents:
        if isinstance(node,bs4.element.Tag):
            if node.name == 'pre' or node.name == 'blockquote':
                continue
            elif node.name == 'code':
                description = description + ' `' + node.get_text() + '` '
                # 给行内代码加一个markdown中的行内代码标签（`inline code`）
            elif node.contents:     # 如果是其他标签，就再往里递归一层
                # description = description + ' ' + node.get_text(' ')
                # description = description.replace('\n',' ')
                # print(description)
                description = description + ' ' + get_description_with_inline_code(node)
            # else:
            #     description = description + ' ' + get_pure_description(node)
        else:           # 没有子标签的话可以直接加里面的文本
            description = description + ' ' + node.get_text(' ')
            description = description.replace('\n',' ').strip()
    description.replace('  ',' ')
    print(description)
    return description

def get_solution_description(BS_question_subject:bs4.element.Tag) -> str:
    # 输入是bs4.BeautifulSoup经过过滤器之后的对象
    # inline code 在<code>代码外不会有<pre>预格式文本标签，代码块则将<pre>标签放在紧邻<code>标签的外侧
    description = ''
    for node in BS_question_subject.contents:
        if isinstance(node,bs4.element.Tag):
            if node.name == 'pre':
                continue
            elif node.name == 'code' or node.name == 'blockquote':
                description = description + ' `' + node.get_text() + '`'
                # 给行内代码加一个markdown中的行内代码标签（`inline code`）
            elif node.contents:     # 如果是其他标签，就再往里递归一层
                description = description + ' ' + get_solution_description(node)
        else:           # 没有子标签的话可以直接加里面的文本
            description = description +  ' ' + node.get_text(' ')
            description = description.replace('\n',' ').strip()
    # print(description)
    description.replace('  ',' ').strip()
    return description

def find_code_and_description(BS_question_subject:bs4.element.Tag, result:list):
    query =len(BS_question_subject.contents)

    for i in range(0,query):
        node = BS_question_subject.contents[i]
        if isinstance(node,bs4.element.Tag):
            if node.name == 'pre' or node.name == 'code':
                code_block = node.get_text()
                if i == 0:
                    description_before = ''
                else:
                    node_p = BS_question_subject.contents[i-1]
                    if isinstance(node_p,bs4.element.NavigableString):
                        description_before = node_p.get_text()
                    else:
                        ps = node.previousSibling
                        if ps:
                            description_before = ps.get_text()
                        else:
                            description_before = ''
                if is_log(code_block,description_before,'.\\template\\log_extract\\'):
                    # print('The log is available!\nThe code is:%s \nThe description is:%s' %(code_block,description_before))

                    result.append(code_block)
                    # return result
                # csv_write.writerow([code_block,description_before])
                # f.close()
            elif node.name == 'blockquote':             # 'blockquote'里一定有'p'标签。
                p_layer = node.find()
                if isinstance(p_layer,bs4.element.Tag):
                    code_block = p_layer.get_text()
                    ps = node.previousSibling   # blockquote一定在问题部分下的第一层
                    if ps:
                        description_before = ps.get_text()
                    else:
                        description_before = ''
                    if is_log(code_block,description_before,'.\\template\\log_extract\\'):
                        # print('The log is available!\ncode is:%s \ndescription is:%s' %(code_block,description_before))
                        result.append(code_block)
                        # return result
                    in_line = len(p_layer.contents)
                    for in_n in range(0,in_line):
                        in_node = p_layer.contents[in_n]
                        if isinstance(in_node,bs4.element.NavigableString):
                            pass
                        elif isinstance(in_node,bs4.element.Tag):
                            # result.append(find_code_and_description(in_node))
                            find_code_and_description(in_node,result)
            else:
                find_code_and_description(node,result)
        else:
            pass

def get_dictionary(dictionary_path:str) -> list:
    dictionary_file = open(dictionary_path, encoding = 'utf-8')
    dictionary_patterns = dictionary_file.readlines()
    dictionary_patterns = [word.strip().replace('_',' ') for word in dictionary_patterns]
    # 把txt里的横线换成空格，把每行的判断词存贮进数组，strip用来删除字符串开头和结尾的空格
    # error和exception可能和别的词没有空格，比如‘RuntimeException’。
    dictionary_file.close()
    return dictionary_patterns

def get_word_list(thesaurus_file_path):
    thesaurus_file = open(thesaurus_file_path, 'r', encoding = 'utf-8')
    words = [word.strip() for word in thesaurus_file.readlines()]
    thesaurus_file.close()
    return words

def remove_punctuation(sentence:str) -> str:   # 去掉句子的标点符号
    # use stanfordnlp
    # words = stanfordnlp.Pipeline(processors='tokenize', lang='en')
    
    # use nltk
    words_origin = nltk.word_tokenize(sentence.strip(), language='english')
    words = [word for word in words_origin if word not in punctuation]  # remove the punctuation in words
    return ' ' + ' '.join(words) + ' '  # join连接的单词由前面的空格连接。

def cut_minimum_sentences(text:str) -> list:
    mini_sentences = []
    sentences = nltk.sent_tokenize(text)
    for sentence in sentences:
        mini_sentences += sentence.split(',')
    return mini_sentences

# 将list中所有词语写成一条正则表达式，只要匹配上list中的任意一个元素（单词）就可以
def generate_RegEx_to_match_any_word(words:list) -> str:  # words是一个list
    RegEx_text = ".*("    # .*贪婪匹配。其实只要匹配上括号里“|”分割的任意一个词就可以了。
    for word in words[0:-1]:    # Python取数组不包含最后一个索引对应的元素
        RegEx_text = RegEx_text + word + "|"    # 最后一个单词应该直接接括号
    RegEx_text = RegEx_text + words[-1]
    RegEx_text = RegEx_text + ").*"   # reg_text[0:-1] 从第一个元素取到倒数第一个元素
    # for word in words:            # 另一种写法。
    #     reg_text = reg_text + word + "|"
    # reg_text = reg_text[0:-1] + ").*"
    return RegEx_text

def conform_word1_pattern(sentence:str, words1:list) -> bool:
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1),re.I)
    # re.compile：正则表达式匹配；re.IGNORECASE=re.I：匹配时忽略大小写
    if re.search(word1_pattern,sentence) is None:
        return False
    else:
        return True

def conform_word1_word2_pattern(sentence:str, words1:list, words2:list) -> bool:
    # word1必须在word2前面，否定词必须在动词前面
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1), re.I)
    word2_pattern = re.compile(generate_RegEx_to_match_any_word(words2), re.I)

    match_words = re.findall(word2_pattern,sentence)
    # findall返回数组，if后如果是空数组就相当于false，如果不是空数组就相当于true。一个空list本身等同于False。
    if match_words:
        for match_word in match_words:
            search_former_sentence = re.search(word1_pattern,remove_punctuation(sentence[0:sentence.find(match_word)]))
            if search_former_sentence is not None:
                # find返回的是找到的匹配项在list的坐标，这一步是要从找到word2的地方的前面去找有没有word1，字符串加索引默认步长为1
                # Python find()方法检测字符串中是否包含子字符串str，如果指定beg（开始）和end（结束）范围，则检查是否包含在指定范围内，如果包含子字符串返回开始的索引值，否则返回-1。
                # https://www.runoob.com/python/att-string-find.html

                # print(search_former_sentence.group())
                return True
    return False

def is_question_valid(title_text:str,question_text:str,dictionary_path:str) -> bool:
    # 获得文件
    words1 = get_dictionary(dictionary_path + 'words1.txt')
    words2 = get_dictionary(dictionary_path + 'words2.txt')
    words3 = get_dictionary(dictionary_path + 'words3.txt')
    words4 = get_dictionary(dictionary_path + 'words4.txt')
    words5 = get_dictionary(dictionary_path + 'words5.txt')
    negative_words = get_dictionary(dictionary_path + 'negative_words.txt')
    # 开始检查标题
    title = remove_punctuation(title_text)
    # 优先级1：title符合M1(出现[触发词1])
    if conform_word1_pattern(title,words1):
        # print('Title M1')
        return True
    # 优先级2：title符合M2(出现[否定词*触发词2])
    if conform_word1_word2_pattern(title, negative_words, words2):
        # print('Title M2')
        return True
    # 优先级3：title符合M3(出现[触发词3])
    if conform_word1_pattern(title,words3):
        # print('Title M3')
        return True
    
    # 如果标题不符合要求，继续检查问题描述部分
    sentences = cut_minimum_sentences(question_text)    # 分成最小的句子的集合
    # 优先级4：question的最短句集合符合M4(出现[触发词4*触发词5]或[触发词5*触发词4])
    for sentence in sentences:
        sentence = remove_punctuation(sentence)
        if conform_word1_word2_pattern(sentence,words4,words5) or conform_word1_word2_pattern(sentence,words5,words4):
            # or说明words4、5不分前后顺序
            # print('Question M4')
            return True
    # 优先级5：question的最短句集合符合M2(出现[否定词*触发词2])
        elif conform_word1_word2_pattern(sentence,negative_words,words2):
            # print('Question M5')
            return True
    return False

def is_log(codeblock:str, description_before:str, dictionary_path:str) -> bool:
    words1 = get_dictionary(dictionary_path + 'words1.txt')
    words2 = get_dictionary(dictionary_path + 'words2.txt')
    words4 = get_dictionary(dictionary_path + 'words4.txt')
    words5 = get_dictionary(dictionary_path + 'words5.txt')

    codeblock = codeblock.replace('\n',' ') # 输入纯文本，把换行符改为空格。

    # %% codeblock是否符合M1(出现[触发词1]或[触发词2])
    codeblock_m1 = False
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1), re.I)  # 不分大小写
    word2_pattern = re.compile(generate_RegEx_to_match_any_word(words2))        # 是带大小写的名词，不能忽略大小写。
    if conform_word1_pattern(codeblock,words1) or re.search(word2_pattern,codeblock) != None:  # 不分大小写，就可以用前面定义的函数。
        codeblock_m1 = True
        # print('Codeblock M1 √')

    # %% codeblock是否符合M2(不出现[触发词3])
    codeblock_m2 = False
    #  这个r代表了原字符串的意思，比如我们匹配信息item\n时，如果没有r，我们要将正则表达式写成re.compile('item\\n')；但是，当我们加上了r时，我们的正则表达式写成re.compile(r'item\n')就可以了。
    # 字符串前面加上r：不考虑转义字符，不进行转义，按照原字符串处理。

    # m2_pattern1 = re.compile(r'[{](.*?)[}]')    # 出现[{*}]
    m2_pattern1 = re.compile(r'[{](.*)[}]')    # 出现[{*}]
    # TODO 为啥用非贪婪？应该去掉问号也可以。
    # 表达式 .*? 是满足条件的情况只匹配一次，即最小匹配（非贪婪模式）
    m2_pattern2 = re.compile(r'[=]')            # 出现[=]
    if re.search(m2_pattern1, codeblock)==None and re.search(m2_pattern2, codeblock)==None:
        # 出现[<*1>*</*2>]
        m2_pattern3 = re.compile(r'[<](.*?)[>]')
        # 这里必须用非贪婪匹配，才能取出所有成对的<>。
        contents = [content.replace('/', '') for content in re.findall(m2_pattern3, codeblock)]  # 提取<>中的内容，并覆盖掉其中的'/'
        if len(contents) == len(set(contents)):
            # 看<>标签是否有成对出现的（比如<a>...</a>），如果成对出现则认为是xml配置文件或者html文件，而不是log。
            codeblock_m2 = True
            # print('Codeblock M2 √')

    description_before = remove_punctuation(description_before) # 去掉描述中的标点符号。

    # %% description_before是否符合M3(出现[触发词4])
    description_before_m3 = False
    if conform_word1_pattern(description_before,words4):        # 可以忽略大小写，re.I，使用函数
        description_before_m3 = True
        # print('Description_before M3 √')

    # %% description_before是否符合M4(不出现[触发词5])
    description_before_m4 = False
    if not conform_word1_pattern(description_before,words5):    # 可以忽略大小写，re.I，使用函数
        description_before_m4 = True
        # print('Description_before M4 √')

    if codeblock_m1 and codeblock_m2 and (description_before_m3 or description_before_m4):
        return True
    else:
        return False

def get_answer_page_total(etree_parser_result:etree._Element) -> int:
    answer_number_str = etree_parser_result.xpath('//*[@class="answers-subheader d-flex ai-center mb8"]//span/text()')
    answer_number_str = ''.join(answer_number_str).strip()
    answer_number = int(answer_number_str)
    print('%d' %(answer_number))
    answer_page_total = math.ceil(answer_number/30)
    return answer_page_total        # 得到这个问题的总页数

def get_possible_reasons(pure_answer_text:str, dictionary_path:str) -> list[str]:
    reasons = []
    # 获得文件
    words1 = get_dictionary(dictionary_path + 'words1.txt')
    words2 = get_dictionary(dictionary_path + 'words2.txt')

    word2_pattern = re.compile(generate_RegEx_to_match_any_word(words2), re.IGNORECASE)

    sentences = nltk.sent_tokenize(pure_answer_text)
    sentence_number = len(sentences)
    for index in range(0,sentence_number):
        sentence = sentences[index]
        sentence_for_RegEx = remove_punctuation(sentence)
        words_in_sentence = nltk.word_tokenize(sentence.lower())
        pos_result = nltk.pos_tag(words_in_sentence)
        if ('since','IN') in pos_result:
            reasons.append(sentence)
        elif conform_word1_pattern(sentence_for_RegEx,words1):
            # reasons.append(sentence)
            if index < sentence_number - 1:
                if conform_word1_pattern(sentences[index + 1],[' think ',' guess ',' consider ',' suppose ']):
                    sentence = sentence + ' ' + sentences[index + 1].strip()
            reasons.append(sentence)

        elif index > 0:
            if ('so','IN') in pos_result:
                try:
                    pos_result.index(('so', 'IN'),0,0)
                    sentence = sentences[index - 1].strip() + ' ' + sentence
                    reasons.append(sentence)
                except:
                    pass
            else:
                match_word = re.search(word2_pattern,sentence_for_RegEx)
                if match_word:
                    if match_word.span()[0] == 0:
                        sentence = sentences[index - 1].strip() + ' ' + sentence
                        reasons.append(sentence)
    return reasons

def is_docker_command(sentence:str,instruction_list_docker:list,instruction_list_docker_compose:list) -> bool:
    word_list = nltk.word_tokenize(sentence)
    number = len(word_list)
    for i in range(0,number):
        if word_list[i] == 'docker':
            if i + 1 < number:
                if word_list[i+1] in instruction_list_docker:
                    return True
        if word_list[i] == 'docker-compose':
            if i + 1 < number:
                if word_list[i+1] in instruction_list_docker_compose:
                    return True
    return False

def solution_verify(answer_html:str, vote:int, is_answer_accepted:bool, dictionary_folder:str) -> bool:
    # 被提问人接受的答案入选
    if is_answer_accepted == True:
        print('accepted')
        return True
    
    # 投票数小于0的答案舍弃
    if vote < 0:
        print('vote < 0')
        return False
    
    config_files_path = dictionary_folder + 'config_files_docker.txt'
    instruction_list_docker_path = dictionary_folder + 'instructions_docker.txt'
    instruction_list_docker_compose_path = dictionary_folder + 'instructions_docker_compose.txt'
    

    instruction_list_docker = get_word_list(instruction_list_docker_path)
    instruction_list_docker_compose = get_word_list(instruction_list_docker_compose_path)
    config_file_list = get_word_list(config_files_path)

    e_answer = etree.HTML(answer_html,parser = None)
    # 有代码或者配置引导的解决方案留下
    if e_answer.xpath('//code'):
        print('Solution have code!')
        return True

    b_answer = BS((answer_html.strip()),'html.parser')
    description = get_solution_description(b_answer)
    sentences = nltk.sent_tokenize(description)         # 分句
    for sentence in sentences:
        sentence_without_punctuate = remove_punctuation(sentence)
        if conform_word1_pattern(sentence_without_punctuate,config_file_list):
            print('Solution have config!')
            return True
        if is_docker_command(sentence,instruction_list_docker,instruction_list_docker_compose):
            nltk.word_tokenize(sentence_without_punctuate)
            print('Solution have docker command!')
            return True
        print(answer_html)
    return False

def solution_fail_keep_reason(vote:int, is_answer_accepted:bool) -> bool:
    # 被提问人接受的答案入选
    if is_answer_accepted == True:
        print('accepted')
        return True
    
    # 投票数小于0的答案舍弃
    if vote < 0:
        print('vote < 0')
        return False
    return True
    

# %% 类定义
# class StackoverflowItem(scrapy.Item):
#     index = scrapy.Field()
#     content = scrapy.Field()

# %% 爬取网页

folderpath = os.getcwd() + '\\data_docker\\'
path_exist = os.path.exists(folderpath)
if not path_exist:
    os.makedirs(folderpath)

# %% xpath查找条件
question_xpath_base = '//*[@class="postcell post-layout--right"]/div[@class="s-prose js-post-body"][1]'

codeblocks_in_question_xpath = question_xpath_base + '//*[name()="code" or name()="blockquote"]'
# description_before_codeblock_xpath = codeblocks_in_question_xpath + '/preceding-sibling::node()[1]'   # 参见前面的函数。
# siblings_after_codeblock_xpath = codeblocks_in_question_xpath + '/following-sibling::node()'          # 好像用不到。

# %% 筛选需要列入提取的问题
csv_name = 'data_test_log.csv'
if os.path.exists(csv_name):
    os.remove(csv_name)
csv_title = ['question_dict']
for i in range(0,150):
    csv_title.append('answer_dict')
f = open(csv_name,'a',encoding='utf-8',newline='')
index_write = csv.writer(f)
# index_write.writerow(['question_dict','answer_dict'])
index_write.writerow(csv_title)
f.close()
# csv_write = csv.writer(f)
dir_list = os.listdir(folderpath)
order = 0   # 记录有效的问题在csv的第几行

for a in dir_list:
    # if a != '001-32.html':
    #     continue
    is_first_page_RegEx = re.compile('[0-9]{3}[-][0-9]{2}[.]html')
    if re.search(is_first_page_RegEx,a):            # 确保被处理的一定是这个问题的第一页
        question_index = a.replace('.html','')
        print('Start processing file %s' %(a))
        html_content = get_html_content(folderpath,a)
        e_content = etree.HTML(html_content,parser = None)      # xpath解析：用来找title,question,tags,codeblocks
        b_content = BS((html_content.strip()), 'html.parser')   # BeautifulSoup解析

        answers_row = []
        question_row = []

        # xpath找到标题
        title = e_content.xpath('//*[@class="fs-headline1 ow-break-word mb8 flex--item fl1"]/a/text()')[0]
        # print(title)

        # xpath找到全部问题描述
        e_question = e_content.xpath(question_xpath_base + '//text()')
        question = ''.join(e_question).strip()
        # print(question)

        b_question = b_content.find(class_="postcell post-layout--right")
        if isinstance(b_question,bs4.element.Tag):
            b_question = b_question.find(class_="s-prose js-post-body")    # 找第二层标签的时候需要用find_next
            # if b_question != None:
            #     print(b_question.text)

        # 判断可能的codeblock，找question_xpath_base的子节点中的<p><pre><code><blockquote>标签。
        # NOTE - 从这些子节点里遍历，看子节点中间是否包含<code>或<blockquote>标签，如果有一对，就直接取子节点的text，如果多于一对，则继续往下分。

        codeblocks = e_content.xpath(codeblocks_in_question_xpath + '//text()')

        # 判断问题描述里有没有code或者backquote标签，如果没有就不再处理这个页面。
        codeblock_total_number = len(codeblocks)
        if codeblock_total_number != 0:
            pass
        else:
            continue
            # 提取html元素内pre、code和blockquote标签以外的所有文本
        if isinstance(b_question,bs4.element.Tag):
            pure_user_description = get_pure_description(b_question)
            pure_user_description = pure_user_description.replace('  ',' ')
            # pure_user_description = regex.sub('\\s+','\\s',pure_user_description)
            # print(pure_user_description)
        else:
            continue

        if is_question_valid(title,pure_user_description,'.\\template\\fault_page_select\\'):
            print('%s is valid.' %(a))  # 问题可用
            order += 1

            # 找到该问题的所有标签
            tag_list = e_content.xpath('//*[@class="ml0 list-ls-none js-post-tag-list-wrapper d-inline"]//text()')
            tag_list = list(set(tag_list))      # 去掉重复的项。set()函数创建一个无序不重复元素集，可进行关系测试，删除重复数据，还可以计算交集、差集、并集等。
            # print(' | '.join(tag_list).strip())
            
        else:
            continue

        
            # 找代码块和前面紧邻的描述
        # w_f = open(csv_name,'w+')
        # w_f.truncate()
        # w_f.close()
        if isinstance(b_question,bs4.element.Tag):
            logs = []
            find_code_and_description(b_question,logs)
            print(logs)
        else:
            continue

        description = get_description_with_inline_code(b_question)
        question_dict = {'order':order,'index':question_index,'title':title,'tags':tag_list,'logs':logs,'description':description}
        question_row.append(question_dict)

    # %% 提取答案和投票数

    # %% BS方法

        html_find_answer_content = html_content.strip()
        if os.path.exists(folderpath + a.replace('.html','') + '-2.html'):  # 说明这个问题对应的答案不止一页
            a_p_a_total = get_answer_page_total(e_content) - 1

            for a_p_a in range(2,a_p_a_total + 2):
                a_p_a_name = a.replace('.html','') + '-' + str(a_p_a).zfill(1) + '.html'
                html_after_content = get_html_content(folderpath,a_p_a_name)
                html_find_answer_content = html_find_answer_content + html_after_content.strip()
                
        b_find_answer_content = BS((html_find_answer_content.strip()), 'html.parser')   # BeautifulSoup解析

        b_answer = b_find_answer_content.find_all(class_=re.compile('(answer js-answer).*'))
        print('This question have %d answer.' %(len(b_answer)))

        if isinstance(b_answer,bs4.element.ResultSet) or isinstance(b_answer,bs4.element.Tag):
            pass
        else:
            continue
        b_answer_total = len(b_answer)

        for ans_i in range(0,b_answer_total):
        # for b_answer_find in b_answer:
            b_answer_find = b_answer[ans_i]

            if isinstance(b_answer_find,bs4.element.Tag):

                b_answer_vote_number_str = b_answer_find['data-score']
                b_answer_is_accepted_str = b_answer_find['itemprop']
            else:
                continue

            if isinstance(b_answer_vote_number_str,str) and isinstance(b_answer_is_accepted_str,str):
                b_answer_vote_number = int(b_answer_vote_number_str)
                # print('How many votes: %d'  %(b_answer_vote_number))
                b_answer_is_accepted = (b_answer_is_accepted_str == 'acceptedAnswer')
                # print(f'Is answer accepted: {b_answer_is_accepted}')
            else:
                continue
            
            b_answer_find_content_tmp = b_answer_find.find(class_='answercell post-layout--right')
            if isinstance(b_answer_find_content_tmp,bs4.element.Tag):
                b_answer_find_content = b_answer_find_content_tmp.find(class_='s-prose js-post-body')
            else:
                continue
            if isinstance(b_answer_find_content,bs4.element.Tag):
                answer_text = b_answer_find_content.get_text()
                answer_html = str(b_answer_find_content).replace(u'\xa0 ', u' ')            # 当成solution
                pure_answer_text = get_description_with_inline_code(b_answer_find_content).strip()
            else:
                continue

            if not solution_verify(answer_html,b_answer_vote_number,b_answer_is_accepted,'.\\knowledge_verify\\'):
                answer_html = ''
                print('%s - %d fail'  %(a,ans_i+1))
                if not solution_fail_keep_reason(b_answer_vote_number,b_answer_is_accepted):
                    continue

            reasons = get_possible_reasons(pure_answer_text,'.\\template\\reason_extract\\')
            combined_reason = ' '.join(reasons)
            # print(reasons)
            solution_md = md(answer_html)   # 将html文本用markdownify库转为markdown文本
            fix = re.compile(r'(?<=[#]\s)(\n)+')
            solution_md = re.sub(fix, '', solution_md)
            order = ans_i + 1
            answer_dict = {'order':order,'index':question_index,'possible_reasons':reasons,'combined_reason':combined_reason,'solution_html':answer_html,'solution_md':solution_md,'vote_number':b_answer_vote_number,'is_answer_accepted':b_answer_is_accepted}
            # 一个答案的信息构成一个字典
            answers_row.append(answer_dict)
                # answer_row.append(answer_html)
        
# %% 数据写入csv
        question_row.extend(answers_row)
        # w_f.truncate()
        if question_row:         # row不为None
            pass
        else:
            continue
        f_w = open(csv_name,'a',encoding='utf-8',newline='')
        csv_write = csv.writer(f_w)
        csv_write.writerow(question_row)
        f_w.close()
