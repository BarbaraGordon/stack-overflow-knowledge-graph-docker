import csv
import re
import json
import os
import sys
import io
import logging

# %% deeplog
# from deeplog import DeepLog
# from deeplog.preprocessor import Preprocessor

# %% logai
# import logai
# from logai.applications.openset.anomaly_detection.openset_anomaly_detection_workflow import OpenSetADWorkflowConfig
# from logai.utils.file_utils import read_file
# from logai.utils.dataset_utils import split_train_dev_test_for_anomaly_detection
# from logai.dataloader.data_loader import FileDataLoader
# from logai.preprocess.hdfs_preprocessor import HDFSPreprocessor
# from logai.information_extraction.log_parser import LogParser
# from logai.preprocess.openset_partitioner import OpenSetPartitioner
# from logai.analysis.nn_anomaly_detector import NNAnomalyDetector
# from logai.information_extraction.log_vectorizer import LogVectorizer
# from logai.utils import constants
# from logai.applications.application_interfaces import WorkFlowConfig
# from logai.applications.log_anomaly_detection import LogAnomalyDetection

# %% logpai
# import logparser
# from logparser import LogSig

import drain3
from drain3.drain import Drain, LogCluster
from drain3 import template_miner,template_miner_config,TemplateMiner
from drain3.template_miner_config import TemplateMinerConfig

sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='utf8') #改变标准输出的默认编码


# 覆盖掉日志中的时间
def remove_time(log):
    # 覆盖时间
    # %% 几种时间格式正则表达式
    time_pattern_RegEx_base = ['\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}\\s[A-Z]{3}'       # 1 2014-10-07 15:56:40 GMT
                               '\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}'                  # 1 2014-10-07 15:56:40
                               '\\d{2}[/]\\d{1,2}[/]\\d{1,2}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}'              # 2 14/03/21 18:42:03
                               '[a-zA-Z]{3}\\s\\d{1,2},\\s\\d{4}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}\\s\\w{2}' # 4 Dec 12, 2012 2:19:41 PM
                               '\\d{1,2}\\s[a-zA-Z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}'           # 5 23 Apr 2014 15:21:28
                               '\\d{1,2}\\s[a-zA-Z]{3}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}'                    # 5 23 Apr 15:21:28.013
                               '[a-zA-Z]{3}\\s[a-zA-Z]{3}\\s\\s\\d{1,2}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}\\s\\d{4}'  # 6 Sat Apr  29 03:15:04 2017
                               '[a-zA-Z]{3}\\s[a-zA-Z]{3}\\s\\d{1,2}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}\\s[a-zA-Z]{3}\\s\\d{4}'   # 6 Sat Apr 29 03:15:04 BST 2017
                               '\\d{1,2}:\\d{1,2}:\\d{1,2}']                                            # 3 09:06:44
    # %% 加r，忽略\\到\的转义
    # time_pattern_RegEx_base = [r'\d{4}-\d{1,2}-\d{1,2}\s\d{1,2}:\d{1,2}:\d{1,2}'                    # 1 2014-10-07 15:56:40
    #                            r'\d{2}/\d{1,2}/\d{1,2}\s\d{1,2}:\d{1,2}:\d{1,2}'                    # 2 14/03/21 18:42:03
    #                            r'\d{1,2}:\d{1,2}:\d{1,2}'                                           # 3 09:06:44
    #                            r'[a-zA-Z]{3}\s\d{1,2},\s\d{4}\s\d{1,2}:\d{1,2}:\d{1,2}\s\w{2}'      # 4 Dec 12, 2012 2:19:41 PM
    #                            r'\d{1,2}\s[a-zA-Z]{3}\s\d{4}\s\d{1,2}:\d{1,2}:\d{1,2}'              # 5 23 Apr 2014 15:21:28
    #                            r'[a-zA-Z]{3}\s[a-zA-Z]{3}\s\d{1,2}\s\d{1,2}:\d{1,2}:\d{1,2}\s[a-zA-Z]{3}\s\d{4}']   # 6 Sat Apr 29 03:15:04 BST 2017
    millisecond_append_element = '[^0-9][0-9]+' # ',346'
    time_pattern_RegEx = []
    base_number = len(time_pattern_RegEx_base)

    for i in range(0,base_number):
        base = time_pattern_RegEx_base[i]
        time_pattern_RegEx.extend([base + millisecond_append_element, base])
    
    time_pattern_RegEx.append('GMT\\+0000')

    patter_number = len(time_pattern_RegEx)
    # time_pattern = []
    
    for j in range(0,patter_number):
        RegEx = re.compile(time_pattern_RegEx[j])
        # time_pattern.append(RegEx)
        log = re.sub(RegEx, '', log)

    return log

# 覆盖掉日志中的消息级别
def remove_level(log):
    # 错误等级正则表达式
    level = ['ERROR:', 'INFO:', 'WARN:', 'DEBUG:', 'FATAL:','HINT:',
             'FATA[0000]', 'INFO[0000]', 'WARN[0000]','ERRO[0000]'
             'ERROR', 'INFO', 'WARN', 'DEBUG', 'FATAL',
             '[ERROR]', '[INFO]', '[WARN]', 
             '[error]', '[info]', '[warn]',
             'FAILED:', 'DEPRECATED:', 
             'Error:', 'Info:', 'Warning:',
             'ERROR:', 'INFO:','WARNING:',
             'Err:']
    for word in level:
        log = log.replace(word, '')
    pattern = re.compile(r'(^error:\s|fatal\serror:\s)')
    log = re.sub(pattern,'',log)
    return log

# 覆盖掉日志中的组件、进程等'[]'里面的内容;以及文件名、类名等""里面的内容
def remove_other_content(log):
    pattern1 = re.compile(r'\[(.*?)\]')
    pattern2 = re.compile(r'["](.*?)["]')

    log = re.sub(pattern1, '*', log)        # NOTE - 原本是类名删掉，之后sxy改成用*替换
    log = re.sub(pattern2, '*', log)        # NOTE - 文件名用*替换
    return log

# 日志中的ip地址、url、路径、文件、数字（含百分数）用通配符替换
def replace_content(log):
    ip_pattern = re.compile("(/|)([0-9]+[.]){3}[0-9]+(:[0-9]+|)(:|)")   # 40.90.12.118或/40.90.12.118或40.90.12.118:8080或40.90.12.118:8080:
    # path_pattern = re.compile(r"\s(/.+/).+?\.")         #  /xxx/xx/xxxxx/xxx.
    linux_path_pattern = re.compile(r"\/(?:[^/]+\/)*")
    windows_path_pattern = re.compile(r'[a-z]:\\(?:[^\\/:*?"<>|\r\n]+\\)*[^\\/:*?"<>|\r\n]*')      # C:\\Users\\Administrator\\Desktop\\chrome.link
    # url_pattern = re.compile(r"([a-z]{4,5}:\/\/)?(?:[-\w.]|(?:%[\\da-fA-F]{2}))+")     # http://  |前面是指英文后缀，|后面指中文乱码
    url_pattern = re.compile(r"([a-z]{4,5}:\/\/(([-\w]+\.)+[-\w\:]+))(\/)?((?<=\/)(((%[\da-fA-F]{2})+|[\-\w\.]+)+(\/)?))*((?<!\/)\?[\-\w\.\=]+)?")
    # NOTE - "?:"不捕获分组，?:开头的括号的内容不会单独出现
    # file_pattern = re.compile(r"\/.*?[.][\w:]+")       # /xxx.xxx:
    # linux_file_pattern = re.compile(r'(^(\/[\w-]+)+(.[a-zA-Z]+?)$|^[a-zA-Z0-9](?:[a-zA-Z0-9 ._-]*[a-zA-Z0-9])?\.[a-zA-Z0-9_-]+$)')
    linux_file_pattern = re.compile(r'((\.)?\/([a-zA-Z0-9\.\-_]+\/)*([a-zA-Z0-9\.\-_%])*|([a-zA-Z0-9\.\-_%])+\.([a-zA-Z0-9\.\-_])+)')
    # windows_file_pattern = re.compile(r'(^[a-zA-Z]:\\(?:\w+\\)*\w+\.\w+$|^[a-zA-Z0-9](?:[a-zA-Z0-9 ._-]*[a-zA-Z0-9])?\.[a-zA-Z0-9_-]+$)')
    windows_file_pattern = re.compile(r'(([a-zA-Z]:|\.)\\)([a-zA-Z0-9\s\.\-_%]+\\)*(([a-zA-Z0-9\.\-_%])+\.([a-zA-Z0-9\-_\+])+)*')
    # int_pattern = re.compile("[0-9]+")              # 整数 234798
    # percentage_pattern = re.compile("[0-9]+%")      # 百分数 63342%
    # float_pattern = re.compile("[0-9]+[.][0-9]+")   # 小数 789.76
    sha_pattern = re.compile(r"([0-9a-z]+)([a-z]{1,4})([0-9]{1,4})[A-Za-z\d]{6,}")
    number_pattern = re.compile(r"[0-9]+([.][0-9]+)*(%)*")

    log = re.sub(url_pattern, '*', log)
    log = re.sub(windows_file_pattern, '*', log)
    log = re.sub(linux_file_pattern, '*', log)
    # log = re.sub(file_pattern, '*', log)
    log = re.sub(windows_path_pattern, '*', log)
    log = re.sub(linux_path_pattern, '*', log)
    # log = re.sub(path_pattern, '*', log)
    log = re.sub(ip_pattern, '*', log)
    log = re.sub(sha_pattern, '*', log)
    # log = re.sub(int_pattern, '*', log)
    # log = re.sub(percentage_pattern, '*', log)
    # log = re.sub(float_pattern, '*', log)
    log = re.sub(number_pattern, '*', log)

    return log

# 覆盖掉日志中所有的特殊符号
def remove_symbols(log:str) -> str:
    symbol_pattern = re.compile(r"(\.\.\.|>>>|--->|==>|=>|->|\$|>|_|~|@|-|\^|\||☁|➜|✗|ℹ|\\\\\\)") # .../>>>/$/^/|
    log = re.sub(symbol_pattern, ' ', log)
    repeat_asterisk_pattern = re.compile(r'(\*(\s)*?)+\*')
    log = re.sub(repeat_asterisk_pattern, '*', log)
    return log.strip()

# 判断日志中某一行是否需要删除，即是否为终端命令行
def is_remove_line(line:str) -> bool:
    # 错误日志的最后一行
    end_line_pattern1 = re.compile(r"\.\.\.\s[0-9]+\smore")
    end_line_pattern2 = re.compile(r"\.\.\.\s[0-9]+\selided")
    
    # 声明版权
    copyright_claim1 = re.compile(r"(®|©)")
    copyright_claim2 = re.compile(r"(All|Some|No)\sright(s)?\sreserved\.")
    copyright_claim3 = re.compile(r"Copyright(s)?\s([\w\s\*])+\.")
    copyright_claim4 = re.compile(r"(Terms and Conditions|Credits)")
    copyright_claim5 = re.compile(r"please visit \*")

    # 说明版本和欢迎词
    info_pattern1 = re.compile(r'\sVersion:\s|^Total\sMemory:\s|^Operating\sSystem:\s|^ID:\s|^OSType:\s|Root\sDir:\s|\sDriver:\s|^Architecture:\s|^Name:\s|Swarm:\s')
    info_pattern2 = re.compile(r'for\shelp\.|Total\sMemory:\s|Client\sversion:\s|^((\s){2})?Git\scommit[a-z\s\(\)]*(:)?(\s){1}|^Cloning\sinto\s|Security\sOptions:')
    info_pattern3 = re.compile(r'^Enter\spassword:|^Server\sversion:\s|^Welcome\sto\s|Volume:\s|^Backing\sFilesystem:\s|^Registry:\s|\sdone$|\sDone$|Complete[!]')
    info_pattern4 = re.compile(r'\s(S|s)uccessfully|^Server:\sDocker\sEngine|^Client:$|^((\s){1})?Context:(\s){1}|^(\s){1}Engine:$|^Containers:\s|Backing\sFilesystem:')
    info_pattern5 = re.compile(r'^((\s){1})?Debug Mode[\s\(\)a-z]*:(\s){1}|^((\s){2})?API\sversion:(\s){2}|^((\s){2})?Go\sversion(:)?(\s){1}|^Total\sMemory:|^Profile:\s')
    info_pattern6 = re.compile(r'^((\s){2})?Built:(\s){1}|^((\s){2})?OS/Arch:(\s){1}|^((\s){2})?Experimental:(\s){1}|^Plugins:$|^CPUs:\s|^(\s)?Insecure\sRegistries:$')
    info_pattern7 = re.compile(r'^Runtimes:\srunc|^runc\sversion:\s|^Live\sRestore\sEnabled:\s|^Default\sRuntime:\s|^Username:\s|^HTTP(s)?\sProxy:\s|^Init\sBinary:\s')
    # info_pattern3 = re.compile(r'')

    # 含有某些特殊字符，一般为terminal操作
    symbol_line_pattern = re.compile(r"(\$|~|>|@|❯|_(\s)*,(\s)*_)")

    # 含有任何英文字母
    word_pattern = re.compile("[a-zA-Z]")

    if re.search(end_line_pattern1, line):
        return True
    if re.search(end_line_pattern2, line):
        return True
    if re.search(copyright_claim1, line):
        return True
    if re.search(copyright_claim2, line):
        return True
    if re.search(copyright_claim3, line):
        return True
    if re.search(copyright_claim4, line):
        return True
    if re.search(copyright_claim5, line):
        return True
    if re.search(symbol_line_pattern, line):
        return True
    if re.search(info_pattern1, line):
        return True
    if re.search(info_pattern2, line):
        return True
    if re.search(info_pattern3, line):
        return True
    if re.search(info_pattern4, line):
        return True
    if re.search(info_pattern5, line):
        return True
    if re.search(info_pattern6, line):
        return True
    if re.search(info_pattern7, line):
        return True
    if not re.search(word_pattern, line):  # 不包含任何英文字母的行需要删除
        return True

    return False

# 正则化得到覆盖信息的log，获得索引文件
def get_log_train_file(knowledge_extract_result_file_csv_path:str, train_file_output_txt_path:str):

    knowledge_extract_result_file = open(knowledge_extract_result_file_csv_path, 'r',encoding='utf-8',newline='')
    # knowledge_extract_result_file = open(knowledge_extract_result_file_csv_path, 'r',newline='')
    reader = csv.reader(knowledge_extract_result_file)
    row_list = list(reader)
    log_train_file = open(train_file_output_txt_path,'w',encoding='utf-8',newline='')

    log_index_txt_path = train_file_output_txt_path.replace('.txt','') + '_index.txt'
    log_index_file = open(log_index_txt_path,'w',encoding='utf-8',newline='')

    log_origin_txt_path = train_file_output_txt_path.replace('.txt','') + '_origin.txt'
    log_origin_file = open(log_origin_txt_path,'w',encoding='utf-8',newline='')

    log_list = []
    line_count_origin = 0
    line_number = 0

    row_number = len(row_list)
    for row_i in range(1,row_number):
        row = row_list[row_i][0]
        question_info = eval(row)       # eval将string转为dict
        index = question_info['index']
        log_list = question_info['logs']
        log_number = len(log_list)
        if log_number <= 0:
            continue
        for log_i in range(0,log_number):
            log = log_list[log_i]
            
            # print(log)
            # line_count += (len(re.findall(re.compile(r'(?<!\)\n'),log)) + 1)

            log_origin_file.write(log + '\n')
            
            # log_index_file.write(index + '\t' + str(log_i + 1).zfill(1) + '\t' + str(line_count) + '\n')

            # 覆盖掉日志中的时间
            log = remove_time(log)
            # 覆盖掉日志中的消息级别
            log = remove_level(log)
            # 覆盖掉'[]'和""里面的内容
            log = remove_other_content(log)
            # ip地址、url、路径、文件、数字（含百分数）用通配符替换
            log = replace_content(log)
            
            lines = log.split('\n')
            # line_count += len(lines)
            line_n = len(lines)
            for line_i in range(0,line_n):
                line_count_origin += 1
                line = lines[line_i]
                # print(line)
                if is_remove_line(line.strip()):    # 这里应该也可以用line
                    continue
                if len(line) > 0:
                    # 覆盖掉所有的特殊符号
                    log_line = (remove_symbols(line)).strip()
                    if len(line.replace(' ', '')) > 10:
                        line_number += 1
                        log_train_file.write(log_line + '\n')
                        log_index_file.write(index + '\t' + str(log_i + 1) + '\t' + str(line_number) + '\t' + str(line_count_origin) + '\n')

            # line_count += 1

            # log_list.append(log)
    
    knowledge_extract_result_file.close()
    log_origin_file.close()
    log_train_file.close()
    log_index_file.close()
    return log_list

# 处理得到的日志模版以对比相似度
def process_template(template:str) -> str | None:
    template = template.replace('<*>','*')
    template = template.replace('(*:*:*)','*')
    template = template.replace("'*,*'","*")
    template = template.replace(" '*'*","*")
    template = template.replace('*(*)','*')
    asterisk_colon_pattern = re.compile(r'\*((\s)*[:](\s)*\*)+')
    template = re.sub(asterisk_colon_pattern, '*', template)
    asterisk_comma_pattern = re.compile(r'\*((\s)*[,](\s)*\*)+')
    template = re.sub(asterisk_comma_pattern, '*', template)
    repeat_asterisk_pattern = re.compile(r'(\*(\s)*?)+\*')
    template = re.sub(repeat_asterisk_pattern, '*', template)
    if len(template.replace(' ', '')) > 10:
        return template.strip()
    else:
        return None

def drain_model_template_extract(train_file_txt_path:str):
    model = Drain()

    index_file_txt_path = train_file_txt_path.replace('.txt','_index.txt')
    template_file_txt_path = train_file_txt_path.replace('.txt','_template.txt')
    index_change_file_txt_path = index_file_txt_path.replace('.txt','_change.txt')
    
    # logs = open('.\\log_template_extract\\log_train.txt', 'r', encoding='utf-8', newline='')
    # index = open('.\\log_template_extract\\log_train_index.txt', 'r', encoding='utf-8', newline='')

    logs = open(train_file_txt_path, 'r', encoding='utf-8', newline='')
    index = open(index_file_txt_path, 'r', encoding='utf-8', newline='')

    entries = logs.readlines()
    logs.close()
    index_origin = index.readlines()
    index.close()

    template_file = open(template_file_txt_path, 'w', encoding='utf-8', newline='')
    index = open(index_change_file_txt_path, 'w', encoding='utf-8', newline='')

    # template_file = open('.\\log_template_extract\\log_train_template.txt', 'w', encoding='utf-8', newline='')
    # index = open('.\\log_template_extract\\log_train_index_change.txt', 'w', encoding='utf-8', newline='')

    # index_change_list = []
    template_line_count = 0
    entry_len = len(entries)

    for entry_i in range(0,entry_len):
        index_i = index_origin[entry_i]
        index_list = index_i.strip().split()
        entry = entries[entry_i]
        entry = entry.strip()
        cluster, change_type = model.add_log_message(entry)
        template_line = cluster.get_template()
        template_line = process_template(template_line)
        
        # print(template_line)
        if not template_line:
            continue
        template_file.write(template_line + '\n')
        template_line_count += 1
        index_list.append(str(template_line_count))
        for i in range(0,5):
            index.write(index_list[i] + '\t')
        index.write('\n')

    template_file.close()
    index.close

def drain_template_miner_extract(log_file_txt_path:str,config_file_ini_path:str):

    template_file_path = log_file_txt_path.replace('.txt','_TemplateMiner.txt')
    index_file_path = log_file_txt_path.replace('.txt','_index.txt')
    index_change_file_txt_path = index_file_path.replace('.txt','_change.txt')

    logs = open(log_file_txt_path, 'r', encoding='utf-8', newline='')
    index = open(index_file_path, 'r', encoding='utf-8', newline='')
    template_miner_file = open(template_file_path, 'w', encoding='utf-8', newline='')
    lines = logs.readlines()
    index_origin = index.readlines()
    line_number = len(lines)

    # logs = open('.\\log_template_extract\\log_train.txt', 'r', encoding='utf-8', newline='')
    # template_miner_file = open('.\\log_template_extract\\log_train_template_miner.txt', 'w', encoding='utf-8', newline='')
    # lines = logs.readlines()

    # persistence_type = "NONE"
    # persistence_type = "REDIS"
    # persistence_type = "KAFKA"
    persistence_type = "FILE"

    # if persistence_type == "KAFKA":
    #     from drain3.kafka_persistence import KafkaPersistence

    #     persistence = KafkaPersistence("drain3_state", bootstrap_servers="localhost:9092")

    # elif persistence_type == "FILE":
    #     from drain3.file_persistence import FilePersistence

    #     persistence = FilePersistence("drain3_state.bin")

    # elif persistence_type == "REDIS":
    #     from drain3.redis_persistence import RedisPersistence

    #     persistence = RedisPersistence(redis_host='',
    #                                 redis_port=25061,
    #                                 redis_db=0,
    #                                 redis_pass='',
    #                                 is_ssl=True,
    #                                 redis_key="drain3_state_key")
    # else:
    #     persistence = None

    from drain3.file_persistence import FilePersistence
    bin_path = config_file_ini_path.replace('.ini','_state.bin')
    persistence = FilePersistence(bin_path)
        
    logger = logging.getLogger(__name__)
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(message)s')

    config = TemplateMinerConfig()
    config.load(config_file_ini_path)
    template_miner = TemplateMiner(persistence_handler = persistence, config = config)

    print(f"Drain3 started with '{persistence_type}' persistence")
    print(f"{len(config.masking_instructions)} masking instructions are in use")
    print(f"Starting training mode.")

    for log_i in range(0,line_number):
        log_line = lines[log_i].strip()
        line_index = index_origin[log_i].strip().split()
        result = template_miner.add_log_message(log_line)
        result_json = json.dumps(result)
        json_i = line_index[0] + '_' + line_index[1] + '_' + line_index[2] + '_' + line_index[3]
        # with open(f"./drain_json/{json_i}.json", "w", encoding = 'utf-8') as f:
        #     json.dump(result, f, indent = 4, ensure_ascii = False)
        #     print(f"加载{str(json_i)}.json完成...")
        print(result_json)
        template = result["template_mined"]
        params = template_miner.extract_parameters(template, log_line)
        print(f"Parameters: {str(params)}")

    print("Training done. Mined clusters:")
    for cluster in template_miner.drain.clusters:
        print(cluster)

    template_line_count = 0
    index = open(index_change_file_txt_path, 'w', encoding='utf-8', newline='')
    print(f"Starting inference mode, matching to pre-trained clusters.")
    for log_i in range(0,line_number):
        log_line = lines[log_i].strip()
        index_list = index_origin[log_i].strip().split()
        cluster = template_miner.match(log_line)
        if cluster is None:
            print(f"No match found.")
        else:
            template_line_count += 1
            index_list.append(str(template_line_count))
            list_length = len(index_list)
            for i in range(0,list_length):
                index.write(index_list[i] + '\t')
            index.write('\n')
            template = cluster.get_template()
            template_miner_file.write(template + '\n')
            print(f"Matched template #{cluster.cluster_id}: {template}")
            print(f"Parameters: {template_miner.get_parameter_list(template, log_line)}")
    logs.close()
    template_miner_file.close()

    # for line in lines:
    #     line = line.strip()
    #     cluster_result = template_miner.match(log_message = line)
    #     if cluster_result:
    #         text = cluster_result.get_template()
    #         template_miner_file.write(text + '\n')
    #     pass

def get_template_csv(template_miner_file_txt_path:str,output_file_csv_path:str) -> None:
    index_file_path = template_miner_file_txt_path.replace('_TemplateMiner.txt','_index_change.txt')
    neo4j_template_path = output_file_csv_path.replace('.csv','_neo4j.csv')

    index_file = open(index_file_path, 'r', encoding='utf-8', newline='')
    template_miner_file = open(template_miner_file_txt_path, 'r', encoding='utf-8', newline='')
    title = ['index']
    for i in range(0,60):
        title.append('template')
    f = open(output_file_csv_path, 'w', encoding='utf-8', newline='')
    csv_write = csv.writer(f)
    csv_write.writerow(title)
    f.close()

    f = open(neo4j_template_path, 'w', encoding='utf-8', newline='')
    csv_write = csv.writer(f)
    csv_write.writerow(title)
    f.close()

    index = index_file.readlines()
    template = template_miner_file.readlines()
    line_number = len(index)

    row = []
    order = 1

    # NOTE 按照单个故障页面分配模版
    for i in range(0,line_number):
        index_list = index[i].strip().split()
        template_line = template[i].replace('\n','').strip()

        if i == 0:
            row = [index_list[0], template_line]
        if i > 0:
            index_list_former = index[i-1].strip().split()
            if index_list_former[0] != index_list[0]:
                f_w = open(output_file_csv_path, 'a', encoding='utf-8', newline='')
                csv_write = csv.writer(f_w)
                csv_write.writerow(row)
                f_w.close()
                row = [index_list[0], template_line]
            else:
                row.append(template_line)

    row_o = [index[0].strip().split()[0]]
    template_extend = ''
    order = 1
    # NOTE 按照单个log分配模版
    for i in range(0,line_number):
        index_list = index[i].strip().split()
        order_current = int(index_list[1])
        template_line = template[i].replace('\n','').strip()
        if i < line_number - 1:
            index_list_later = index[i+1].strip().split()
            
            if index_list_later[0] == index_list[0]:
                if order_current > order + 1:
                    row_o.append(template_extend)
                    template_extend = ''
                    vacant_number = order_current - (order + 1)
                    for vacant_i in range(0, vacant_number):
                        row_o.append(template_extend)
                    template_extend = template_line
                    order = order_current
                elif order_current == order + 1:
                    row_o.append(template_extend)
                    template_extend = template_line
                    order = order_current
                elif order_current == order:
                    # template_extend = template_extend + '\n' + template_line
                    if len(template_extend.split()) == 0:
                        template_extend = template_line
                    else:
                        template_extend = template_extend + '\n' + template_line

            elif index_list_later[0] != index_list[0]:
                if len(template_extend.split()) == 0:
                    template_extend = template_line
                else:
                    template_extend = template_extend + '\n' + template_line
                row_o.append(template_extend)
                template_extend = ''    # 一个页面记录结束了，数组要清零

                f_w = open(neo4j_template_path, 'a', encoding='utf-8', newline='')
                csv_write = csv.writer(f_w)
                csv_write.writerow(row_o)
                f_w.close()
                order = 1
                row_o = [index_list_later[0]]

            # row_o.append(template_line)

        if i == line_number - 1:
            if order_current > order + 1:
                row_o.append(template_extend)
                template_extend = ''
                vacant_number = order_current - (order + 1)
                for vacant_i in range(0, vacant_number):
                    row_o.append(template_extend)
                template_extend = template_line
                order = order_current
            elif order_current == order + 1:
                row_o.append(template_extend)
                template_extend = template_line
                order = order_current
            elif order_current == order:
                # template_extend = template_extend + '\n' + template_line
                if len(template_extend.split()) == 0:
                    template_extend = template_line
                else:
                    template_extend = template_extend + '\n' + template_line

            f_w = open(neo4j_template_path, 'a', encoding='utf-8', newline='')
            csv_write = csv.writer(f_w)
            csv_write.writerow(row_o)
            f_w.close()

def check_same_template(csv_file_fo_neo4j_path:str) -> None:
    output_txt_file_path = csv_file_fo_neo4j_path.replace('.csv','.txt')
    template_miner_file = open(csv_file_fo_neo4j_path, 'r', encoding='utf-8', newline='')
    output_file = open(output_txt_file_path, 'w', encoding='utf-8', newline='')
    reader = csv.reader(template_miner_file)
    row_list = list(reader)
    row_number = len(row_list)
    # 让不同的故障页面一一匹配
    for row_i in range(1, row_number):
        row1 = row_list[row_i]
        log_number = len(row1) - 1
        index1 = row_list[row_i][0]
        for tmp_i in range(1,log_number + 1):
            template1 = row_list[row_i][tmp_i]
            template_list1 = template1.strip().split('\n')
            template_list1 = [t1.strip() for t1 in template_list1 if t1 != '']
            template_set1 = set(template_list1)
            for row_j in range(row_i + 1, row_number):
                index2 = row_list[row_j][0]
                row2 = row_list[row_j]
                log_number = len(row2) - 1
                for tmp_j in range(1, log_number + 1):
                    template2 = row_list[row_j][tmp_j]
                    template_list2 = template2.strip().split('\n')
                    template_list2 = [t2.strip() for t2 in template_list2 if t2 != '']
                    template_set2 = set(template_list2)

                    intersection_set = template_set1 & template_set2
                    intersection_list = list(intersection_set)
                    if len(intersection_list) != 0:
                        intersection_size = len(intersection_list)
                        intersection_dict = {'index1':index1,'log_order1':tmp_i,'index2':index2,'log_order2':tmp_j,'intersection':intersection_list,'intersection_size':intersection_size}
                        output_file.write(str(intersection_dict) + '\n')
    # 在相同的故障页面内一一匹配
    for row_i in range(1,row_number):
        row = row_list[row_i]
        log_number = len(row) - 1
        if log_number <= 1:
            continue
        else:
            pass
        index = row_list[row_i][0]
        for tmp_i in range(1,log_number + 1):
            template1 = row_list[row_i][tmp_i]
            template_list1 = template1.strip().split('\n')
            template_list1 = [t1.strip() for t1 in template_list1 if t1 != '']
            template_set1 = set(template_list1)
            for tmp_j in range(tmp_i + 1, log_number + 1):
                template2 = row_list[row_i][tmp_j]
                template_list2 = template2.strip().split('\n')
                template_list2 = [t2.strip() for t2 in template_list2 if t2 != '']
                template_set2 = set(template_list2)

                intersection_set = template_set1 & template_set2
                intersection_list = list(intersection_set)
                if len(intersection_list) != 0:
                    intersection_size = len(intersection_list)
                    intersection_dict = {'index1':index,'log_order1':tmp_i,'index2':index,'log_order2':tmp_j,'intersection':intersection_list,'intersection_size':intersection_size}
                    output_file.write(str(intersection_dict) + '\n')
    template_miner_file.close()
    output_file.close()

# 代码

# get_log_train_file('data_test_log.csv','.\\log_template_extract\\log_train.txt')

# get_log_train_file('data_test_log.csv','.\\log_template_extract\\log_data.txt')

# drain_template_miner_extract('.\\log_template_extract\\log_train.txt','.\\log_template_extract\\drain3.ini')

get_template_csv('.\\log_template_extract\\log_train_TemplateMiner.txt','.\\log_template_extract\\log_train.csv')


check_same_template('.\\log_template_extract\\log_train_neo4j.csv')




# %% Drain

# model = Drain()

# logs = open('.\\log_template_extract\\log_train.txt', 'r', encoding='utf-8', newline='')
# index = open('.\\log_template_extract\\log_train_index.txt', 'r', encoding='utf-8', newline='')
# entries = logs.readlines()
# logs.close()
# index_origin = index.readlines()
# index.close()
# template_file = open('.\\log_template_extract\\log_train_template.txt', 'w', encoding='utf-8', newline='')

# # index_change_list = []
# template_line_count = 0
# entry_len = len(entries)

# index = open('.\\log_template_extract\\log_train_index_change.txt', 'w', encoding='utf-8', newline='')

# for entry_i in range(0,entry_len):
#     index_i = index_origin[entry_i]
#     index_list = index_i.strip().split()
#     entry = entries[entry_i]
#     entry = entry.strip()
#     cluster, change_type = model.add_log_message(entry)
#     template_line = cluster.get_template()
#     template_line = process_template(template_line)
    
#     # print(template_line)
#     if not template_line:
#         continue
#     template_file.write(template_line + '\n')
#     template_line_count += 1
#     index_list.append(str(template_line_count))
#     index_change_list = index_list
#     for i in range(0,5):
#         index.write(index_change_list[i] + '\t')
#     index.write('\n')

# template_file.close()
# index.close()



# %% template_miner.TemplateMiner

# cluster = template_miner.match(line)
# parms = template_miner.get_parameter_list(cluster.get_template(), line)
# print(cluster.get_template())
# print(parms)



# %% TemplateMiner

# config = TemplateMinerConfig()
# config.load('drain3.ini')
# miner = TemplateMiner(config = config)

# logs = open('.\\log_template_extract\\log_data_origin.txt', 'r', encoding='utf-8', newline='')
# template_miner_file = open('.\\log_template_extract\\log_data_origin_template_miner.txt', 'w', encoding='utf-8', newline='')
# # lines = logs.readlines()

# # origin_logs = open('.\\log_template_extract\\log_data_origin.txt', 'r', encoding='utf-8', newline='')
# # template_miner_origin_file = open('.\\log_template_extract\\log_origin_template_miner.txt', 'w', encoding='utf-8', newline='')
# lines = logs.readlines()

# for line in lines:
#     line = line.strip()
#     # miner.add_log_message(line)
#     cluster_result = miner.match(log_message = line)
#     # templates = miner.drain
#     # print(templates)
#     # template_miner_file.write(templates + '\n')
#     # text = cluster_result.get_template()
#     if cluster_result:
#         text = cluster_result.get_template()
#         template_miner_file.write(text + '\n')
#     pass






# def extract_log_templates(log_file):
#     # 创建Drain3实例
#     miner = TemplateMiner()
    

#     # 读取日志文件并提取模板
#     with open(log_file, 'r') as f:
#         lines = f.readlines()
#         for line in lines:
#             miner.add_log_message(line)

#     # 获取提取的日志模板
#     templates = miner.drain

#     return templates

# # 使用示例：
# log_file = "path/to/your/logfile.log"
# templates = extract_log_templates(log_file)

# # 打印提取的日志模板
# for template in templates:
#     print(template)