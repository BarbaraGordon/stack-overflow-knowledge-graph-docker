import nltk
from bs4 import BeautifulSoup as BS
import bs4
from string import punctuation
import re
import lxml
from lxml import etree
from lxml import html
from lxml.html import fromstring, tostring
from html.parser import HTMLParser
import pandas
import csv

def remove_punctuation(sentence:str) -> str:   # 去掉句子的标点符号
    # use stanfordnlp
    # words = stanfordnlp.Pipeline(processors='tokenize', lang='en')
    
    # use nltk
    words_origin = nltk.word_tokenize(sentence.strip(), language='english')
    words = [word for word in words_origin if word not in punctuation]  # remove the punctuation in words
    return ' ' + ' '.join(words) + ' '  # join连接的单词由前面的空格连接。

def get_dictionary(dictionary_path:str) -> list:
    dictionary_file = open(dictionary_path)
    dictionary_patterns = dictionary_file.readlines()
    dictionary_patterns = [word.strip().replace('_',' ') for word in dictionary_patterns]
    # 把txt里的横线换成空格，把每行的判断词存贮进数组，strip用来删除字符串开头和结尾的空格
    # error和exception可能和别的词没有空格，比如‘RuntimeException’。
    dictionary_file.close()
    return dictionary_patterns

def get_description_with_inline_code(BS_question_subject:bs4.element.Tag) -> str:   # 提出类似md文件中行内`code`的形式  
    # 输入是bs4.BeautifulSoup经过过滤器之后的对象
    # inline code 在<code>代码外不会有<pre>预格式文本标签，代码块则将<pre>标签放在紧邻<code>标签的外侧
    description = ''
    for node in BS_question_subject.contents:
        if isinstance(node,bs4.element.Tag):
            if node.name == 'pre' or node.name == 'blockquote':
                continue
            elif node.name == 'code':
                description = description + ' `' + node.get_text() + '`'
                # 给行内代码加一个markdown中的行内代码标签（`inline code`）
            elif node.contents:     # 如果是其他标签，就再往里递归一层
                description = description + ' ' + get_description_with_inline_code(node)
        else:           # 没有子标签的话可以直接加里面的文本
            description = description + node.get_text(' ')
            description = description.replace('\n',' ').strip()
            # print(description)
    description.replace('  ',' ').strip()
    return description

def get_solution_description(BS_question_subject:bs4.element.Tag) -> str:
    # 输入是bs4.BeautifulSoup经过过滤器之后的对象
    # inline code 在<code>代码外不会有<pre>预格式文本标签，代码块则将<pre>标签放在紧邻<code>标签的外侧
    description = ''
    for node in BS_question_subject.contents:
        if isinstance(node,bs4.element.Tag):
            if node.name == 'pre':
                continue
            elif node.name == 'code' or node.name == 'blockquote':
                description = description + ' `' + node.get_text() + '`'
                # 给行内代码加一个markdown中的行内代码标签（`inline code`）
            elif node.contents:     # 如果是其他标签，就再往里递归一层
                description = description + ' ' + get_solution_description(node)
        else:           # 没有子标签的话可以直接加里面的文本
            description = description + node.get_text(' ')
            description = description.replace('\n',' ').strip()
            # print(description)
    description.replace('  ',' ').strip()
    return description

def generate_RegEx_to_match_any_word(words:list) -> str:  # words是一个list
    RegEx_text = ".*("    # .*贪婪匹配。其实只要匹配上括号里“|”分割的任意一个词就可以了。
    for word in words[0:-1]:    # Python取数组不包含最后一个索引对应的元素
        RegEx_text = RegEx_text + word + "|"    # 最后一个单词应该直接接括号
    RegEx_text = RegEx_text + words[-1]
    RegEx_text = RegEx_text + ").*"   # reg_text[0:-1] 从第一个元素取到倒数第一个元素
    # for word in words:            # 另一种写法。
    #     reg_text = reg_text + word + "|"
    # reg_text = reg_text[0:-1] + ").*"
    return RegEx_text

def conform_word1_pattern(sentence:str, words1:list) -> bool:
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1),re.I)
    # re.compile：正则表达式匹配；re.IGNORECASE=re.I：匹配时忽略大小写
    if re.search(word1_pattern,sentence) is None:
        return False
    else:
        return True

def conform_word1_word2_pattern(sentence:str, words1:list, words2:list) -> bool:
    # word1必须在word2前面，否定词必须在动词前面
    word1_pattern = re.compile(generate_RegEx_to_match_any_word(words1), re.I)
    word2_pattern = re.compile(generate_RegEx_to_match_any_word(words2), re.I)

    match_words = re.findall(word2_pattern,sentence)
    # findall返回数组，if后如果是空数组就相当于false，如果不是空数组就相当于true。一个空list本身等同于False。
    if match_words:
        for match_word in match_words:
            search_former_sentence = re.search(word1_pattern,remove_punctuation(sentence[0:sentence.find(match_word)]))
            if search_former_sentence is not None:
                # find返回的是找到的匹配项在list的坐标，这一步是要从找到word2的地方的前面去找有没有word1，字符串加索引默认步长为1
                # Python find()方法检测字符串中是否包含子字符串str，如果指定beg（开始）和end（结束）范围，则检查是否包含在指定范围内，如果包含子字符串返回开始的索引值，否则返回-1。
                # https://www.runoob.com/python/att-string-find.html

                # print(search_former_sentence.group())
                return True
    return False

def solution_verify(answer_html:str, vote:int, is_answer_accepted:bool, dictionary_folder:str) -> bool:
    # 被提问人接受的答案入选
    if is_answer_accepted == True:
        return True
    
    # 投票数小于0的答案舍弃
    if vote < 0:
        return False
    
    config_files_path = dictionary_folder + 'config_files.docker'
    instruction_list_docker_path = dictionary_folder + 'instructions_docker.txt'
    instruction_list_docker_compose_path = dictionary_folder + 'instructions_docker_compose.txt'
    

    instruction_list_docker = get_dictionary(instruction_list_docker_path)
    instruction_list_docker_compose = get_dictionary(instruction_list_docker_compose_path)
    config_file_list = get_dictionary(config_files_path)

    e_answer = etree.HTML(answer_html,parser = None)
    # 有代码或者配置引导的解决方案留下
    if e_answer.xpath('//code'):
        return True

    b_answer = BS((answer_html.strip()),'html.parser')
    description = get_solution_description(b_answer)
    sentences = nltk.sent_tokenize(description)         # 分句
    for sentence in sentences:
        sentence_without_punctuate = remove_punctuation(sentence)
        if conform_word1_pattern(sentence_without_punctuate,config_file_list):
            return True
        if conform_word1_word2_pattern(sentence_without_punctuate,['docker'],instruction_list_docker):
            return True
        if conform_word1_word2_pattern(sentence_without_punctuate,['docker-compose'],instruction_list_docker_compose):
            return True
    return False
        
        
        

    # for line_number in solution_json:
    #     line_list = solution_json[line_number]
    #     for element_dic in line_list:
    #         if element_dic['type'] == 'code':
    #             return True
    #         elif element_dic['type'] == 'text':
    #             text = element_dic['content']
    #             words = nltk.word_tokenize(text.lower())
    #             if set(words).intersection(set(instructions)):
    #                 return True
    #             if set(words).intersection(set(config_files)):
    #                 return True
    # return False

csv_file = 'data.csv'
p_csv = pandas.read_csv(csv_file,sep='\t')
f = open(csv_file)
reader = csv.reader(f)
# row_number = len(reader)

word_list_folder = '.\\knowledge_verify\\'

# 求行数
question_number = len(p_csv) - 1        # 第一行是index

for qi in range(1,question_number+1):
    data_line = p_csv.iloc[0]
    answer_number = len(data_line)-1
    for ai in range(1,answer_number+1):
        answer_dict = dict(data_line[ai])
        solution_html = answer_dict['solution_html']
        vote_number = answer_dict['vote_number']
        is_answer_accepted = answer_dict['is_answer_accepted']

        if solution_verify(solution_html,vote_number,is_answer_accepted,word_list_folder):
            print('%d - %d pass'  %(qi,ai))
            pass
        else:
            answer_dict['solution_html'] = ''
            print('%d - %d fail'  %(qi,ai))
        