import stanza
import nltk
from nltk.corpus import stopwords
from nltk import download
import jieba

import mteb
# import word2vec
import gensim
from gensim import utils, corpora, models, similarities
from gensim.test.utils import datapath, common_corpus, common_dictionary
from gensim.models import word2vec, keyedvectors
from gensim.models.fasttext import FastText
from gensim.models.word2vec import Word2Vec, LineSentence
from gensim.models.ldamodel import LdaModel
from gensim.models.keyedvectors import KeyedVectors
from gensim.models.ldamodel import LdaModel
from gensim.models.coherencemodel import CoherenceModel
from gensim.similarities import WmdSimilarity

import torch
from transformers import BertTokenizer, AutoModel, AutoTokenizer


from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.feature_extraction.text import TfidfTransformer
# from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from sentence_transformers import SentenceTransformer,util

import os
import csv
import json
import itertools
from string import punctuation
import re
import math
import numpy as np
from numpy import reshape, shape, empty, zeros, ndarray
from scipy.sparse import coo_matrix
from scipy import spatial
from scipy.spatial.distance import pdist, cosine
from scipy.linalg import norm
import scipy

import warnings
warnings.filterwarnings("ignore")

# download('stopwords')  # Download stopwords list.
stop_words = stopwords.words('english')

LOGS = False  # Set to True if you want to see progress in logs.
if LOGS:
    import logging
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# 使用nltk对句子分词，并去掉特殊符号全部转化为小写后转化成一个字符串输出
def tokenize_doc(document:str) -> str:
    # 输入的一定是没有换行符的文本
    document = re.sub('\n',' ',document)
    number_pattern = re.compile('[0-9]+')
    document = re.sub(number_pattern, '', document)
    # print(document)

    output = ''
    words = []
    # use stanza
    # words = stanza.Pipeline(processors='tokenize', lang='en')

    # use nltk
    sentences = nltk.sent_tokenize(document)
    for sentence in sentences:
        # sentence_letter = re.sub('[\W 0-9]+',' ',sentence)   # 去掉所有非字母字符（包括数字）
        sentence_letter = re.sub(r'[\W]+',' ',sentence)   # 去掉所有非字母和数字的字符
        words_origin = nltk.word_tokenize(sentence_letter, language='english')
        words.extend([word.lower() for word in words_origin if word not in punctuation])  # remove the punctuation in words
        # 得到的都是小写字母
        # for word in words_origin:
        #     if word not in punctuation:
    result = ' '.join(words)
    return result.strip()  # join连接的单词由前面的空格连接。

def clean_sentence(sentence:str) -> list[str]:
    stopwords = stop_words
    list = [word for word in sentence.lower().split() if word not in stopwords]
    return list

def get_stopword_list(train_file_folder:str) -> list:
    # 停用词表，不计入统计
    f = open(train_file_folder + 'stopwords.txt','r',encoding='utf-8')
    stopwords = [word.strip() for word in f.readlines()]
    f.close()
    return stopwords

def get_sentences_list(train_file_folder:str,train_filename:str) -> list[str]:
    f = open(train_file_folder + train_filename,'r',encoding='utf-8')
    list = [sentence.strip() for sentence in f.readlines()]
    f.close()
    return list

# 从页面抽取知识得到的csv结果文件中读取所有的故障描述（只有标题），然后分词生成训练文件
def get_title_train_file(knowledge_extract_result_file_csv_path:str, train_file_output_txt_path:str) -> list:
    knowledge_extract_result_file = open(knowledge_extract_result_file_csv_path, 'r',encoding='utf-8',newline='')
    reader = csv.reader(knowledge_extract_result_file)
    row_list = list(reader)
    description_train_file = open(train_file_output_txt_path,'w',encoding='utf-8',newline='')

    origin_file_output_txt_path = train_file_output_txt_path.replace('.txt', '_origin.txt')
    description_origin_file = open(origin_file_output_txt_path,'w',encoding='utf-8',newline='')

    clean_file_output_txt_path = train_file_output_txt_path.replace('.txt', '_clean.txt')
    description_clean_file = open(clean_file_output_txt_path,'w',encoding='utf-8',newline='')

    index_file_output_txt_path = train_file_output_txt_path.replace('.txt','_index.txt')
    description_index_file = open(index_file_output_txt_path,'w',encoding='utf-8',newline='')

    description_list = []
    train_list = []
    line_count = 0

    row_number = len(row_list)
    for row_i in range(1,row_number):
        q_col = row_list[row_i][0]
        question_info = eval(q_col)       # eval将string转为dict
        index = question_info['index']
        title = question_info['title']
        if len(title) > 0:
            line_count += 1
            description_origin_file.write(title)

            token_description = tokenize_doc(title)
            description_train_file.write(token_description)

            clean = clean_sentence(token_description)
            train_list.append(clean)
            clean_sent = ' '.join(clean)
            description_clean_file.write(clean_sent)

            index_line = [index, line_count]
            description_index_file.write(str(index_line) + '\n')
            if row_i < row_number - 1:         # 最后一个标题不用换行。
                description_train_file.write('\n')
                description_origin_file.write('\n')
                description_clean_file.write('\n')
            description_list.append(token_description)
    knowledge_extract_result_file.close()
    description_train_file.close()
    dictionary = corpora.Dictionary(train_list)
    dictionary.save(train_file_output_txt_path.replace('.txt','.dict')) # 把字典保存起来，方便以后使用
    return description_list

# 从页面抽取知识得到的csv结果文件中读取所有的故障原因，然后分词生成训练文件
def get_reason_train_file(knowledge_extract_result_file_csv_path:str, train_file_output_txt_path:str) -> list:

    knowledge_extract_result_file = open(knowledge_extract_result_file_csv_path, 'r',encoding='utf-8',newline='')
    reader = csv.reader(knowledge_extract_result_file)
    row_list = list(reader)
    reason_train_file = open(train_file_output_txt_path,'w',encoding='utf-8',newline='')

    origin_file_output_txt_path = train_file_output_txt_path.replace('.txt','_origin.txt')
    reason_origin_file = open(origin_file_output_txt_path,'w',encoding='utf-8',newline='')

    clean_file_output_txt_path = train_file_output_txt_path.replace('.txt','_clean.txt')
    reason_clean_file = open(clean_file_output_txt_path,'w',encoding='utf-8',newline='')

    index_file_output_txt_path = train_file_output_txt_path.replace('.txt','_index.txt')
    reason_index_file = open(index_file_output_txt_path,'w',encoding='utf-8',newline='')

    reason_list = []
    train_list = []
    line_count = 0

    row_number = len(row_list)
    for row_i in range(1,row_number):
        question = row_list[row_i][0]
        question_info = eval(question)
        index = question_info['index']
        answer_number = len(row_list[row_i]) - 1
        for ans_i in range(1,answer_number + 1):
            ans = row_list[row_i][ans_i]
            answer_info = eval(ans)       # eval将string转为dict
            # reasons = answer_info['possible_reasons']

        # %% 方案1：将一个答案中的所有可能原因合并为一个句子
            # reason = ' '.join(reasons)
            # if len(reason) > 0:
            #     reason_train_file.write(tokenize_doc(reason))
            #     if row_i < row_number - 1 and ans_i < answer_number - 1:         # 最后一个标题不用换行。
            #         reason_train_file.write('\n')
            
        # %% 方案1.1：直接加载csv文件中已经按照回答合并好的原因
            reason = answer_info['combined_reason']
            if reason != '':
                line_count += 1

                reason_origin_file.write(reason + '\n')

                token_reason = tokenize_doc(reason)
                reason_train_file.write(token_reason + '\n')

                clean = clean_sentence(token_reason)
                train_list.append(clean)
                clean_sent = ' '.join(clean)
                reason_clean_file.write(clean_sent + '\n')

                index_i = index + f' ({ans_i})'
                index_line = [index_i, line_count]
                reason_index_file.write(str(index_line) + '\n')

                reason_list.append(token_reason)

        # %% 方案2：一个答案中的原因按照列表，一行一个，不合并
            # reason_number = len(reasons)
            # for res_i in range(0,reason_number):
            #     reason_i = reasons[res_i]

            #     reason_origin_file.write(reason_i)

            #     token_reason_i = tokenize_doc(reason_i)
            #     reason_train_file.write(token_reason_i)

            #     clean = clean_sentence(token_reason_i)
            #     train_list.append(clean)
            #     clean_sent = ' '.join(clean)
            #     reason_clean_file.write(clean_sent)

            #     if row_i < row_number - 1 and ans_i < answer_number - 1 and res_i < reason_number - 1:
            #         reason_train_file.write('\n')
            #         reason_origin_file.write('\n')
            #         reason_clean_file.write('\n')

            #     reason_list.append(token_reason_i)


    knowledge_extract_result_file.close()
    reason_train_file.close()
    reason_origin_file.close()
    dictionary = corpora.Dictionary(train_list)
    dictionary.save(train_file_output_txt_path.replace('.txt','.dict')) # 把字典保存起来，方便以后使用
    return reason_list

def get_valid_words(train_file_folder:str,train_filename:str) -> list[str]:
    # train_file_path = train_file_folder + train_filename
    clean_train_file_path = train_file_folder + train_filename.replace('.txt','_clean.txt')
    # stopwords = get_stopword_list(train_file_folder)
    stopwords = stop_words
    train_list = []
    f = open(clean_train_file_path,'r',encoding='utf-8',newline='')
    for line in f.readlines():
        sentence = line.strip()
        words = sentence.split()
        train_words = words
        train_list.append(train_words)

    # f = open(train_file_path,'r',encoding='utf-8',newline='')
    # for line in f.readlines():
    #     train_words = []
    #     # sentence = tokenize_doc(line)
    #     sentence = line.strip()
    #     words = sentence.split()
    #     for train_word in words:
    #         if train_word.strip() not in stopwords:
    #             train_words.append(train_word.strip())
    #     train_words = clean_sentence(sentence)
    #     train_list.append(train_words)
    if not os.path.exists(train_file_folder + train_filename.replace('.txt','.dict')):
        dictionary = corpora.Dictionary(train_list)
        dictionary.save(train_file_folder + train_filename.replace('.txt','.dict')) # 把字典保存起来，方便以后使用

    return train_list

# %% NOTE gensim ti_idf

def train_gensim_ti_idf_model(train_file_folder:str,train_filename:str):
    corpus_file = open(train_file_folder + train_filename.replace('.txt','_corpus_list.txt'),'w',encoding='utf-8',newline='')
    dictionary = corpora.Dictionary.load(train_file_folder + train_filename.replace('.txt','.dict'))
    train_list = get_valid_words(train_file_folder,train_filename)
    corpus = [dictionary.doc2bow(text) for text in train_list]
    corpus_str = str(corpus)
    corpus_file.write(corpus_str)
    corpus_file.close()

    tfidf = models.TfidfModel(corpus)
    tfidf.save(train_file_folder + 'gensim_%s_tf_idf_train_model.model'  %(train_filename.replace('.txt','')))

    corpus_tfidf = tfidf[corpus]
    
    return corpus_tfidf

# %% NOTE gensim word2vec
def train_gensim_word2vec_model(train_file_folder:str,train_filename:str):
    clean_train_filename = train_filename.replace('.txt', '_clean.txt')

    # NOTE 自己从语料库读句子，一句一句汇总成list
    clean_sentence_list = get_sentences_list(train_file_folder,clean_train_filename)
    cleaned_corpus_list = []
    for cs in clean_sentence_list:
        cs_word_list = cs.split()
        cleaned_corpus_list.append(cs_word_list)

    # min_count = round(len(list(itertools.chain(*word_list)))/3000000)
    min_count = 0
    LineSentence_path = train_file_folder + clean_train_filename
    # NOTE 自己从语料库读句子
    model_list = Word2Vec(sentences = cleaned_corpus_list, min_count = min_count, window = 3, epochs = 10, vector_size = 150, negative = 7, sg = 1, hs = 0)
    # NOTE 用Gensim自带的读语料库的方法读句子
    model_file = Word2Vec(sentences = None, corpus_file = LineSentence_path, min_count = min_count, window = 4, epochs = 15, vector_size = 200, negative = 7, sg = 1, hs = 0)
    # NOTE Gensim方法见<https://radimrehurek.com/gensim/models/keyedvectors.html#gensim.models.keyedvectors.KeyedVectors>
    # model.wv.evaluate_word_pairs(datapath('wordsim353.tsv'))
    model = model_file
    model.save(train_file_folder + 'gensim_%s_word2vec_train_model.model'  %(train_filename.replace('.txt','')))

def train_gensim_fasttext_model(train_file_folder:str,train_filename:str):
    npy_path = (train_file_folder + 'gensim_%s_fasttext_train_model.model.wv.vectors_ngrams.npy'  %(train_filename.replace('.txt','')))
    if os.path.exists(npy_path):
        os.remove(npy_path)
    min_count = 0
    train_path = train_file_folder + train_filename
    model = FastText(min_count = min_count, window = 3, epochs = 10, vector_size = 150, negative = 7, sg = 1, hs = 0)
    model.build_vocab(corpus_file= train_path)
    model.train(
        corpus_file=train_path, epochs=model.epochs,
        total_examples=model.corpus_count, total_words=model.corpus_total_words,
    )
    
    model.save(train_file_folder + 'gensim_%s_fasttext_train_model.model'  %(train_filename.replace('.txt','')))
    # if os.path.exists(npy_path):
    #     os.remove(npy_path)

# NOTE- %% gensim lda

# lda主题模型，不太适用
def train_gensim_lda_model(train_file_folder:str,train_filename:str) -> None:
    npy_path = (train_file_folder + 'gensim_%s_lda_train_model.model.expElogbeta.npy'  %(train_filename.replace('.txt','')))
    if os.path.exists(npy_path):
        os.remove(npy_path)
    train_list = get_valid_words(train_file_folder,train_filename)
    # 构建词典，构建词频矩阵，训练LDA模型
    dictionary = corpora.Dictionary.load(train_file_folder + '%s.dict' %(train_filename.replace('.txt','')))
    # train_list = list(dictionary.token2id.keys())
    corpus = [dictionary.doc2bow(text) for text in train_list]
    tag_total_number = 978  # Known by Neo4j: `MATCH (n:Tag) RETURN count(n)`
    lda = LdaModel(corpus=corpus,id2word=dictionary,num_topics=tag_total_number-1)
    model_path = (train_file_folder + 'gensim_%s_lda_train_model.model'  %(train_filename.replace('.txt','')))
    lda.save(model_path)
    # if os.path.exists(npy_path):
    #     os.remove(npy_path)



# model = LdaModel(common_corpus, 5, common_dictionary)
# cm = CoherenceModel(model=model, corpus=common_corpus, coherence='u_mass')
# coherence = cm.get_coherence()  # get coherence value

# %% NOTE gensim 语料库处理

# 分割语料库
def split_corpus(corpus_path:str) -> list:
    corpus_list = []
    for line in open(corpus_path):
        corpus_list.append(utils.simple_preprocess(line))
    return corpus_list

class MyCorpus:
    """An iterator that yields sentences (lists of str)."""

    def __iter__(self):
        corpus_path = datapath('lee_background.cor')
        for line in open(corpus_path):
            # assume there's one document per line, tokens separated by whitespace
            yield utils.simple_preprocess(line)

def main():

    description_corpus = get_title_train_file('data_test_log.csv','.\\similarity_template\\description.txt')
    reason_corpus = get_reason_train_file('data_test_log.csv','.\\similarity_template\\reason.txt')

    train_gensim_lda_model('.\\similarity_template\\','description.txt')
    train_gensim_fasttext_model('.\\similarity_template\\','description.txt')
    train_gensim_word2vec_model('.\\similarity_template\\','description.txt')
    train_gensim_ti_idf_model('.\\similarity_template\\','description.txt')

    train_gensim_lda_model('.\\similarity_template\\','reason.txt')
    train_gensim_fasttext_model('.\\similarity_template\\','reason.txt')
    train_gensim_word2vec_model('.\\similarity_template\\','reason.txt')
    train_gensim_ti_idf_model('.\\similarity_template\\','reason.txt')

if __name__ == '__main__':
    main()