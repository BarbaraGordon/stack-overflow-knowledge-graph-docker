import stanza
import nltk
from nltk.corpus import stopwords
from nltk import download
import jieba

import mteb

import gensim
from gensim import utils, corpora, models, similarities

from gensim.test.utils import datapath, common_corpus, common_dictionary
from gensim.models import word2vec, keyedvectors
from gensim.models.fasttext import FastText
from gensim.models.word2vec import Word2Vec, LineSentence
from gensim.models.keyedvectors import KeyedVectors
from gensim.models.ldamodel import LdaModel
from gensim.models.coherencemodel import CoherenceModel
from gensim.similarities import WmdSimilarity

import torch
from transformers import BertTokenizer, AutoModel, AutoTokenizer, AutoModelForCausalLM


from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.feature_extraction.text import TfidfTransformer
# from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from sentence_transformers import SentenceTransformer,util

import os
# import csv
# import json
# import itertools
from string import punctuation
# import re
# import math
import numpy
import numpy as np
from numpy import shape, reshape, empty, zeros, ndarray, save, load
from scipy.sparse import coo_matrix
from scipy import spatial
from scipy.spatial.distance import pdist, cosine
from scipy.linalg import norm
import scipy

# 引入自己写的函数
import sentence_embedding_model_training
from sentence_embedding_model_training import tokenize_doc
from sentence_embedding_model_training import clean_sentence

# download('stopwords')  # Download stopwords list.
stop_words = stopwords.words('english')

import warnings
warnings.filterwarnings('ignore')

def check_dir(folderpath:str):
    # folderpath = os.getcwd() + '\\' + relative_path + '\\'
    path_exist = os.path.exists(folderpath)
    if not path_exist:
        os.makedirs(folderpath)

def get_npy_path(train_path:str,suffix:str,index:str):
    result = train_path.replace('.txt', suffix) + '\\' + index + '.npy'
    return result

# %% NOTE sklearn tf_idf
# 使用nltk对句子分词，并去掉特殊符号全部转化为小写后转化成一个字符串输出
# 使用TF-IDF计算两个句子的相似度，语料库仅为两句话
# def sklearn_tf_idf_embedding(sentence:str):

def gensim_ti_idf_embedding(sentence:str,train_file_folder:str,train_filename:str):
    dictionary = corpora.Dictionary.load(train_file_folder + train_filename.replace('.txt','.dict'))
    corpus_file = open(train_file_folder + train_filename.replace('.txt','_corpus_list.txt'),'r',encoding='utf-8',newline='')
    sentence = tokenize_doc(sentence)
    bow = dictionary.doc2bow(sentence.split())
    vector = bow
    corpus = eval(corpus_file.read())

    # train_list = get_valid_words(train_file_folder,train_filename)
    # corpus = [dictionary.doc2bow(text) for text in train_list]
    # tfidf = models.TfidfModel(corpus)
    # corpus_tfidf = tfidf[corpus]

    tfidf = models.TfidfModel.load(train_file_folder + 'gensim_%s_tf_idf_train_model.model'  %(train_filename.replace('.txt','')))
    corpus_tfidf = tfidf[corpus]
    index = similarities.MatrixSimilarity(corpus_tfidf)

    new_vector_tfidf_ls = tfidf[vector]     # 将要比较文档转换为tfidf表示方法

    target_vector = index[new_vector_tfidf_ls]
    if isinstance(target_vector, ndarray):
        # similarity_float = np.dot(target_vectors[0], target_vectors[1]) / (norm(target_vectors[0]) * norm(target_vectors[1]))
        print(type(target_vector))
        print(str(target_vector))
        return target_vector
    else:
        exit(1)

def gensim_word2vec_embedding(sentence:str,train_file_folder:str,train_filename:str):
    # stopwords = stop_words
    # sentence_cleaned_list = []
    s = clean_sentence(sentence)
    model = Word2Vec.load(train_file_folder + 'gensim_%s_word2vec_train_model.model'  %(train_filename.replace('.txt','')))

    # NOTE 计算句子向量
    shape_v = shape(model.wv[s[0]])
    v = zeros(shape = shape_v)
    n = 0
    for w in s:
        v += model.wv[w]
        n += 1
    sv = v / n
    print(str(sv))
    return sv

def sentence_transformers_embedding(sentence:str, model_parent_dir:str, model_name:str):
    model = SentenceTransformer(model_parent_dir + model_name + '\\')
    sentence_embedding = model.encode(sentence, convert_to_numpy = True)
    print(type(sentence_embedding))
    print(str(sentence_embedding))
    return sentence_embedding

def transformers_torch_embedding(sentence:str):
    # Get our models - The package will take care of downloading the models automatically
    # For best performance: Muennighoff/SGPT-5.8B-weightedmean-msmarco-specb-bitfit
    tokenizer = AutoTokenizer.from_pretrained("Muennighoff/SGPT-125M-weightedmean-msmarco-specb-bitfit")
    model = AutoModel.from_pretrained("Muennighoff/SGPT-125M-weightedmean-msmarco-specb-bitfit")
    # Deactivate Dropout (There is no dropout in the above models so it makes no difference here but other SGPT models may have dropout)
    model.eval()
    SPECB_QUE_BOS = tokenizer.encode("[", add_special_tokens=False)[0]
    SPECB_QUE_EOS = tokenizer.encode("]", add_special_tokens=False)[0]
    SPECB_DOC_BOS = tokenizer.encode("{", add_special_tokens=False)[0]
    SPECB_DOC_EOS = tokenizer.encode("}", add_special_tokens=False)[0]

    def tokenize_with_specb(texts, is_query):
        # Tokenize without padding
        batch_tokens = tokenizer(texts, padding=False, truncation=True)   
        # Add special brackets & pay attention to them
        ids = batch_tokens["input_ids"]
        mask = batch_tokens["attention_mask"]
        if isinstance(ids,list) and isinstance(mask,list):
            for seq, att in zip(ids, mask):
                if isinstance(seq,int):
                    seq = [seq]
                if isinstance(att,int):
                    att = [att]
                if is_query:
                    seq.insert(0, SPECB_QUE_BOS)
                    seq.append(SPECB_QUE_EOS)
                else:
                    seq.insert(0, SPECB_DOC_BOS)
                    seq.append(SPECB_DOC_EOS)
                att.insert(0, 1)
                att.append(1)
        batch_tokens["input_ids"] = ids
        batch_tokens["attention_mask"] = mask
        # Add padding
        batch_tokens = tokenizer.pad(batch_tokens, padding=True, return_tensors="pt")
        # batch_tokens = tokenizer.__call__()
        return batch_tokens
    def get_weightedmean_embedding(batch_tokens, model):
        # Get the embeddings
        with torch.no_grad():
            # Get hidden state of shape [bs, seq_len, hid_dim]
            last_hidden_state = model(**batch_tokens, output_hidden_states=True, return_dict=True).last_hidden_state
        # Get weights of shape [bs, seq_len, hid_dim]
        weights = (
            torch.arange(start=1, end=last_hidden_state.shape[1] + 1)
            .unsqueeze(0)
            # .unsqueeze(-1)
            .expand(last_hidden_state.size())
            .float().to(last_hidden_state.device)
        )
        # Get attn mask of shape [bs, seq_len, hid_dim]
        input_mask_expanded = (
            batch_tokens["attention_mask"]
            .unsqueeze(-1)
            .expand(last_hidden_state.size())
            .float()
        )
        # Perform weighted mean pooling across seq_len: bs, seq_len, hidden_dim -> bs, hidden_dim
        sum_embeddings = torch.sum(last_hidden_state * input_mask_expanded * weights, dim=1)
        sum_mask = torch.sum(input_mask_expanded * weights, dim=1)
        embeddings = sum_embeddings / sum_mask
        return embeddings
    embedding = get_weightedmean_embedding(tokenize_with_specb(sentence, is_query=True), model)
    return embedding.numpy()

def transformers_torch_embedding_symmetry(sentence:str):
    tokenizer = AutoTokenizer.from_pretrained("Muennighoff/SGPT-125M-weightedmean-nli-bitfit")
    model = AutoModel.from_pretrained("Muennighoff/SGPT-125M-weightedmean-nli-bitfit")
    # Deactivate Dropout (There is no dropout in the above models so it makes no difference here but other SGPT models may have dropout)
    model.eval()
    # Tokenize input texts
    texts = [
        sentence,
    ]
    batch_tokens = tokenizer(texts, padding=True, truncation=True, return_tensors="pt")
    # Get the embeddings
    with torch.no_grad():
        # Get hidden state of shape [bs, seq_len, hid_dim]
        last_hidden_state = model(**batch_tokens, output_hidden_states=True, return_dict=True).last_hidden_state
    # Get weights of shape [bs, seq_len, hid_dim]
    weights = (
        torch.arange(start=1, end=last_hidden_state.shape[1] + 1)
        .unsqueeze(0)
        .unsqueeze(-1)
        .expand(last_hidden_state.size())
        .float().to(last_hidden_state.device)
    )
    # Get attn mask of shape [bs, seq_len, hid_dim]
    mask = batch_tokens["attention_mask"]
    if isinstance(mask,torch.Tensor):
        pass
    else:
        exit(1)
    input_mask_expanded = (
        mask
        .unsqueeze(-1)
        .expand(last_hidden_state.size())
        .float()
    )
    
    # Perform weighted mean pooling across seq_len: bs, seq_len, hidden_dim -> bs, hidden_dim
    sum_embeddings = torch.sum(last_hidden_state * input_mask_expanded * weights, dim=1)
    sum_mask = torch.sum(input_mask_expanded * weights, dim=1)
    embeddings = sum_embeddings / sum_mask
    return embeddings.numpy()

def gensim_fasttext_embedding(sentence:str,train_file_folder:str,train_filename:str):
    s = tokenize_doc(sentence)
    model = FastText.load(train_file_folder + 'gensim_%s_fasttext_train_model.model'  %(train_filename.replace('.txt','')))
    shape = numpy.shape(model.wv[s[0]])
    v = zeros(shape = shape)
    n = 0
    for w in s:
        v += model.wv[w]
        n += 1
    sv = v / n
    return sv

def gensim_lda_embedding(sentence:str,train_file_folder:str,train_filename:str):
    s = tokenize_doc(sentence)
    model = LdaModel.load(train_file_folder + 'gensim_%s_fasttext_train_model.model'  %(train_filename.replace('.txt','')))
    shape = numpy.shape(model.wv[s[0]])
    v = zeros(shape = shape)
    n = 0
    for w in s:
        v += model.wv[w]
        n += 1
    sv = v / n
    return sv

def embedding_corpus(train_file_folder:str,train_filename:str,model_parent_dir:str):
    train_path = train_file_folder + train_filename
    train_clean_filename = train_filename.replace('.txt', '_clean.txt')
    train_clean_path = train_file_folder + train_clean_filename
    index_path = train_file_folder + train_filename.replace('.txt','_index.txt')
    corpus_file = open(train_path, 'r', encoding='utf-8', newline='')
    corpus_clean_file = open(train_clean_path, 'r', encoding='utf-8', newline='')
    index_file = open(index_path, 'r', encoding='utf-8', newline='')
    corpus = corpus_file.readlines()
    corpus_clean = corpus_clean_file.readlines()
    index_list = index_file.readlines()

    # NOTE ignore model list
    ignore_list = ['SGPT-125M-weightedmean-msmarco-specb-bitfit', 'SGPT-125M-weightedmean-nli-bitfit', 'gpt-neo-125m']
    # pre_trained_model_list = ['all-MiniLM-L6-v2', 'sentence-transformers-e5-large-v2', 'SGPT-125M-weightedmean-nli-bitfit', 'SGPT-125M-weightedmean-msmarco-specb-bitfit']
    pre_trained_model_list = os.listdir(model_parent_dir)

    dir_list = []
    suffix = '_gensim_ti_idf'
    dir_new = train_path.replace('.txt', suffix)
    dir_list.append(dir_new)

    suffix = '_gensim_word2vec'
    dir_new = train_path.replace('.txt', suffix)
    dir_list.append(dir_new)

    suffix = '_gensim_fasttext'
    dir_new = train_path.replace('.txt', suffix)
    dir_list.append(dir_new)

    suffix = '_gensim_lda'
    dir_new = train_path.replace('.txt', suffix)
    dir_list.append(dir_new)
    
    suffix_base = '_sentence_transformers_'
    for m in pre_trained_model_list:
        if m not in ignore_list:
            suffix = suffix_base + m
            dir_new = train_path.replace('.txt', suffix)
            dir_list.append(dir_new)

    suffix_base = '_transformers_'
    for m in ignore_list:
        suffix = suffix_base + m
        dir_new = train_path.replace('.txt', suffix)
        dir_list.append(dir_new)

    for dir_path in dir_list:
        check_dir(dir_path)
    
    # dir_number = len(dir_list)

    line_number = len(corpus)
    for i in range(0,line_number):
        index = eval(index_list[i].strip())[0]
        
        s = corpus[i].strip()
        sc = corpus_clean[i].strip()

        npy_path = dir_list[0] + '\\' + index + '.npy'
        if not os.path.exists(npy_path):
            e = gensim_ti_idf_embedding(sc,train_file_folder,train_filename)
            numpy.save(npy_path, e)
        
        npy_path = dir_list[1] + '\\' + index + '.npy'
        if not os.path.exists(npy_path):
            e = gensim_word2vec_embedding(sc,train_file_folder,train_filename)
            # array_list.append(e)
            numpy.save(npy_path, e)

        npy_path = dir_list[2] + '\\' + index + '.npy'
        if not os.path.exists(npy_path):
            e = gensim_fasttext_embedding(s,train_file_folder,train_filename)
            # array_list.append(e)
            numpy.save(npy_path, e)
        
        # npy_path = dir_list[3] + '\\' + index + '.npy'
        # if not os.path.exists(npy_path):
        #     e = gensim_lda_embedding(sc,train_file_folder,train_filename)
        #     numpy.save(npy_path, e)

        for m in pre_trained_model_list:
            if m not in ignore_list:
                suffix = '_sentence_transformers_' + m
                npy_path = train_path.replace('.txt', suffix) + '\\' + index + '.npy'
                npy_path = get_npy_path(train_path, suffix, index)
                if not os.path.exists(npy_path):
                    e = sentence_transformers_embedding(s, model_parent_dir, m)
                    numpy.save(npy_path, e)


            if m == ignore_list[0]:
                suffix = '_transformers_' + m
                npy_path = get_npy_path(train_path, suffix, index)
                if not os.path.exists(npy_path):
                    e = transformers_torch_embedding(s)
                    numpy.save(npy_path, e)
            
            if m == ignore_list[1]:
                suffix = '_transformers_' + m
                npy_path = get_npy_path(train_path, suffix, index)
                if not os.path.exists(npy_path):
                    e = transformers_torch_embedding_symmetry(s)
                    numpy.save(npy_path, e)

def main():
    parent_folder = '.\\transformers_models\\'
    train_file_folder = '.\\similarity_template\\'
    # embedding_corpus(train_file_folder,'description.txt',parent_folder)
    embedding_corpus(train_file_folder,'reason.txt',parent_folder)

if __name__ =='__main__':
    main()


    